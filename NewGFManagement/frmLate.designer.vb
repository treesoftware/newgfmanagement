﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.butSave = New System.Windows.Forms.Button()
        Me.butEdit = New System.Windows.Forms.Button()
        Me.butDel = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dtpLatetime = New System.Windows.Forms.DateTimePicker()
        Me.txtSumlate = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtpLatedate = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtComment = New System.Windows.Forms.TextBox()
        Me.txtNum = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtPosition = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDep = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtLname = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtFname = New System.Windows.Forms.TextBox()
        Me.butSearch = New System.Windows.Forms.Button()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtTimeout = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.txtTimein = New System.Windows.Forms.TextBox()
        Me.txtSasom = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dgvLate = New System.Windows.Forms.DataGridView()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvLate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.butSave)
        Me.GroupBox1.Controls.Add(Me.butEdit)
        Me.GroupBox1.Controls.Add(Me.butDel)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.dgvLate)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(20, 8)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1711, 1356)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "บันทึกขอเข้างานสายรายเดือน"
        '
        'butSave
        '
        Me.butSave.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSave.Location = New System.Drawing.Point(394, 591)
        Me.butSave.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(259, 56)
        Me.butSave.TabIndex = 100
        Me.butSave.Text = "บันทึกขอเข้าสาย"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'butEdit
        '
        Me.butEdit.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butEdit.Location = New System.Drawing.Point(877, 591)
        Me.butEdit.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butEdit.Name = "butEdit"
        Me.butEdit.Size = New System.Drawing.Size(117, 56)
        Me.butEdit.TabIndex = 101
        Me.butEdit.Text = "แก้ไข"
        Me.butEdit.UseVisualStyleBackColor = True
        '
        'butDel
        '
        Me.butDel.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butDel.Location = New System.Drawing.Point(720, 591)
        Me.butDel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butDel.Name = "butDel"
        Me.butDel.Size = New System.Drawing.Size(117, 56)
        Me.butDel.TabIndex = 102
        Me.butDel.Text = "ลบ"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dtpLatetime)
        Me.GroupBox3.Controls.Add(Me.txtSumlate)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.dtpLatedate)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.txtComment)
        Me.GroupBox3.Controls.Add(Me.txtNum)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(28, 352)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox3.Size = New System.Drawing.Size(1529, 211)
        Me.GroupBox3.TabIndex = 99
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "ข้อมูลขอเข้าสาย"
        '
        'dtpLatetime
        '
        Me.dtpLatetime.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpLatetime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpLatetime.Location = New System.Drawing.Point(703, 51)
        Me.dtpLatetime.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpLatetime.Name = "dtpLatetime"
        Me.dtpLatetime.Size = New System.Drawing.Size(201, 51)
        Me.dtpLatetime.TabIndex = 93
        '
        'txtSumlate
        '
        Me.txtSumlate.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtSumlate.Location = New System.Drawing.Point(1072, 56)
        Me.txtSumlate.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtSumlate.Name = "txtSumlate"
        Me.txtSumlate.Size = New System.Drawing.Size(120, 51)
        Me.txtSumlate.TabIndex = 92
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.Location = New System.Drawing.Point(45, 58)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(251, 43)
        Me.Label7.TabIndex = 80
        Me.Label7.Text = "วันเดือนปีที่ขอเข้างานสาย :"
        '
        'dtpLatedate
        '
        Me.dtpLatedate.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpLatedate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpLatedate.Location = New System.Drawing.Point(320, 56)
        Me.dtpLatedate.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpLatedate.Name = "dtpLatedate"
        Me.dtpLatedate.Size = New System.Drawing.Size(201, 51)
        Me.dtpLatedate.TabIndex = 79
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label12.Location = New System.Drawing.Point(567, 58)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(129, 43)
        Me.Label12.TabIndex = 81
        Me.Label12.Text = "เวลาเข้าสาย :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.Location = New System.Drawing.Point(952, 58)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(114, 43)
        Me.Label13.TabIndex = 86
        Me.Label13.Text = "รวมครั้งนี้ :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label16.Location = New System.Drawing.Point(577, 122)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(111, 43)
        Me.Label16.TabIndex = 91
        Me.Label16.Text = "หมายเหตุ :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.Location = New System.Drawing.Point(1213, 58)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(50, 43)
        Me.Label14.TabIndex = 87
        Me.Label14.Text = "ชม."
        '
        'txtComment
        '
        Me.txtComment.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtComment.Location = New System.Drawing.Point(703, 116)
        Me.txtComment.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(532, 51)
        Me.txtComment.TabIndex = 90
        '
        'txtNum
        '
        Me.txtNum.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtNum.Location = New System.Drawing.Point(320, 114)
        Me.txtNum.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtNum.Name = "txtNum"
        Me.txtNum.Size = New System.Drawing.Size(201, 51)
        Me.txtNum.TabIndex = 88
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.Location = New System.Drawing.Point(93, 118)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(209, 43)
        Me.Label15.TabIndex = 89
        Me.Label15.Text = "เลขที่หนังสืออนุญาติ :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtPosition)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtDep)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtLname)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.txtFname)
        Me.GroupBox2.Controls.Add(Me.butSearch)
        Me.GroupBox2.Controls.Add(Me.txtId)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.txtTimeout)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.TextBox12)
        Me.GroupBox2.Controls.Add(Me.txtTimein)
        Me.GroupBox2.Controls.Add(Me.txtSasom)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(28, 44)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox2.Size = New System.Drawing.Size(1532, 296)
        Me.GroupBox2.TabIndex = 98
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "ข้อมูลพนักงาน"
        '
        'txtPosition
        '
        Me.txtPosition.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPosition.Location = New System.Drawing.Point(1283, 127)
        Me.txtPosition.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPosition.Name = "txtPosition"
        Me.txtPosition.Size = New System.Drawing.Size(124, 51)
        Me.txtPosition.TabIndex = 108
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(1132, 138)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(130, 43)
        Me.Label2.TabIndex = 107
        Me.Label2.Text = "ชื่อตำแหน่ง :"
        '
        'txtDep
        '
        Me.txtDep.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtDep.Location = New System.Drawing.Point(904, 130)
        Me.txtDep.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDep.Name = "txtDep"
        Me.txtDep.Size = New System.Drawing.Size(199, 51)
        Me.txtDep.TabIndex = 106
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(784, 135)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(109, 43)
        Me.Label1.TabIndex = 105
        Me.Label1.Text = "ชื่อแผนก :"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label11.Location = New System.Drawing.Point(101, 139)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(56, 43)
        Me.Label11.TabIndex = 101
        Me.Label11.Text = "ชื่อ :"
        '
        'txtLname
        '
        Me.txtLname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtLname.Location = New System.Drawing.Point(541, 131)
        Me.txtLname.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtLname.Name = "txtLname"
        Me.txtLname.Size = New System.Drawing.Size(220, 51)
        Me.txtLname.TabIndex = 102
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.Location = New System.Drawing.Point(428, 135)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(107, 43)
        Me.Label10.TabIndex = 103
        Me.Label10.Text = "นามสกุล :"
        '
        'txtFname
        '
        Me.txtFname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtFname.Location = New System.Drawing.Point(188, 135)
        Me.txtFname.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtFname.Name = "txtFname"
        Me.txtFname.Size = New System.Drawing.Size(185, 51)
        Me.txtFname.TabIndex = 104
        '
        'butSearch
        '
        Me.butSearch.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSearch.Location = New System.Drawing.Point(508, 41)
        Me.butSearch.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butSearch.Name = "butSearch"
        Me.butSearch.Size = New System.Drawing.Size(157, 52)
        Me.butSearch.TabIndex = 100
        Me.butSearch.Text = "ค้นหา"
        Me.butSearch.UseVisualStyleBackColor = True
        '
        'txtId
        '
        Me.txtId.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtId.Location = New System.Drawing.Point(245, 49)
        Me.txtId.Margin = New System.Windows.Forms.Padding(4)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(236, 44)
        Me.txtId.TabIndex = 99
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(45, 56)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(199, 43)
        Me.Label9.TabIndex = 98
        Me.Label9.Text = "ค้นหารหัสพนักงาน :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(45, 225)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(129, 43)
        Me.Label5.TabIndex = 70
        Me.Label5.Text = "เวลาเข้างาน :"
        '
        'txtTimeout
        '
        Me.txtTimeout.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTimeout.Location = New System.Drawing.Point(597, 219)
        Me.txtTimeout.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtTimeout.Name = "txtTimeout"
        Me.txtTimeout.Size = New System.Drawing.Size(239, 51)
        Me.txtTimeout.TabIndex = 71
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(473, 225)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(141, 43)
        Me.Label4.TabIndex = 72
        Me.Label4.Text = "เวลาออกงาน :"
        '
        'TextBox12
        '
        Me.TextBox12.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox12.Location = New System.Drawing.Point(981, 308)
        Me.TextBox12.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(191, 51)
        Me.TextBox12.TabIndex = 85
        '
        'txtTimein
        '
        Me.txtTimein.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTimein.Location = New System.Drawing.Point(188, 219)
        Me.txtTimein.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtTimein.Name = "txtTimein"
        Me.txtTimein.Size = New System.Drawing.Size(236, 51)
        Me.txtTimein.TabIndex = 73
        '
        'txtSasom
        '
        Me.txtSasom.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtSasom.Location = New System.Drawing.Point(1108, 222)
        Me.txtSasom.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtSasom.Name = "txtSasom"
        Me.txtSasom.Size = New System.Drawing.Size(185, 51)
        Me.txtSasom.TabIndex = 74
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.Location = New System.Drawing.Point(892, 225)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(189, 43)
        Me.Label6.TabIndex = 75
        Me.Label6.Text = "เวลาสะสมคงเหลือ :"
        '
        'dgvLate
        '
        Me.dgvLate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLate.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column3, Me.Column5, Me.Column7, Me.Column8, Me.Column9, Me.Column4})
        Me.dgvLate.Location = New System.Drawing.Point(19, 665)
        Me.dgvLate.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvLate.Name = "dgvLate"
        Me.dgvLate.RowTemplate.Height = 28
        Me.dgvLate.Size = New System.Drawing.Size(1588, 636)
        Me.dgvLate.TabIndex = 94
        '
        'Column3
        '
        Me.Column3.HeaderText = "ว/ด/ป ขอเข้าสาย"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 250
        '
        'Column5
        '
        Me.Column5.HeaderText = "เวลาขอเข้าสาย"
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 250
        '
        'Column7
        '
        Me.Column7.HeaderText = "รวมครั้งนี้"
        Me.Column7.Name = "Column7"
        Me.Column7.Width = 200
        '
        'Column8
        '
        Me.Column8.HeaderText = "เลขที่หนังสือ"
        Me.Column8.Name = "Column8"
        Me.Column8.Width = 250
        '
        'Column9
        '
        Me.Column9.HeaderText = "หมายเหตุ"
        Me.Column9.Name = "Column9"
        Me.Column9.Width = 200
        '
        'Column4
        '
        Me.Column4.FillWeight = 120.0!
        Me.Column4.HeaderText = "วันเดือนปีที่บันทึก"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 250
        '
        'frmLate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1762, 1479)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmLate"
        Me.Text = "frmLate"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvLate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents dgvLate As DataGridView
    Friend WithEvents Label16 As Label
    Friend WithEvents txtComment As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txtNum As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents TextBox12 As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents dtpLatedate As DateTimePicker
    Friend WithEvents Label6 As Label
    Friend WithEvents txtSasom As TextBox
    Friend WithEvents txtTimein As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtTimeout As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents butSearch As Button
    Friend WithEvents txtId As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txtLname As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtFname As TextBox
    Friend WithEvents txtDep As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtSumlate As TextBox
    Friend WithEvents txtPosition As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents dtpLatetime As DateTimePicker
    Friend WithEvents butSave As Button
    Friend WithEvents butEdit As Button
    Friend WithEvents butDel As Button
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents Column9 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
End Class
