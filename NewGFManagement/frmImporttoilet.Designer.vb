﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImporttoilet
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.butSave = New System.Windows.Forms.Button()
        Me.dgvImporttoilet = New System.Windows.Forms.DataGridView()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pgbImport = New System.Windows.Forms.ProgressBar()
        Me.butImport = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvImporttoilet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.butSave)
        Me.GroupBox1.Controls.Add(Me.dgvImporttoilet)
        Me.GroupBox1.Controls.Add(Me.pgbImport)
        Me.GroupBox1.Controls.Add(Me.butImport)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(25, 32)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1886, 808)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "นำเข้าข้อมูลการใช้ห้องน้ำ"
        '
        'butSave
        '
        Me.butSave.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSave.Location = New System.Drawing.Point(706, 720)
        Me.butSave.Margin = New System.Windows.Forms.Padding(4)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(288, 63)
        Me.butSave.TabIndex = 5
        Me.butSave.Text = "บันทึกข้อมูล"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'dgvImporttoilet
        '
        Me.dgvImporttoilet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvImporttoilet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.Column3, Me.Column4, Me.Column6, Me.Column8})
        Me.dgvImporttoilet.Location = New System.Drawing.Point(42, 190)
        Me.dgvImporttoilet.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvImporttoilet.Name = "dgvImporttoilet"
        Me.dgvImporttoilet.RowTemplate.Height = 33
        Me.dgvImporttoilet.Size = New System.Drawing.Size(1785, 477)
        Me.dgvImporttoilet.TabIndex = 4
        '
        'Column2
        '
        Me.Column2.HeaderText = "รหัสพนักงาน"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 250
        '
        'Column3
        '
        Me.Column3.HeaderText = "ว้นเดือนปีเข้า"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 250
        '
        'Column4
        '
        Me.Column4.HeaderText = "เวลาเข้า"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 200
        '
        'Column6
        '
        Me.Column6.HeaderText = "เวลาออก"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 200
        '
        'Column8
        '
        Me.Column8.FillWeight = 120.0!
        Me.Column8.HeaderText = "วันเดิอนปีที่บันทึก"
        Me.Column8.Name = "Column8"
        Me.Column8.Width = 300
        '
        'pgbImport
        '
        Me.pgbImport.Location = New System.Drawing.Point(22, 142)
        Me.pgbImport.Margin = New System.Windows.Forms.Padding(4)
        Me.pgbImport.Name = "pgbImport"
        Me.pgbImport.Size = New System.Drawing.Size(1844, 19)
        Me.pgbImport.TabIndex = 3
        '
        'butImport
        '
        Me.butImport.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butImport.Location = New System.Drawing.Point(753, 51)
        Me.butImport.Margin = New System.Windows.Forms.Padding(4)
        Me.butImport.Name = "butImport"
        Me.butImport.Size = New System.Drawing.Size(288, 65)
        Me.butImport.TabIndex = 2
        Me.butImport.Text = "นำเข้าข้อมูลการใช้ห้องน้ำ"
        Me.butImport.UseVisualStyleBackColor = True
        '
        'frmImporttoilet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1998, 916)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmImporttoilet"
        Me.Text = "frmImporttoilet"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvImporttoilet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents butSave As Button
    Friend WithEvents dgvImporttoilet As DataGridView
    Friend WithEvents pgbImport As ProgressBar
    Friend WithEvents butImport As Button
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
End Class
