﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmEmpmonth
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox27 = New System.Windows.Forms.GroupBox()
        Me.txtAnothersnew = New System.Windows.Forms.TextBox()
        Me.chkAnothersnew = New System.Windows.Forms.CheckBox()
        Me.chkEmpOffice = New System.Windows.Forms.CheckBox()
        Me.chkAnnouncement = New System.Windows.Forms.CheckBox()
        Me.chkSignsrecruit = New System.Windows.Forms.CheckBox()
        Me.chkInternet = New System.Windows.Forms.CheckBox()
        Me.chkFriend = New System.Windows.Forms.CheckBox()
        Me.butClear4 = New System.Windows.Forms.Button()
        Me.butDel4 = New System.Windows.Forms.Button()
        Me.butSave4 = New System.Windows.Forms.Button()
        Me.GroupBox28 = New System.Windows.Forms.GroupBox()
        Me.dtpReady = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox26 = New System.Windows.Forms.GroupBox()
        Me.txtReltionincom = New System.Windows.Forms.TextBox()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.txtLnameincom = New System.Windows.Forms.TextBox()
        Me.txtFnameincom = New System.Windows.Forms.TextBox()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.GroupBox25 = New System.Windows.Forms.GroupBox()
        Me.chkGuarantorno = New System.Windows.Forms.CheckBox()
        Me.chkGuarantor = New System.Windows.Forms.CheckBox()
        Me.GroupBox24 = New System.Windows.Forms.GroupBox()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.txtTypingeng = New System.Windows.Forms.TextBox()
        Me.chkTypingeng = New System.Windows.Forms.CheckBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.txtTypingth = New System.Windows.Forms.TextBox()
        Me.chkTypingth = New System.Windows.Forms.CheckBox()
        Me.txtAnotherscom = New System.Windows.Forms.TextBox()
        Me.chkAnotherscom = New System.Windows.Forms.CheckBox()
        Me.chkPowerpoint = New System.Windows.Forms.CheckBox()
        Me.chkExcel = New System.Windows.Forms.CheckBox()
        Me.chkWord = New System.Windows.Forms.CheckBox()
        Me.GroupBox23 = New System.Windows.Forms.GroupBox()
        Me.butDelref = New System.Windows.Forms.Button()
        Me.butSaveref = New System.Windows.Forms.Button()
        Me.cboTitleref = New System.Windows.Forms.ComboBox()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.txtPhoneref = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.dgvRef = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtPositionref = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.txtAddressref = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.txtRelationref = New System.Windows.Forms.TextBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.txtLnameref = New System.Windows.Forms.TextBox()
        Me.txtFnameref = New System.Windows.Forms.TextBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.butClear3 = New System.Windows.Forms.Button()
        Me.butDel3 = New System.Windows.Forms.Button()
        Me.butSave3 = New System.Windows.Forms.Button()
        Me.GroupBox20 = New System.Windows.Forms.GroupBox()
        Me.butLangdel = New System.Windows.Forms.Button()
        Me.butLangsave = New System.Windows.Forms.Button()
        Me.GroupBox31 = New System.Windows.Forms.GroupBox()
        Me.rdbSpeakC = New System.Windows.Forms.RadioButton()
        Me.rdbSpeakB = New System.Windows.Forms.RadioButton()
        Me.rdbSpeakA = New System.Windows.Forms.RadioButton()
        Me.GroupBox34 = New System.Windows.Forms.GroupBox()
        Me.rdbWriteC = New System.Windows.Forms.RadioButton()
        Me.rdbWriteB = New System.Windows.Forms.RadioButton()
        Me.rdbWriteA = New System.Windows.Forms.RadioButton()
        Me.GroupBox33 = New System.Windows.Forms.GroupBox()
        Me.rdbReadC = New System.Windows.Forms.RadioButton()
        Me.rdbReadB = New System.Windows.Forms.RadioButton()
        Me.rdbReadA = New System.Windows.Forms.RadioButton()
        Me.GroupBox32 = New System.Windows.Forms.GroupBox()
        Me.rdbUnderstandC = New System.Windows.Forms.RadioButton()
        Me.rdbUnderstandB = New System.Windows.Forms.RadioButton()
        Me.rdbUnderstandA = New System.Windows.Forms.RadioButton()
        Me.dgvLanguage = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cboLanguage = New System.Windows.Forms.ComboBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.GroupBox22 = New System.Windows.Forms.GroupBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.txtBad = New System.Windows.Forms.TextBox()
        Me.txtGood = New System.Windows.Forms.TextBox()
        Me.GroupBox21 = New System.Windows.Forms.GroupBox()
        Me.butWorkhisdel = New System.Windows.Forms.Button()
        Me.butWorkhissave = New System.Windows.Forms.Button()
        Me.txtIncome = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.dgvWorkhis = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtLastsalary = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.txtPosition = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.txtEnd = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.txtStart = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.txtOfficename = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.butClear2 = New System.Windows.Forms.Button()
        Me.GroupBox36 = New System.Windows.Forms.GroupBox()
        Me.GroupBox17 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboTitlemother = New System.Windows.Forms.ComboBox()
        Me.chkDeadmother = New System.Windows.Forms.CheckBox()
        Me.txtAgemother = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtJobmother = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtFnamemother = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.GroupBox16 = New System.Windows.Forms.GroupBox()
        Me.cboTitlefather = New System.Windows.Forms.ComboBox()
        Me.chkDeadfather = New System.Windows.Forms.CheckBox()
        Me.txtAgefather = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtJobfather = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtFnamefather = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboTitlemarry = New System.Windows.Forms.ComboBox()
        Me.txtOfficezipcode = New System.Windows.Forms.TextBox()
        Me.txtOfficeProvince = New System.Windows.Forms.TextBox()
        Me.txtOfficecanton = New System.Windows.Forms.TextBox()
        Me.txtPhonemarry = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtOfficemarry = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtOfficedistrict = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtOfficeaddress = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtOfficemoo = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtJobmarry = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtFnamemarry = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.butDel2 = New System.Windows.Forms.Button()
        Me.GroupBox35 = New System.Windows.Forms.GroupBox()
        Me.GroupBox15 = New System.Windows.Forms.GroupBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtChildlearning = New System.Windows.Forms.TextBox()
        Me.GroupBox14 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtChild = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.rdbEarningno = New System.Windows.Forms.RadioButton()
        Me.rdbEarning = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rdbRegisno = New System.Windows.Forms.RadioButton()
        Me.rdbRegis = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rdbSeparate = New System.Windows.Forms.RadioButton()
        Me.rdbWidowed = New System.Windows.Forms.RadioButton()
        Me.rdbDivorce = New System.Windows.Forms.RadioButton()
        Me.rdbMarry = New System.Windows.Forms.RadioButton()
        Me.rdbSingle = New System.Windows.Forms.RadioButton()
        Me.GroupBox18 = New System.Windows.Forms.GroupBox()
        Me.rdbAnotherssol = New System.Windows.Forms.RadioButton()
        Me.rdbPMilitary = New System.Windows.Forms.RadioButton()
        Me.rdbEducate = New System.Windows.Forms.RadioButton()
        Me.rdbExcept = New System.Windows.Forms.RadioButton()
        Me.txtAnotherssol = New System.Windows.Forms.TextBox()
        Me.butSave2 = New System.Windows.Forms.Button()
        Me.GroupBox19 = New System.Windows.Forms.GroupBox()
        Me.cboEdulevel = New System.Windows.Forms.ComboBox()
        Me.dgvEdu = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtEdugrade = New System.Windows.Forms.TextBox()
        Me.butEdudel = New System.Windows.Forms.Button()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.txtEdumajor = New System.Windows.Forms.TextBox()
        Me.butEdusave = New System.Windows.Forms.Button()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txtEduend = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.txtEdustart = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.txtEduname = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.butClear1 = New System.Windows.Forms.Button()
        Me.GroupBox37 = New System.Windows.Forms.GroupBox()
        Me.rdbContain = New System.Windows.Forms.RadioButton()
        Me.rdbTrial = New System.Windows.Forms.RadioButton()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.rdbSicknessno = New System.Windows.Forms.RadioButton()
        Me.rdbSickness = New System.Windows.Forms.RadioButton()
        Me.txtSickness = New System.Windows.Forms.TextBox()
        Me.butDel1 = New System.Windows.Forms.Button()
        Me.GroupBox30 = New System.Windows.Forms.GroupBox()
        Me.txtByprovince = New System.Windows.Forms.TextBox()
        Me.txtBycanton = New System.Windows.Forms.TextBox()
        Me.dtpBirthday = New System.Windows.Forms.DateTimePicker()
        Me.dtpEndidcard = New System.Windows.Forms.DateTimePicker()
        Me.dtpStartidcard = New System.Windows.Forms.DateTimePicker()
        Me.cboEducation = New System.Windows.Forms.ComboBox()
        Me.lblEducation = New System.Windows.Forms.Label()
        Me.lblM = New System.Windows.Forms.Label()
        Me.lblIssuedatdistrict = New System.Windows.Forms.Label()
        Me.lblKg = New System.Windows.Forms.Label()
        Me.lblIssuedatprovince = New System.Windows.Forms.Label()
        Me.lblYear = New System.Windows.Forms.Label()
        Me.txtNickname = New System.Windows.Forms.TextBox()
        Me.lblNickname = New System.Windows.Forms.Label()
        Me.cboBlood = New System.Windows.Forms.ComboBox()
        Me.lblGblood = New System.Windows.Forms.Label()
        Me.lblEndcard = New System.Windows.Forms.Label()
        Me.txtHeight = New System.Windows.Forms.TextBox()
        Me.lblStartcard = New System.Windows.Forms.Label()
        Me.lblHeight = New System.Windows.Forms.Label()
        Me.txtWeight = New System.Windows.Forms.TextBox()
        Me.txtIdcard = New System.Windows.Forms.TextBox()
        Me.txtAge = New System.Windows.Forms.TextBox()
        Me.lblWeight = New System.Windows.Forms.Label()
        Me.lblIdcard = New System.Windows.Forms.Label()
        Me.lblAge = New System.Windows.Forms.Label()
        Me.cboReligion = New System.Windows.Forms.ComboBox()
        Me.lblReligion = New System.Windows.Forms.Label()
        Me.cboNationality = New System.Windows.Forms.ComboBox()
        Me.lblNationality = New System.Windows.Forms.Label()
        Me.lblBirthday = New System.Windows.Forms.Label()
        Me.txtLname = New System.Windows.Forms.TextBox()
        Me.txtFname = New System.Windows.Forms.TextBox()
        Me.cboTitle = New System.Windows.Forms.ComboBox()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblEmployeeData = New System.Windows.Forms.Label()
        Me.butSave1 = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtProvince = New System.Windows.Forms.TextBox()
        Me.txtDistrict = New System.Windows.Forms.TextBox()
        Me.txtCanton = New System.Windows.Forms.TextBox()
        Me.txtZipcode = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtMobile = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtMoo = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.txtNowprovince = New System.Windows.Forms.TextBox()
        Me.txtNowdistrict = New System.Windows.Forms.TextBox()
        Me.txtNowphone = New System.Windows.Forms.TextBox()
        Me.txtNowcanton = New System.Windows.Forms.TextBox()
        Me.lblPostcode = New System.Windows.Forms.Label()
        Me.lblTelephone = New System.Windows.Forms.Label()
        Me.txtSmartphone = New System.Windows.Forms.TextBox()
        Me.txtNowaddress = New System.Windows.Forms.TextBox()
        Me.lblProvince = New System.Windows.Forms.Label()
        Me.lblDistrict = New System.Windows.Forms.Label()
        Me.lblSubdistrict = New System.Windows.Forms.Label()
        Me.txtNowmoo = New System.Windows.Forms.TextBox()
        Me.lblNum = New System.Windows.Forms.Label()
        Me.lblMu = New System.Windows.Forms.Label()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.RadioButton10 = New System.Windows.Forms.RadioButton()
        Me.rdbMan = New System.Windows.Forms.RadioButton()
        Me.GroupBox29 = New System.Windows.Forms.GroupBox()
        Me.dtpContain = New System.Windows.Forms.DateTimePicker()
        Me.dtpStart = New System.Windows.Forms.DateTimePicker()
        Me.dtpRegis = New System.Windows.Forms.DateTimePicker()
        Me.txtAccountnum = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboDep = New System.Windows.Forms.ComboBox()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.txtWage = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboPosition = New System.Windows.Forms.ComboBox()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.chkIdcard = New System.Windows.Forms.CheckBox()
        Me.chkRegisHome = New System.Windows.Forms.CheckBox()
        Me.chkAnother = New System.Windows.Forms.CheckBox()
        Me.chkWork = New System.Windows.Forms.CheckBox()
        Me.chkDrive = New System.Windows.Forms.CheckBox()
        Me.chkMotorcyc = New System.Windows.Forms.CheckBox()
        Me.chkMarry = New System.Windows.Forms.CheckBox()
        Me.chkMilitary = New System.Windows.Forms.CheckBox()
        Me.chkMedicalcer = New System.Windows.Forms.CheckBox()
        Me.chkEducation = New System.Windows.Forms.CheckBox()
        Me.chkPicture = New System.Windows.Forms.CheckBox()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.rdbCigaretteno = New System.Windows.Forms.RadioButton()
        Me.rdbCigarette = New System.Windows.Forms.RadioButton()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.rdbGlassesno = New System.Windows.Forms.RadioButton()
        Me.rdbGlasses = New System.Windows.Forms.RadioButton()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.rdbPregnantno = New System.Windows.Forms.RadioButton()
        Me.rdbPregnant = New System.Windows.Forms.RadioButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.rdbOthershome = New System.Windows.Forms.RadioButton()
        Me.rdbMyhome = New System.Windows.Forms.RadioButton()
        Me.rdbFatherhome = New System.Windows.Forms.RadioButton()
        Me.rdbLeasehome = New System.Windows.Forms.RadioButton()
        Me.picOneinch = New System.Windows.Forms.PictureBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox27.SuspendLayout()
        Me.GroupBox28.SuspendLayout()
        Me.GroupBox26.SuspendLayout()
        Me.GroupBox25.SuspendLayout()
        Me.GroupBox24.SuspendLayout()
        Me.GroupBox23.SuspendLayout()
        CType(Me.dgvRef, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox20.SuspendLayout()
        Me.GroupBox31.SuspendLayout()
        Me.GroupBox34.SuspendLayout()
        Me.GroupBox33.SuspendLayout()
        Me.GroupBox32.SuspendLayout()
        CType(Me.dgvLanguage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox22.SuspendLayout()
        Me.GroupBox21.SuspendLayout()
        CType(Me.dgvWorkhis, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox36.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.GroupBox35.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox18.SuspendLayout()
        Me.GroupBox19.SuspendLayout()
        CType(Me.dgvEdu, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox37.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox30.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox29.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.picOneinch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.GroupBox27)
        Me.TabPage4.Controls.Add(Me.butClear4)
        Me.TabPage4.Controls.Add(Me.butDel4)
        Me.TabPage4.Controls.Add(Me.butSave4)
        Me.TabPage4.Controls.Add(Me.GroupBox28)
        Me.TabPage4.Controls.Add(Me.GroupBox26)
        Me.TabPage4.Controls.Add(Me.GroupBox25)
        Me.TabPage4.Controls.Add(Me.GroupBox24)
        Me.TabPage4.Controls.Add(Me.GroupBox23)
        Me.TabPage4.Font = New System.Drawing.Font("Angsana New", 16.125!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TabPage4.Location = New System.Drawing.Point(8, 39)
        Me.TabPage4.Margin = New System.Windows.Forms.Padding(4)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(4)
        Me.TabPage4.Size = New System.Drawing.Size(2315, 1484)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "หน้า4"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'GroupBox27
        '
        Me.GroupBox27.Controls.Add(Me.txtAnothersnew)
        Me.GroupBox27.Controls.Add(Me.chkAnothersnew)
        Me.GroupBox27.Controls.Add(Me.chkEmpOffice)
        Me.GroupBox27.Controls.Add(Me.chkAnnouncement)
        Me.GroupBox27.Controls.Add(Me.chkSignsrecruit)
        Me.GroupBox27.Controls.Add(Me.chkInternet)
        Me.GroupBox27.Controls.Add(Me.chkFriend)
        Me.GroupBox27.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox27.Location = New System.Drawing.Point(17, 746)
        Me.GroupBox27.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox27.Name = "GroupBox27"
        Me.GroupBox27.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox27.Size = New System.Drawing.Size(988, 142)
        Me.GroupBox27.TabIndex = 121
        Me.GroupBox27.TabStop = False
        Me.GroupBox27.Text = "ท่านทราบข่าวการรับสมัครงานจากที่ไหน"
        '
        'txtAnothersnew
        '
        Me.txtAnothersnew.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtAnothersnew.Location = New System.Drawing.Point(774, 83)
        Me.txtAnothersnew.Margin = New System.Windows.Forms.Padding(6)
        Me.txtAnothersnew.Name = "txtAnothersnew"
        Me.txtAnothersnew.Size = New System.Drawing.Size(204, 51)
        Me.txtAnothersnew.TabIndex = 20
        '
        'chkAnothersnew
        '
        Me.chkAnothersnew.AutoSize = True
        Me.chkAnothersnew.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkAnothersnew.Location = New System.Drawing.Point(588, 87)
        Me.chkAnothersnew.Margin = New System.Windows.Forms.Padding(6)
        Me.chkAnothersnew.Name = "chkAnothersnew"
        Me.chkAnothersnew.Size = New System.Drawing.Size(193, 47)
        Me.chkAnothersnew.TabIndex = 19
        Me.chkAnothersnew.Text = "อื่นๆโปรดระบุ..."
        Me.chkAnothersnew.UseVisualStyleBackColor = True
        '
        'chkEmpOffice
        '
        Me.chkEmpOffice.AutoSize = True
        Me.chkEmpOffice.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkEmpOffice.Location = New System.Drawing.Point(16, 87)
        Me.chkEmpOffice.Margin = New System.Windows.Forms.Padding(6)
        Me.chkEmpOffice.Name = "chkEmpOffice"
        Me.chkEmpOffice.Size = New System.Drawing.Size(218, 47)
        Me.chkEmpOffice.TabIndex = 17
        Me.chkEmpOffice.Text = "สำนักงานจัดหางาน"
        Me.chkEmpOffice.UseVisualStyleBackColor = True
        '
        'chkAnnouncement
        '
        Me.chkAnnouncement.AutoSize = True
        Me.chkAnnouncement.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkAnnouncement.Location = New System.Drawing.Point(262, 87)
        Me.chkAnnouncement.Margin = New System.Windows.Forms.Padding(6)
        Me.chkAnnouncement.Name = "chkAnnouncement"
        Me.chkAnnouncement.Size = New System.Drawing.Size(226, 47)
        Me.chkAnnouncement.TabIndex = 18
        Me.chkAnnouncement.Text = "เสียงตามสายหมู่บ้าน"
        Me.chkAnnouncement.UseVisualStyleBackColor = True
        '
        'chkSignsrecruit
        '
        Me.chkSignsrecruit.AutoSize = True
        Me.chkSignsrecruit.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkSignsrecruit.Location = New System.Drawing.Point(262, 40)
        Me.chkSignsrecruit.Margin = New System.Windows.Forms.Padding(6)
        Me.chkSignsrecruit.Name = "chkSignsrecruit"
        Me.chkSignsrecruit.Size = New System.Drawing.Size(287, 47)
        Me.chkSignsrecruit.TabIndex = 15
        Me.chkSignsrecruit.Text = "ป้ายรับสมัครงานหน้าบริษัท"
        Me.chkSignsrecruit.UseVisualStyleBackColor = True
        '
        'chkInternet
        '
        Me.chkInternet.AutoSize = True
        Me.chkInternet.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkInternet.Location = New System.Drawing.Point(16, 40)
        Me.chkInternet.Margin = New System.Windows.Forms.Padding(6)
        Me.chkInternet.Name = "chkInternet"
        Me.chkInternet.Size = New System.Drawing.Size(139, 47)
        Me.chkInternet.TabIndex = 14
        Me.chkInternet.Text = "อิเตอร์เน็ต"
        Me.chkInternet.UseVisualStyleBackColor = True
        '
        'chkFriend
        '
        Me.chkFriend.AutoSize = True
        Me.chkFriend.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkFriend.Location = New System.Drawing.Point(588, 40)
        Me.chkFriend.Margin = New System.Windows.Forms.Padding(6)
        Me.chkFriend.Name = "chkFriend"
        Me.chkFriend.Size = New System.Drawing.Size(252, 47)
        Me.chkFriend.TabIndex = 16
        Me.chkFriend.Text = "จากเพื่อนหรือบุคคลอื่น"
        Me.chkFriend.UseVisualStyleBackColor = True
        '
        'butClear4
        '
        Me.butClear4.Location = New System.Drawing.Point(1072, 956)
        Me.butClear4.Margin = New System.Windows.Forms.Padding(6)
        Me.butClear4.Name = "butClear4"
        Me.butClear4.Size = New System.Drawing.Size(160, 58)
        Me.butClear4.TabIndex = 118
        Me.butClear4.Text = "เคลียร์"
        Me.butClear4.UseVisualStyleBackColor = True
        '
        'butDel4
        '
        Me.butDel4.Location = New System.Drawing.Point(864, 956)
        Me.butDel4.Margin = New System.Windows.Forms.Padding(6)
        Me.butDel4.Name = "butDel4"
        Me.butDel4.Size = New System.Drawing.Size(160, 58)
        Me.butDel4.TabIndex = 120
        Me.butDel4.Text = "ลบ"
        Me.butDel4.UseVisualStyleBackColor = True
        '
        'butSave4
        '
        Me.butSave4.Location = New System.Drawing.Point(663, 956)
        Me.butSave4.Margin = New System.Windows.Forms.Padding(6)
        Me.butSave4.Name = "butSave4"
        Me.butSave4.Size = New System.Drawing.Size(160, 58)
        Me.butSave4.TabIndex = 119
        Me.butSave4.Text = "บันทึก"
        Me.butSave4.UseVisualStyleBackColor = True
        '
        'GroupBox28
        '
        Me.GroupBox28.Controls.Add(Me.dtpReady)
        Me.GroupBox28.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox28.Location = New System.Drawing.Point(1488, 604)
        Me.GroupBox28.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox28.Name = "GroupBox28"
        Me.GroupBox28.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox28.Size = New System.Drawing.Size(499, 125)
        Me.GroupBox28.TabIndex = 52
        Me.GroupBox28.TabStop = False
        Me.GroupBox28.Text = "ท่านพร้อมจะปฎิบัติงานกับบิษัทได้ในวันที่"
        '
        'dtpReady
        '
        Me.dtpReady.Font = New System.Drawing.Font("AngsanaUPC", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpReady.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpReady.Location = New System.Drawing.Point(125, 54)
        Me.dtpReady.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.dtpReady.Name = "dtpReady"
        Me.dtpReady.Size = New System.Drawing.Size(199, 51)
        Me.dtpReady.TabIndex = 161
        '
        'GroupBox26
        '
        Me.GroupBox26.Controls.Add(Me.txtReltionincom)
        Me.GroupBox26.Controls.Add(Me.Label69)
        Me.GroupBox26.Controls.Add(Me.txtLnameincom)
        Me.GroupBox26.Controls.Add(Me.txtFnameincom)
        Me.GroupBox26.Controls.Add(Me.Label68)
        Me.GroupBox26.Controls.Add(Me.Label67)
        Me.GroupBox26.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox26.Location = New System.Drawing.Point(469, 604)
        Me.GroupBox26.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox26.Name = "GroupBox26"
        Me.GroupBox26.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox26.Size = New System.Drawing.Size(1013, 125)
        Me.GroupBox26.TabIndex = 50
        Me.GroupBox26.TabStop = False
        Me.GroupBox26.Text = "บุคคลในบริษัทที่ท่านรู้จักคุ้นเคย "
        '
        'txtReltionincom
        '
        Me.txtReltionincom.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReltionincom.Location = New System.Drawing.Point(780, 54)
        Me.txtReltionincom.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtReltionincom.Name = "txtReltionincom"
        Me.txtReltionincom.Size = New System.Drawing.Size(200, 51)
        Me.txtReltionincom.TabIndex = 160
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.Location = New System.Drawing.Point(624, 60)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(145, 43)
        Me.Label69.TabIndex = 82
        Me.Label69.Text = "ความสัมพันธ์ :"
        '
        'txtLnameincom
        '
        Me.txtLnameincom.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLnameincom.Location = New System.Drawing.Point(412, 54)
        Me.txtLnameincom.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtLnameincom.Name = "txtLnameincom"
        Me.txtLnameincom.Size = New System.Drawing.Size(200, 51)
        Me.txtLnameincom.TabIndex = 159
        '
        'txtFnameincom
        '
        Me.txtFnameincom.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFnameincom.Location = New System.Drawing.Point(84, 54)
        Me.txtFnameincom.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtFnameincom.Name = "txtFnameincom"
        Me.txtFnameincom.Size = New System.Drawing.Size(200, 51)
        Me.txtFnameincom.TabIndex = 158
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label68.Location = New System.Drawing.Point(296, 60)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(107, 43)
        Me.Label68.TabIndex = 1
        Me.Label68.Text = "นามสกุล :"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.Location = New System.Drawing.Point(20, 60)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(56, 43)
        Me.Label67.TabIndex = 0
        Me.Label67.Text = "ชื่อ :"
        '
        'GroupBox25
        '
        Me.GroupBox25.Controls.Add(Me.chkGuarantorno)
        Me.GroupBox25.Controls.Add(Me.chkGuarantor)
        Me.GroupBox25.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox25.Location = New System.Drawing.Point(11, 604)
        Me.GroupBox25.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox25.Name = "GroupBox25"
        Me.GroupBox25.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox25.Size = New System.Drawing.Size(456, 125)
        Me.GroupBox25.TabIndex = 49
        Me.GroupBox25.TabStop = False
        Me.GroupBox25.Text = "ในตำแหน่งงานที่ต้องมีการค้ำประกัน"
        '
        'chkGuarantorno
        '
        Me.chkGuarantorno.AutoSize = True
        Me.chkGuarantorno.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGuarantorno.Location = New System.Drawing.Point(212, 52)
        Me.chkGuarantorno.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkGuarantorno.Name = "chkGuarantorno"
        Me.chkGuarantorno.Size = New System.Drawing.Size(137, 47)
        Me.chkGuarantorno.TabIndex = 157
        Me.chkGuarantorno.Text = "ไม่ขัดข้อง"
        Me.chkGuarantorno.UseVisualStyleBackColor = True
        '
        'chkGuarantor
        '
        Me.chkGuarantor.AutoSize = True
        Me.chkGuarantor.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGuarantor.Location = New System.Drawing.Point(37, 52)
        Me.chkGuarantor.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkGuarantor.Name = "chkGuarantor"
        Me.chkGuarantor.Size = New System.Drawing.Size(113, 47)
        Me.chkGuarantor.TabIndex = 156
        Me.chkGuarantor.Text = "ขัดข้อง"
        Me.chkGuarantor.UseVisualStyleBackColor = True
        '
        'GroupBox24
        '
        Me.GroupBox24.Controls.Add(Me.Label66)
        Me.GroupBox24.Controls.Add(Me.txtTypingeng)
        Me.GroupBox24.Controls.Add(Me.chkTypingeng)
        Me.GroupBox24.Controls.Add(Me.Label63)
        Me.GroupBox24.Controls.Add(Me.txtTypingth)
        Me.GroupBox24.Controls.Add(Me.chkTypingth)
        Me.GroupBox24.Controls.Add(Me.txtAnotherscom)
        Me.GroupBox24.Controls.Add(Me.chkAnotherscom)
        Me.GroupBox24.Controls.Add(Me.chkPowerpoint)
        Me.GroupBox24.Controls.Add(Me.chkExcel)
        Me.GroupBox24.Controls.Add(Me.chkWord)
        Me.GroupBox24.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox24.Location = New System.Drawing.Point(5, 464)
        Me.GroupBox24.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox24.Name = "GroupBox24"
        Me.GroupBox24.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox24.Size = New System.Drawing.Size(1976, 122)
        Me.GroupBox24.TabIndex = 48
        Me.GroupBox24.TabStop = False
        Me.GroupBox24.Text = "ความรู้ด้านคอมพิวเตอร์"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.Location = New System.Drawing.Point(1856, 52)
        Me.Label66.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(85, 43)
        Me.Label66.TabIndex = 85
        Me.Label66.Text = "คำ/นาที"
        '
        'txtTypingeng
        '
        Me.txtTypingeng.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTypingeng.Location = New System.Drawing.Point(1739, 50)
        Me.txtTypingeng.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtTypingeng.Name = "txtTypingeng"
        Me.txtTypingeng.Size = New System.Drawing.Size(103, 41)
        Me.txtTypingeng.TabIndex = 155
        '
        'chkTypingeng
        '
        Me.chkTypingeng.AutoSize = True
        Me.chkTypingeng.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTypingeng.Location = New System.Drawing.Point(1491, 50)
        Me.chkTypingeng.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkTypingeng.Name = "chkTypingeng"
        Me.chkTypingeng.Size = New System.Drawing.Size(233, 47)
        Me.chkTypingeng.TabIndex = 154
        Me.chkTypingeng.Text = "พิมพ์ดีด ภาษาอังกฤษ"
        Me.chkTypingeng.UseVisualStyleBackColor = True
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label63.Location = New System.Drawing.Point(1356, 52)
        Me.Label63.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(85, 43)
        Me.Label63.TabIndex = 82
        Me.Label63.Text = "คำ/นาที"
        '
        'txtTypingth
        '
        Me.txtTypingth.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTypingth.Location = New System.Drawing.Point(1237, 50)
        Me.txtTypingth.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtTypingth.Name = "txtTypingth"
        Me.txtTypingth.Size = New System.Drawing.Size(103, 41)
        Me.txtTypingth.TabIndex = 153
        '
        'chkTypingth
        '
        Me.chkTypingth.AutoSize = True
        Me.chkTypingth.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTypingth.Location = New System.Drawing.Point(1016, 50)
        Me.chkTypingth.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkTypingth.Name = "chkTypingth"
        Me.chkTypingth.Size = New System.Drawing.Size(206, 47)
        Me.chkTypingth.TabIndex = 152
        Me.chkTypingth.Text = "พิมพ์ดีด ภาษาไทย"
        Me.chkTypingth.UseVisualStyleBackColor = True
        '
        'txtAnotherscom
        '
        Me.txtAnotherscom.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtAnotherscom.Location = New System.Drawing.Point(779, 50)
        Me.txtAnotherscom.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtAnotherscom.Name = "txtAnotherscom"
        Me.txtAnotherscom.Size = New System.Drawing.Size(199, 41)
        Me.txtAnotherscom.TabIndex = 151
        '
        'chkAnotherscom
        '
        Me.chkAnotherscom.AutoSize = True
        Me.chkAnotherscom.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAnotherscom.Location = New System.Drawing.Point(596, 50)
        Me.chkAnotherscom.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkAnotherscom.Name = "chkAnotherscom"
        Me.chkAnotherscom.Size = New System.Drawing.Size(183, 47)
        Me.chkAnotherscom.TabIndex = 150
        Me.chkAnotherscom.Text = "อื่นๆ โปรดระบุ"
        Me.chkAnotherscom.UseVisualStyleBackColor = True
        '
        'chkPowerpoint
        '
        Me.chkPowerpoint.AutoSize = True
        Me.chkPowerpoint.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPowerpoint.Location = New System.Drawing.Point(392, 50)
        Me.chkPowerpoint.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkPowerpoint.Name = "chkPowerpoint"
        Me.chkPowerpoint.Size = New System.Drawing.Size(191, 47)
        Me.chkPowerpoint.TabIndex = 149
        Me.chkPowerpoint.Text = "MS Power Point"
        Me.chkPowerpoint.UseVisualStyleBackColor = True
        '
        'chkExcel
        '
        Me.chkExcel.AutoSize = True
        Me.chkExcel.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExcel.Location = New System.Drawing.Point(204, 50)
        Me.chkExcel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkExcel.Name = "chkExcel"
        Me.chkExcel.Size = New System.Drawing.Size(134, 47)
        Me.chkExcel.TabIndex = 148
        Me.chkExcel.Text = "MS Excel"
        Me.chkExcel.UseVisualStyleBackColor = True
        '
        'chkWord
        '
        Me.chkWord.AutoSize = True
        Me.chkWord.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkWord.Location = New System.Drawing.Point(12, 50)
        Me.chkWord.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkWord.Name = "chkWord"
        Me.chkWord.Size = New System.Drawing.Size(136, 47)
        Me.chkWord.TabIndex = 147
        Me.chkWord.Text = "MS Word"
        Me.chkWord.UseVisualStyleBackColor = True
        '
        'GroupBox23
        '
        Me.GroupBox23.Controls.Add(Me.butDelref)
        Me.GroupBox23.Controls.Add(Me.butSaveref)
        Me.GroupBox23.Controls.Add(Me.cboTitleref)
        Me.GroupBox23.Controls.Add(Me.Label65)
        Me.GroupBox23.Controls.Add(Me.txtPhoneref)
        Me.GroupBox23.Controls.Add(Me.Label58)
        Me.GroupBox23.Controls.Add(Me.dgvRef)
        Me.GroupBox23.Controls.Add(Me.txtPositionref)
        Me.GroupBox23.Controls.Add(Me.Label59)
        Me.GroupBox23.Controls.Add(Me.txtAddressref)
        Me.GroupBox23.Controls.Add(Me.Label60)
        Me.GroupBox23.Controls.Add(Me.Label61)
        Me.GroupBox23.Controls.Add(Me.txtRelationref)
        Me.GroupBox23.Controls.Add(Me.Label62)
        Me.GroupBox23.Controls.Add(Me.txtLnameref)
        Me.GroupBox23.Controls.Add(Me.txtFnameref)
        Me.GroupBox23.Controls.Add(Me.Label64)
        Me.GroupBox23.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox23.Location = New System.Drawing.Point(5, 28)
        Me.GroupBox23.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox23.Name = "GroupBox23"
        Me.GroupBox23.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox23.Size = New System.Drawing.Size(1976, 441)
        Me.GroupBox23.TabIndex = 47
        Me.GroupBox23.TabStop = False
        Me.GroupBox23.Text = "บุคคลซึ่งไม่ใช่ญาติซึ่งทราบประวัติของท่านและบริษัทสามารถสอบถามได้"
        '
        'butDelref
        '
        Me.butDelref.Location = New System.Drawing.Point(1014, 175)
        Me.butDelref.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butDelref.Name = "butDelref"
        Me.butDelref.Size = New System.Drawing.Size(288, 52)
        Me.butDelref.TabIndex = 149
        Me.butDelref.Text = "ลบบุคคลอ้างอิง"
        Me.butDelref.UseVisualStyleBackColor = True
        '
        'butSaveref
        '
        Me.butSaveref.Location = New System.Drawing.Point(674, 175)
        Me.butSaveref.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butSaveref.Name = "butSaveref"
        Me.butSaveref.Size = New System.Drawing.Size(275, 52)
        Me.butSaveref.TabIndex = 148
        Me.butSaveref.Text = "เพิ่มบุคคลอ้างอิง"
        Me.butSaveref.UseVisualStyleBackColor = True
        '
        'cboTitleref
        '
        Me.cboTitleref.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTitleref.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboTitleref.FormattingEnabled = True
        Me.cboTitleref.Items.AddRange(New Object() {"นาย", "นาง", "นางสาว", "ว่าที่ร้อยตรี", "Mr.", "Ms.", "Mrs.", ""})
        Me.cboTitleref.Location = New System.Drawing.Point(11, 101)
        Me.cboTitleref.Margin = New System.Windows.Forms.Padding(6)
        Me.cboTitleref.Name = "cboTitleref"
        Me.cboTitleref.Size = New System.Drawing.Size(100, 51)
        Me.cboTitleref.TabIndex = 147
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.Location = New System.Drawing.Point(20, 52)
        Me.Label65.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(109, 43)
        Me.Label65.TabIndex = 88
        Me.Label65.Text = "คำนำหน้า "
        '
        'txtPhoneref
        '
        Me.txtPhoneref.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhoneref.Location = New System.Drawing.Point(1692, 100)
        Me.txtPhoneref.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtPhoneref.Name = "txtPhoneref"
        Me.txtPhoneref.Size = New System.Drawing.Size(263, 51)
        Me.txtPhoneref.TabIndex = 145
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.Location = New System.Drawing.Point(1780, 52)
        Me.Label58.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(96, 43)
        Me.Label58.TabIndex = 86
        Me.Label58.Text = "โทรศัพท์"
        '
        'dgvRef
        '
        Me.dgvRef.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRef.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.Column8})
        Me.dgvRef.Location = New System.Drawing.Point(12, 247)
        Me.dgvRef.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.dgvRef.Name = "dgvRef"
        Me.dgvRef.Size = New System.Drawing.Size(1947, 182)
        Me.dgvRef.TabIndex = 146
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "คำนำหน้า"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 200
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "ชื่อ"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Width = 200
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "นามสกุล"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.Width = 200
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "ความสัมพันธ์"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.Width = 250
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "สถานที่ทำงาน/ที่อยู่"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.Width = 400
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "ตำแหน่ง"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.Width = 200
        '
        'Column8
        '
        Me.Column8.HeaderText = "โทรศัพท์"
        Me.Column8.Name = "Column8"
        Me.Column8.Width = 200
        '
        'txtPositionref
        '
        Me.txtPositionref.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPositionref.Location = New System.Drawing.Point(1491, 100)
        Me.txtPositionref.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtPositionref.Name = "txtPositionref"
        Me.txtPositionref.Size = New System.Drawing.Size(192, 51)
        Me.txtPositionref.TabIndex = 144
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(1544, 52)
        Me.Label59.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(93, 43)
        Me.Label59.TabIndex = 82
        Me.Label59.Text = "ตำแหน่ง"
        '
        'txtAddressref
        '
        Me.txtAddressref.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddressref.Location = New System.Drawing.Point(941, 100)
        Me.txtAddressref.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtAddressref.Name = "txtAddressref"
        Me.txtAddressref.Size = New System.Drawing.Size(532, 51)
        Me.txtAddressref.TabIndex = 143
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.Location = New System.Drawing.Point(1125, 52)
        Me.Label60.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(185, 43)
        Me.Label60.TabIndex = 80
        Me.Label60.Text = "สถานที่ทำงาน/ที่อยู่"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label61.Location = New System.Drawing.Point(525, 52)
        Me.Label61.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(96, 43)
        Me.Label61.TabIndex = 79
        Me.Label61.Text = "นามสกุล"
        '
        'txtRelationref
        '
        Me.txtRelationref.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRelationref.Location = New System.Drawing.Point(708, 102)
        Me.txtRelationref.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtRelationref.Name = "txtRelationref"
        Me.txtRelationref.Size = New System.Drawing.Size(217, 51)
        Me.txtRelationref.TabIndex = 142
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.Location = New System.Drawing.Point(760, 52)
        Me.Label62.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(134, 43)
        Me.Label62.TabIndex = 77
        Me.Label62.Text = "ความสัมพันธ์"
        '
        'txtLnameref
        '
        Me.txtLnameref.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLnameref.Location = New System.Drawing.Point(437, 102)
        Me.txtLnameref.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtLnameref.Name = "txtLnameref"
        Me.txtLnameref.Size = New System.Drawing.Size(256, 51)
        Me.txtLnameref.TabIndex = 141
        '
        'txtFnameref
        '
        Me.txtFnameref.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFnameref.Location = New System.Drawing.Point(128, 100)
        Me.txtFnameref.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtFnameref.Name = "txtFnameref"
        Me.txtFnameref.Size = New System.Drawing.Size(297, 51)
        Me.txtFnameref.TabIndex = 140
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.Location = New System.Drawing.Point(259, 52)
        Me.Label64.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(45, 43)
        Me.Label64.TabIndex = 69
        Me.Label64.Text = "ชื่อ"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.butClear3)
        Me.TabPage3.Controls.Add(Me.butDel3)
        Me.TabPage3.Controls.Add(Me.butSave3)
        Me.TabPage3.Controls.Add(Me.GroupBox20)
        Me.TabPage3.Controls.Add(Me.GroupBox22)
        Me.TabPage3.Controls.Add(Me.GroupBox21)
        Me.TabPage3.Font = New System.Drawing.Font("Angsana New", 16.125!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TabPage3.Location = New System.Drawing.Point(8, 39)
        Me.TabPage3.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.TabPage3.Size = New System.Drawing.Size(2315, 1484)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "หน้า3"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'butClear3
        '
        Me.butClear3.Location = New System.Drawing.Point(1092, 1397)
        Me.butClear3.Margin = New System.Windows.Forms.Padding(6)
        Me.butClear3.Name = "butClear3"
        Me.butClear3.Size = New System.Drawing.Size(160, 58)
        Me.butClear3.TabIndex = 173
        Me.butClear3.Text = "เคลียร์"
        Me.butClear3.UseVisualStyleBackColor = True
        '
        'butDel3
        '
        Me.butDel3.Location = New System.Drawing.Point(884, 1397)
        Me.butDel3.Margin = New System.Windows.Forms.Padding(6)
        Me.butDel3.Name = "butDel3"
        Me.butDel3.Size = New System.Drawing.Size(160, 58)
        Me.butDel3.TabIndex = 175
        Me.butDel3.Text = "ลบ"
        Me.butDel3.UseVisualStyleBackColor = True
        '
        'butSave3
        '
        Me.butSave3.Location = New System.Drawing.Point(683, 1397)
        Me.butSave3.Margin = New System.Windows.Forms.Padding(6)
        Me.butSave3.Name = "butSave3"
        Me.butSave3.Size = New System.Drawing.Size(160, 58)
        Me.butSave3.TabIndex = 174
        Me.butSave3.Text = "บันทึก"
        Me.butSave3.UseVisualStyleBackColor = True
        '
        'GroupBox20
        '
        Me.GroupBox20.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox20.Controls.Add(Me.butLangdel)
        Me.GroupBox20.Controls.Add(Me.butLangsave)
        Me.GroupBox20.Controls.Add(Me.GroupBox31)
        Me.GroupBox20.Controls.Add(Me.GroupBox34)
        Me.GroupBox20.Controls.Add(Me.GroupBox33)
        Me.GroupBox20.Controls.Add(Me.GroupBox32)
        Me.GroupBox20.Controls.Add(Me.dgvLanguage)
        Me.GroupBox20.Controls.Add(Me.cboLanguage)
        Me.GroupBox20.Controls.Add(Me.Label50)
        Me.GroupBox20.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.GroupBox20.Location = New System.Drawing.Point(16, 11)
        Me.GroupBox20.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox20.Name = "GroupBox20"
        Me.GroupBox20.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox20.Size = New System.Drawing.Size(1976, 581)
        Me.GroupBox20.TabIndex = 172
        Me.GroupBox20.TabStop = False
        Me.GroupBox20.Text = "ภาษา"
        '
        'butLangdel
        '
        Me.butLangdel.Location = New System.Drawing.Point(1007, 219)
        Me.butLangdel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butLangdel.Name = "butLangdel"
        Me.butLangdel.Size = New System.Drawing.Size(288, 52)
        Me.butLangdel.TabIndex = 126
        Me.butLangdel.Text = "ลบทักษะภาษา"
        Me.butLangdel.UseVisualStyleBackColor = True
        '
        'butLangsave
        '
        Me.butLangsave.Location = New System.Drawing.Point(667, 219)
        Me.butLangsave.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butLangsave.Name = "butLangsave"
        Me.butLangsave.Size = New System.Drawing.Size(275, 52)
        Me.butLangsave.TabIndex = 125
        Me.butLangsave.Text = "เพิ่มทักษะภาษา"
        Me.butLangsave.UseVisualStyleBackColor = True
        '
        'GroupBox31
        '
        Me.GroupBox31.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.GroupBox31.Controls.Add(Me.rdbSpeakC)
        Me.GroupBox31.Controls.Add(Me.rdbSpeakB)
        Me.GroupBox31.Controls.Add(Me.rdbSpeakA)
        Me.GroupBox31.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox31.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox31.Location = New System.Drawing.Point(396, 36)
        Me.GroupBox31.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox31.Name = "GroupBox31"
        Me.GroupBox31.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox31.Size = New System.Drawing.Size(349, 135)
        Me.GroupBox31.TabIndex = 104
        Me.GroupBox31.TabStop = False
        Me.GroupBox31.Text = "การพูด"
        '
        'rdbSpeakC
        '
        Me.rdbSpeakC.AutoSize = True
        Me.rdbSpeakC.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbSpeakC.Location = New System.Drawing.Point(224, 60)
        Me.rdbSpeakC.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbSpeakC.Name = "rdbSpeakC"
        Me.rdbSpeakC.Size = New System.Drawing.Size(101, 47)
        Me.rdbSpeakC.TabIndex = 103
        Me.rdbSpeakC.TabStop = True
        Me.rdbSpeakC.Text = "พอใช้"
        Me.rdbSpeakC.UseVisualStyleBackColor = True
        '
        'rdbSpeakB
        '
        Me.rdbSpeakB.AutoSize = True
        Me.rdbSpeakB.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbSpeakB.Location = New System.Drawing.Point(140, 60)
        Me.rdbSpeakB.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbSpeakB.Name = "rdbSpeakB"
        Me.rdbSpeakB.Size = New System.Drawing.Size(63, 47)
        Me.rdbSpeakB.TabIndex = 102
        Me.rdbSpeakB.TabStop = True
        Me.rdbSpeakB.Text = "ดี"
        Me.rdbSpeakB.UseVisualStyleBackColor = True
        '
        'rdbSpeakA
        '
        Me.rdbSpeakA.AutoSize = True
        Me.rdbSpeakA.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbSpeakA.Location = New System.Drawing.Point(16, 60)
        Me.rdbSpeakA.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbSpeakA.Name = "rdbSpeakA"
        Me.rdbSpeakA.Size = New System.Drawing.Size(98, 47)
        Me.rdbSpeakA.TabIndex = 101
        Me.rdbSpeakA.TabStop = True
        Me.rdbSpeakA.Text = "ดีมาก"
        Me.rdbSpeakA.UseVisualStyleBackColor = True
        '
        'GroupBox34
        '
        Me.GroupBox34.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.GroupBox34.Controls.Add(Me.rdbWriteC)
        Me.GroupBox34.Controls.Add(Me.rdbWriteB)
        Me.GroupBox34.Controls.Add(Me.rdbWriteA)
        Me.GroupBox34.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox34.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox34.Location = New System.Drawing.Point(1605, 36)
        Me.GroupBox34.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox34.Name = "GroupBox34"
        Me.GroupBox34.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox34.Size = New System.Drawing.Size(301, 135)
        Me.GroupBox34.TabIndex = 107
        Me.GroupBox34.TabStop = False
        Me.GroupBox34.Text = "การเขียน"
        '
        'rdbWriteC
        '
        Me.rdbWriteC.AutoSize = True
        Me.rdbWriteC.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbWriteC.Location = New System.Drawing.Point(184, 54)
        Me.rdbWriteC.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbWriteC.Name = "rdbWriteC"
        Me.rdbWriteC.Size = New System.Drawing.Size(101, 47)
        Me.rdbWriteC.TabIndex = 103
        Me.rdbWriteC.TabStop = True
        Me.rdbWriteC.Text = "พอใช้"
        Me.rdbWriteC.UseVisualStyleBackColor = True
        '
        'rdbWriteB
        '
        Me.rdbWriteB.AutoSize = True
        Me.rdbWriteB.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbWriteB.Location = New System.Drawing.Point(120, 54)
        Me.rdbWriteB.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbWriteB.Name = "rdbWriteB"
        Me.rdbWriteB.Size = New System.Drawing.Size(63, 47)
        Me.rdbWriteB.TabIndex = 102
        Me.rdbWriteB.TabStop = True
        Me.rdbWriteB.Text = "ดี"
        Me.rdbWriteB.UseVisualStyleBackColor = True
        '
        'rdbWriteA
        '
        Me.rdbWriteA.AutoSize = True
        Me.rdbWriteA.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbWriteA.Location = New System.Drawing.Point(21, 54)
        Me.rdbWriteA.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbWriteA.Name = "rdbWriteA"
        Me.rdbWriteA.Size = New System.Drawing.Size(98, 47)
        Me.rdbWriteA.TabIndex = 101
        Me.rdbWriteA.TabStop = True
        Me.rdbWriteA.Text = "ดีมาก"
        Me.rdbWriteA.UseVisualStyleBackColor = True
        '
        'GroupBox33
        '
        Me.GroupBox33.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.GroupBox33.Controls.Add(Me.rdbReadC)
        Me.GroupBox33.Controls.Add(Me.rdbReadB)
        Me.GroupBox33.Controls.Add(Me.rdbReadA)
        Me.GroupBox33.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox33.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox33.Location = New System.Drawing.Point(1205, 36)
        Me.GroupBox33.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox33.Name = "GroupBox33"
        Me.GroupBox33.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox33.Size = New System.Drawing.Size(349, 135)
        Me.GroupBox33.TabIndex = 106
        Me.GroupBox33.TabStop = False
        Me.GroupBox33.Text = "การอ่าน"
        '
        'rdbReadC
        '
        Me.rdbReadC.AutoSize = True
        Me.rdbReadC.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbReadC.Location = New System.Drawing.Point(229, 60)
        Me.rdbReadC.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbReadC.Name = "rdbReadC"
        Me.rdbReadC.Size = New System.Drawing.Size(101, 47)
        Me.rdbReadC.TabIndex = 103
        Me.rdbReadC.TabStop = True
        Me.rdbReadC.Text = "พอใช้"
        Me.rdbReadC.UseVisualStyleBackColor = True
        '
        'rdbReadB
        '
        Me.rdbReadB.AutoSize = True
        Me.rdbReadB.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbReadB.Location = New System.Drawing.Point(140, 60)
        Me.rdbReadB.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbReadB.Name = "rdbReadB"
        Me.rdbReadB.Size = New System.Drawing.Size(63, 47)
        Me.rdbReadB.TabIndex = 102
        Me.rdbReadB.TabStop = True
        Me.rdbReadB.Text = "ดี"
        Me.rdbReadB.UseVisualStyleBackColor = True
        '
        'rdbReadA
        '
        Me.rdbReadA.AutoSize = True
        Me.rdbReadA.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbReadA.Location = New System.Drawing.Point(11, 60)
        Me.rdbReadA.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbReadA.Name = "rdbReadA"
        Me.rdbReadA.Size = New System.Drawing.Size(98, 47)
        Me.rdbReadA.TabIndex = 101
        Me.rdbReadA.TabStop = True
        Me.rdbReadA.Text = "ดีมาก"
        Me.rdbReadA.UseVisualStyleBackColor = True
        '
        'GroupBox32
        '
        Me.GroupBox32.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.GroupBox32.Controls.Add(Me.rdbUnderstandC)
        Me.GroupBox32.Controls.Add(Me.rdbUnderstandB)
        Me.GroupBox32.Controls.Add(Me.rdbUnderstandA)
        Me.GroupBox32.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox32.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox32.Location = New System.Drawing.Point(796, 36)
        Me.GroupBox32.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox32.Name = "GroupBox32"
        Me.GroupBox32.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox32.Size = New System.Drawing.Size(349, 135)
        Me.GroupBox32.TabIndex = 105
        Me.GroupBox32.TabStop = False
        Me.GroupBox32.Text = "การเข้าใจ"
        '
        'rdbUnderstandC
        '
        Me.rdbUnderstandC.AutoSize = True
        Me.rdbUnderstandC.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbUnderstandC.Location = New System.Drawing.Point(224, 60)
        Me.rdbUnderstandC.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbUnderstandC.Name = "rdbUnderstandC"
        Me.rdbUnderstandC.Size = New System.Drawing.Size(101, 47)
        Me.rdbUnderstandC.TabIndex = 103
        Me.rdbUnderstandC.TabStop = True
        Me.rdbUnderstandC.Text = "พอใช้"
        Me.rdbUnderstandC.UseVisualStyleBackColor = True
        '
        'rdbUnderstandB
        '
        Me.rdbUnderstandB.AutoSize = True
        Me.rdbUnderstandB.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbUnderstandB.Location = New System.Drawing.Point(144, 60)
        Me.rdbUnderstandB.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbUnderstandB.Name = "rdbUnderstandB"
        Me.rdbUnderstandB.Size = New System.Drawing.Size(63, 47)
        Me.rdbUnderstandB.TabIndex = 102
        Me.rdbUnderstandB.TabStop = True
        Me.rdbUnderstandB.Text = "ดี"
        Me.rdbUnderstandB.UseVisualStyleBackColor = True
        '
        'rdbUnderstandA
        '
        Me.rdbUnderstandA.AutoSize = True
        Me.rdbUnderstandA.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbUnderstandA.Location = New System.Drawing.Point(13, 60)
        Me.rdbUnderstandA.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbUnderstandA.Name = "rdbUnderstandA"
        Me.rdbUnderstandA.Size = New System.Drawing.Size(98, 47)
        Me.rdbUnderstandA.TabIndex = 101
        Me.rdbUnderstandA.TabStop = True
        Me.rdbUnderstandA.Text = "ดีมาก"
        Me.rdbUnderstandA.UseVisualStyleBackColor = True
        '
        'dgvLanguage
        '
        Me.dgvLanguage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLanguage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5})
        Me.dgvLanguage.Location = New System.Drawing.Point(33, 302)
        Me.dgvLanguage.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.dgvLanguage.Name = "dgvLanguage"
        Me.dgvLanguage.Size = New System.Drawing.Size(1932, 222)
        Me.dgvLanguage.TabIndex = 85
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "ภาษา"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 200
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "ทักษะการพูด"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 250
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "ทักษะการเข้าใจ"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 250
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "ทักษะการอ่าน"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 250
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "ทักษะการเขียน"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 250
        '
        'cboLanguage
        '
        Me.cboLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLanguage.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLanguage.FormattingEnabled = True
        Me.cboLanguage.Items.AddRange(New Object() {"ไทย", "พม่า", "อังกฤษ", "จีน", "ญี่ปุ่น"})
        Me.cboLanguage.Location = New System.Drawing.Point(33, 88)
        Me.cboLanguage.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.cboLanguage.Name = "cboLanguage"
        Me.cboLanguage.Size = New System.Drawing.Size(289, 51)
        Me.cboLanguage.TabIndex = 68
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(93, 36)
        Me.Label50.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(161, 53)
        Me.Label50.TabIndex = 0
        Me.Label50.Text = "ประเภทภาษา"
        '
        'GroupBox22
        '
        Me.GroupBox22.Controls.Add(Me.Label57)
        Me.GroupBox22.Controls.Add(Me.Label56)
        Me.GroupBox22.Controls.Add(Me.txtBad)
        Me.GroupBox22.Controls.Add(Me.txtGood)
        Me.GroupBox22.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox22.Location = New System.Drawing.Point(26, 1141)
        Me.GroupBox22.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox22.Name = "GroupBox22"
        Me.GroupBox22.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox22.Size = New System.Drawing.Size(1976, 206)
        Me.GroupBox22.TabIndex = 39
        Me.GroupBox22.TabStop = False
        Me.GroupBox22.Text = "ข้อดีข้อเสียของตนเอง"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.Location = New System.Drawing.Point(12, 144)
        Me.Label57.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(86, 43)
        Me.Label57.TabIndex = 77
        Me.Label57.Text = "ข้อเสีย :"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.Location = New System.Drawing.Point(32, 68)
        Me.Label56.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(69, 43)
        Me.Label56.TabIndex = 76
        Me.Label56.Text = "ข้อดี :"
        '
        'txtBad
        '
        Me.txtBad.Location = New System.Drawing.Point(116, 129)
        Me.txtBad.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtBad.Name = "txtBad"
        Me.txtBad.Size = New System.Drawing.Size(1836, 60)
        Me.txtBad.TabIndex = 138
        '
        'txtGood
        '
        Me.txtGood.Location = New System.Drawing.Point(116, 52)
        Me.txtGood.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtGood.Name = "txtGood"
        Me.txtGood.Size = New System.Drawing.Size(1836, 60)
        Me.txtGood.TabIndex = 137
        '
        'GroupBox21
        '
        Me.GroupBox21.Controls.Add(Me.butWorkhisdel)
        Me.GroupBox21.Controls.Add(Me.butWorkhissave)
        Me.GroupBox21.Controls.Add(Me.txtIncome)
        Me.GroupBox21.Controls.Add(Me.Label55)
        Me.GroupBox21.Controls.Add(Me.dgvWorkhis)
        Me.GroupBox21.Controls.Add(Me.txtLastsalary)
        Me.GroupBox21.Controls.Add(Me.Label47)
        Me.GroupBox21.Controls.Add(Me.txtPosition)
        Me.GroupBox21.Controls.Add(Me.Label48)
        Me.GroupBox21.Controls.Add(Me.Label51)
        Me.GroupBox21.Controls.Add(Me.txtEnd)
        Me.GroupBox21.Controls.Add(Me.Label52)
        Me.GroupBox21.Controls.Add(Me.txtStart)
        Me.GroupBox21.Controls.Add(Me.Label53)
        Me.GroupBox21.Controls.Add(Me.txtOfficename)
        Me.GroupBox21.Controls.Add(Me.Label54)
        Me.GroupBox21.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox21.Location = New System.Drawing.Point(16, 608)
        Me.GroupBox21.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox21.Name = "GroupBox21"
        Me.GroupBox21.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox21.Size = New System.Drawing.Size(1976, 511)
        Me.GroupBox21.TabIndex = 38
        Me.GroupBox21.TabStop = False
        Me.GroupBox21.Text = "ประวัติการทำงาน"
        '
        'butWorkhisdel
        '
        Me.butWorkhisdel.Location = New System.Drawing.Point(944, 161)
        Me.butWorkhisdel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butWorkhisdel.Name = "butWorkhisdel"
        Me.butWorkhisdel.Size = New System.Drawing.Size(288, 52)
        Me.butWorkhisdel.TabIndex = 138
        Me.butWorkhisdel.Text = "ลบประวัติทำงาน"
        Me.butWorkhisdel.UseVisualStyleBackColor = True
        '
        'butWorkhissave
        '
        Me.butWorkhissave.Location = New System.Drawing.Point(604, 161)
        Me.butWorkhissave.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butWorkhissave.Name = "butWorkhissave"
        Me.butWorkhissave.Size = New System.Drawing.Size(275, 52)
        Me.butWorkhissave.TabIndex = 137
        Me.butWorkhissave.Text = "เพิ่มประวัติทำงาน"
        Me.butWorkhissave.UseVisualStyleBackColor = True
        '
        'txtIncome
        '
        Me.txtIncome.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtIncome.Location = New System.Drawing.Point(1668, 98)
        Me.txtIncome.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtIncome.Name = "txtIncome"
        Me.txtIncome.Size = New System.Drawing.Size(263, 51)
        Me.txtIncome.TabIndex = 135
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label55.Location = New System.Drawing.Point(1740, 50)
        Me.Label55.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(120, 53)
        Me.Label55.TabIndex = 86
        Me.Label55.Text = "รายได้อื่น"
        '
        'dgvWorkhis
        '
        Me.dgvWorkhis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvWorkhis.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.Column7})
        Me.dgvWorkhis.Location = New System.Drawing.Point(10, 236)
        Me.dgvWorkhis.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.dgvWorkhis.Name = "dgvWorkhis"
        Me.dgvWorkhis.Size = New System.Drawing.Size(1940, 223)
        Me.dgvWorkhis.TabIndex = 136
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "ชื่อสถานประกอบการ"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 350
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "จาก พ.ศ."
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 200
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "ถึง พ.ศ."
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 200
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "ตำแหน่ง"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Width = 200
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "เงินเดือนสุดท้าย"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Width = 250
        '
        'Column7
        '
        Me.Column7.HeaderText = "รายได้อื่น"
        Me.Column7.Name = "Column7"
        Me.Column7.Width = 200
        '
        'txtLastsalary
        '
        Me.txtLastsalary.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtLastsalary.Location = New System.Drawing.Point(1372, 98)
        Me.txtLastsalary.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtLastsalary.Name = "txtLastsalary"
        Me.txtLastsalary.Size = New System.Drawing.Size(263, 51)
        Me.txtLastsalary.TabIndex = 134
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label47.Location = New System.Drawing.Point(1404, 50)
        Me.Label47.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(228, 53)
        Me.Label47.TabIndex = 82
        Me.Label47.Text = "เงินเดือนครั้งสุดท้าย"
        '
        'txtPosition
        '
        Me.txtPosition.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPosition.Location = New System.Drawing.Point(1091, 98)
        Me.txtPosition.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtPosition.Name = "txtPosition"
        Me.txtPosition.Size = New System.Drawing.Size(244, 51)
        Me.txtPosition.TabIndex = 133
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label48.Location = New System.Drawing.Point(1155, 50)
        Me.Label48.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(111, 53)
        Me.Label48.TabIndex = 80
        Me.Label48.Text = "ตำแหน่ง"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label51.Location = New System.Drawing.Point(544, 111)
        Me.Label51.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(61, 43)
        Me.Label51.TabIndex = 79
        Me.Label51.Text = " พ.ศ."
        '
        'txtEnd
        '
        Me.txtEnd.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtEnd.Location = New System.Drawing.Point(885, 98)
        Me.txtEnd.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtEnd.Name = "txtEnd"
        Me.txtEnd.Size = New System.Drawing.Size(151, 51)
        Me.txtEnd.TabIndex = 132
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label52.Location = New System.Drawing.Point(776, 108)
        Me.Label52.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(94, 43)
        Me.Label52.TabIndex = 77
        Me.Label52.Text = "ถึง   พ.ศ."
        '
        'txtStart
        '
        Me.txtStart.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtStart.Location = New System.Drawing.Point(620, 102)
        Me.txtStart.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtStart.Name = "txtStart"
        Me.txtStart.Size = New System.Drawing.Size(151, 51)
        Me.txtStart.TabIndex = 131
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label53.Location = New System.Drawing.Point(773, 50)
        Me.Label53.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(116, 53)
        Me.Label53.TabIndex = 75
        Me.Label53.Text = "ระยะงาน"
        '
        'txtOfficename
        '
        Me.txtOfficename.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtOfficename.Location = New System.Drawing.Point(37, 111)
        Me.txtOfficename.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtOfficename.Name = "txtOfficename"
        Me.txtOfficename.Size = New System.Drawing.Size(484, 41)
        Me.txtOfficename.TabIndex = 130
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label54.Location = New System.Drawing.Point(115, 56)
        Me.Label54.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(369, 53)
        Me.Label54.TabIndex = 69
        Me.Label54.Text = "ชื่อสถานประกอบการ/ลักษณะงาน"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.butClear2)
        Me.TabPage2.Controls.Add(Me.GroupBox36)
        Me.TabPage2.Controls.Add(Me.butDel2)
        Me.TabPage2.Controls.Add(Me.GroupBox35)
        Me.TabPage2.Controls.Add(Me.butSave2)
        Me.TabPage2.Controls.Add(Me.GroupBox19)
        Me.TabPage2.Font = New System.Drawing.Font("Angsana New", 16.125!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TabPage2.Location = New System.Drawing.Point(8, 39)
        Me.TabPage2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TabPage2.Size = New System.Drawing.Size(2315, 1484)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "หน้า2"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'butClear2
        '
        Me.butClear2.Location = New System.Drawing.Point(856, 1417)
        Me.butClear2.Margin = New System.Windows.Forms.Padding(6)
        Me.butClear2.Name = "butClear2"
        Me.butClear2.Size = New System.Drawing.Size(160, 58)
        Me.butClear2.TabIndex = 80
        Me.butClear2.Text = "เคลียร์"
        Me.butClear2.UseVisualStyleBackColor = True
        '
        'GroupBox36
        '
        Me.GroupBox36.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox36.Controls.Add(Me.GroupBox17)
        Me.GroupBox36.Controls.Add(Me.GroupBox16)
        Me.GroupBox36.Controls.Add(Me.GroupBox13)
        Me.GroupBox36.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox36.Location = New System.Drawing.Point(13, 335)
        Me.GroupBox36.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox36.Name = "GroupBox36"
        Me.GroupBox36.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox36.Size = New System.Drawing.Size(1963, 529)
        Me.GroupBox36.TabIndex = 114
        Me.GroupBox36.TabStop = False
        Me.GroupBox36.Text = "ข้อมูลครอบครัว"
        '
        'GroupBox17
        '
        Me.GroupBox17.Controls.Add(Me.Label6)
        Me.GroupBox17.Controls.Add(Me.cboTitlemother)
        Me.GroupBox17.Controls.Add(Me.chkDeadmother)
        Me.GroupBox17.Controls.Add(Me.txtAgemother)
        Me.GroupBox17.Controls.Add(Me.Label17)
        Me.GroupBox17.Controls.Add(Me.txtJobmother)
        Me.GroupBox17.Controls.Add(Me.Label24)
        Me.GroupBox17.Controls.Add(Me.txtFnamemother)
        Me.GroupBox17.Controls.Add(Me.Label26)
        Me.GroupBox17.Location = New System.Drawing.Point(12, 161)
        Me.GroupBox17.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox17.Size = New System.Drawing.Size(1916, 106)
        Me.GroupBox17.TabIndex = 35
        Me.GroupBox17.TabStop = False
        Me.GroupBox17.Text = "ข้อมูลมารดา"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(250, 43)
        Me.Label6.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(103, 43)
        Me.Label6.TabIndex = 105
        Me.Label6.Text = "ชื่อ-สกุล :"
        '
        'cboTitlemother
        '
        Me.cboTitlemother.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTitlemother.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboTitlemother.FormattingEnabled = True
        Me.cboTitlemother.Items.AddRange(New Object() {"นาย", "นาง", "นางสาว", "ว่าที่ร้อยตรี", "Mr.", "Ms.", "Mrs.", ""})
        Me.cboTitlemother.Location = New System.Drawing.Point(125, 43)
        Me.cboTitlemother.Margin = New System.Windows.Forms.Padding(6)
        Me.cboTitlemother.Name = "cboTitlemother"
        Me.cboTitlemother.Size = New System.Drawing.Size(100, 51)
        Me.cboTitlemother.TabIndex = 104
        '
        'chkDeadmother
        '
        Me.chkDeadmother.AutoSize = True
        Me.chkDeadmother.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDeadmother.Location = New System.Drawing.Point(1323, 42)
        Me.chkDeadmother.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.chkDeadmother.Name = "chkDeadmother"
        Me.chkDeadmother.Size = New System.Drawing.Size(147, 47)
        Me.chkDeadmother.TabIndex = 103
        Me.chkDeadmother.Text = "ถึงแก่กรรม"
        Me.chkDeadmother.UseVisualStyleBackColor = True
        '
        'txtAgemother
        '
        Me.txtAgemother.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAgemother.Location = New System.Drawing.Point(928, 39)
        Me.txtAgemother.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtAgemother.Name = "txtAgemother"
        Me.txtAgemother.Size = New System.Drawing.Size(81, 51)
        Me.txtAgemother.TabIndex = 101
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(856, 42)
        Me.Label17.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(63, 43)
        Me.Label17.TabIndex = 59
        Me.Label17.Text = "อายุ :"
        '
        'txtJobmother
        '
        Me.txtJobmother.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobmother.Location = New System.Drawing.Point(1109, 39)
        Me.txtJobmother.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtJobmother.Name = "txtJobmother"
        Me.txtJobmother.Size = New System.Drawing.Size(164, 51)
        Me.txtJobmother.TabIndex = 102
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(1028, 42)
        Me.Label24.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(80, 43)
        Me.Label24.TabIndex = 57
        Me.Label24.Text = "อาชีพ :"
        '
        'txtFnamemother
        '
        Me.txtFnamemother.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFnamemother.Location = New System.Drawing.Point(394, 34)
        Me.txtFnamemother.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtFnamemother.Name = "txtFnamemother"
        Me.txtFnamemother.Size = New System.Drawing.Size(430, 51)
        Me.txtFnamemother.TabIndex = 99
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(11, 42)
        Me.Label26.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(115, 43)
        Me.Label26.TabIndex = 38
        Me.Label26.Text = "คำนำหน้า :"
        '
        'GroupBox16
        '
        Me.GroupBox16.Controls.Add(Me.cboTitlefather)
        Me.GroupBox16.Controls.Add(Me.chkDeadfather)
        Me.GroupBox16.Controls.Add(Me.txtAgefather)
        Me.GroupBox16.Controls.Add(Me.Label30)
        Me.GroupBox16.Controls.Add(Me.txtJobfather)
        Me.GroupBox16.Controls.Add(Me.Label34)
        Me.GroupBox16.Controls.Add(Me.txtFnamefather)
        Me.GroupBox16.Controls.Add(Me.Label36)
        Me.GroupBox16.Controls.Add(Me.Label37)
        Me.GroupBox16.Location = New System.Drawing.Point(12, 61)
        Me.GroupBox16.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox16.Size = New System.Drawing.Size(1916, 100)
        Me.GroupBox16.TabIndex = 34
        Me.GroupBox16.TabStop = False
        Me.GroupBox16.Text = "ข้อมูลบิดา"
        '
        'cboTitlefather
        '
        Me.cboTitlefather.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTitlefather.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboTitlefather.FormattingEnabled = True
        Me.cboTitlefather.Items.AddRange(New Object() {"นาย", "นาง", "นางสาว", "ว่าที่ร้อยตรี", "Mr.", "Ms.", "Mrs.", ""})
        Me.cboTitlefather.Location = New System.Drawing.Point(125, 36)
        Me.cboTitlefather.Margin = New System.Windows.Forms.Padding(6)
        Me.cboTitlefather.Name = "cboTitlefather"
        Me.cboTitlefather.Size = New System.Drawing.Size(100, 51)
        Me.cboTitlefather.TabIndex = 98
        '
        'chkDeadfather
        '
        Me.chkDeadfather.AutoSize = True
        Me.chkDeadfather.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDeadfather.Location = New System.Drawing.Point(1323, 39)
        Me.chkDeadfather.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.chkDeadfather.Name = "chkDeadfather"
        Me.chkDeadfather.Size = New System.Drawing.Size(147, 47)
        Me.chkDeadfather.TabIndex = 97
        Me.chkDeadfather.Text = "ถึงแก่กรรม"
        Me.chkDeadfather.UseVisualStyleBackColor = True
        '
        'txtAgefather
        '
        Me.txtAgefather.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAgefather.Location = New System.Drawing.Point(928, 35)
        Me.txtAgefather.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtAgefather.Name = "txtAgefather"
        Me.txtAgefather.Size = New System.Drawing.Size(81, 51)
        Me.txtAgefather.TabIndex = 95
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(860, 39)
        Me.Label30.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(63, 43)
        Me.Label30.TabIndex = 59
        Me.Label30.Text = "อายุ :"
        '
        'txtJobfather
        '
        Me.txtJobfather.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobfather.Location = New System.Drawing.Point(1109, 35)
        Me.txtJobfather.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtJobfather.Name = "txtJobfather"
        Me.txtJobfather.Size = New System.Drawing.Size(164, 51)
        Me.txtJobfather.TabIndex = 96
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(1028, 39)
        Me.Label34.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(80, 43)
        Me.Label34.TabIndex = 57
        Me.Label34.Text = "อาชีพ :"
        '
        'txtFnamefather
        '
        Me.txtFnamefather.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFnamefather.Location = New System.Drawing.Point(394, 37)
        Me.txtFnamefather.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtFnamefather.Name = "txtFnamefather"
        Me.txtFnamefather.Size = New System.Drawing.Size(430, 51)
        Me.txtFnamefather.TabIndex = 93
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(11, 39)
        Me.Label36.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(115, 43)
        Me.Label36.TabIndex = 38
        Me.Label36.Text = "คำนำหน้า :"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(250, 39)
        Me.Label37.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(103, 43)
        Me.Label37.TabIndex = 37
        Me.Label37.Text = "ชื่อ-สกุล :"
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.Label7)
        Me.GroupBox13.Controls.Add(Me.cboTitlemarry)
        Me.GroupBox13.Controls.Add(Me.txtOfficezipcode)
        Me.GroupBox13.Controls.Add(Me.txtOfficeProvince)
        Me.GroupBox13.Controls.Add(Me.txtOfficecanton)
        Me.GroupBox13.Controls.Add(Me.txtPhonemarry)
        Me.GroupBox13.Controls.Add(Me.Label28)
        Me.GroupBox13.Controls.Add(Me.txtOfficemarry)
        Me.GroupBox13.Controls.Add(Me.Label13)
        Me.GroupBox13.Controls.Add(Me.txtOfficedistrict)
        Me.GroupBox13.Controls.Add(Me.Label1)
        Me.GroupBox13.Controls.Add(Me.txtOfficeaddress)
        Me.GroupBox13.Controls.Add(Me.Label2)
        Me.GroupBox13.Controls.Add(Me.Label3)
        Me.GroupBox13.Controls.Add(Me.Label9)
        Me.GroupBox13.Controls.Add(Me.txtOfficemoo)
        Me.GroupBox13.Controls.Add(Me.Label11)
        Me.GroupBox13.Controls.Add(Me.Label12)
        Me.GroupBox13.Controls.Add(Me.txtJobmarry)
        Me.GroupBox13.Controls.Add(Me.Label15)
        Me.GroupBox13.Controls.Add(Me.txtFnamemarry)
        Me.GroupBox13.Controls.Add(Me.Label32)
        Me.GroupBox13.Location = New System.Drawing.Point(12, 269)
        Me.GroupBox13.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox13.Size = New System.Drawing.Size(1916, 250)
        Me.GroupBox13.TabIndex = 31
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "ข้อมูลคู่สมรส"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(250, 51)
        Me.Label7.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(103, 43)
        Me.Label7.TabIndex = 122
        Me.Label7.Text = "ชื่อ-สกุล :"
        '
        'cboTitlemarry
        '
        Me.cboTitlemarry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTitlemarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboTitlemarry.FormattingEnabled = True
        Me.cboTitlemarry.Items.AddRange(New Object() {"นาย", "นาง", "นางสาว", "ว่าที่ร้อยตรี", "Mr.", "Ms.", "Mrs.", ""})
        Me.cboTitlemarry.Location = New System.Drawing.Point(125, 43)
        Me.cboTitlemarry.Margin = New System.Windows.Forms.Padding(6)
        Me.cboTitlemarry.Name = "cboTitlemarry"
        Me.cboTitlemarry.Size = New System.Drawing.Size(100, 51)
        Me.cboTitlemarry.TabIndex = 121
        '
        'txtOfficezipcode
        '
        Me.txtOfficezipcode.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOfficezipcode.Location = New System.Drawing.Point(1159, 187)
        Me.txtOfficezipcode.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtOfficezipcode.Name = "txtOfficezipcode"
        Me.txtOfficezipcode.Size = New System.Drawing.Size(208, 51)
        Me.txtOfficezipcode.TabIndex = 119
        '
        'txtOfficeProvince
        '
        Me.txtOfficeProvince.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOfficeProvince.Location = New System.Drawing.Point(795, 187)
        Me.txtOfficeProvince.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtOfficeProvince.Name = "txtOfficeProvince"
        Me.txtOfficeProvince.Size = New System.Drawing.Size(208, 51)
        Me.txtOfficeProvince.TabIndex = 118
        '
        'txtOfficecanton
        '
        Me.txtOfficecanton.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOfficecanton.Location = New System.Drawing.Point(105, 184)
        Me.txtOfficecanton.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtOfficecanton.Name = "txtOfficecanton"
        Me.txtOfficecanton.Size = New System.Drawing.Size(208, 51)
        Me.txtOfficecanton.TabIndex = 117
        '
        'txtPhonemarry
        '
        Me.txtPhonemarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhonemarry.Location = New System.Drawing.Point(1252, 42)
        Me.txtPhonemarry.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtPhonemarry.Name = "txtPhonemarry"
        Me.txtPhonemarry.Size = New System.Drawing.Size(276, 51)
        Me.txtPhonemarry.TabIndex = 108
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(1144, 48)
        Me.Label28.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(107, 43)
        Me.Label28.TabIndex = 75
        Me.Label28.Text = "โทรศัพท์ :"
        '
        'txtOfficemarry
        '
        Me.txtOfficemarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOfficemarry.Location = New System.Drawing.Point(176, 114)
        Me.txtOfficemarry.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtOfficemarry.Name = "txtOfficemarry"
        Me.txtOfficemarry.Size = New System.Drawing.Size(668, 51)
        Me.txtOfficemarry.TabIndex = 109
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(12, 119)
        Me.Label13.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(152, 43)
        Me.Label13.TabIndex = 73
        Me.Label13.Text = "สถานที่ทำงาน :"
        '
        'txtOfficedistrict
        '
        Me.txtOfficedistrict.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOfficedistrict.Location = New System.Drawing.Point(458, 187)
        Me.txtOfficedistrict.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtOfficedistrict.Name = "txtOfficedistrict"
        Me.txtOfficedistrict.Size = New System.Drawing.Size(208, 51)
        Me.txtOfficedistrict.TabIndex = 116
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(1020, 192)
        Me.Label1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 43)
        Me.Label1.TabIndex = 71
        Me.Label1.Text = "รหัสไปรษณี :"
        '
        'txtOfficeaddress
        '
        Me.txtOfficeaddress.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOfficeaddress.Location = New System.Drawing.Point(942, 119)
        Me.txtOfficeaddress.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtOfficeaddress.Name = "txtOfficeaddress"
        Me.txtOfficeaddress.Size = New System.Drawing.Size(215, 51)
        Me.txtOfficeaddress.TabIndex = 110
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(698, 192)
        Me.Label2.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 43)
        Me.Label2.TabIndex = 66
        Me.Label2.Text = "จังหวัด :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(359, 192)
        Me.Label3.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 43)
        Me.Label3.TabIndex = 64
        Me.Label3.Text = "อำเภอ :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(21, 189)
        Me.Label9.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(79, 43)
        Me.Label9.TabIndex = 63
        Me.Label9.Text = "ตำบล :"
        '
        'txtOfficemoo
        '
        Me.txtOfficemoo.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOfficemoo.Location = New System.Drawing.Point(1265, 115)
        Me.txtOfficemoo.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtOfficemoo.Name = "txtOfficemoo"
        Me.txtOfficemoo.Size = New System.Drawing.Size(96, 51)
        Me.txtOfficemoo.TabIndex = 111
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(870, 124)
        Me.Label11.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(75, 43)
        Me.Label11.TabIndex = 60
        Me.Label11.Text = "เลขที่ :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(1190, 124)
        Me.Label12.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(72, 43)
        Me.Label12.TabIndex = 59
        Me.Label12.Text = "หมู่ที่ :"
        '
        'txtJobmarry
        '
        Me.txtJobmarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobmarry.Location = New System.Drawing.Point(952, 44)
        Me.txtJobmarry.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtJobmarry.Name = "txtJobmarry"
        Me.txtJobmarry.Size = New System.Drawing.Size(164, 51)
        Me.txtJobmarry.TabIndex = 107
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(872, 46)
        Me.Label15.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(80, 43)
        Me.Label15.TabIndex = 57
        Me.Label15.Text = "อาชีพ :"
        '
        'txtFnamemarry
        '
        Me.txtFnamemarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFnamemarry.Location = New System.Drawing.Point(394, 42)
        Me.txtFnamemarry.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtFnamemarry.Name = "txtFnamemarry"
        Me.txtFnamemarry.Size = New System.Drawing.Size(418, 51)
        Me.txtFnamemarry.TabIndex = 105
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(11, 46)
        Me.Label32.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(115, 43)
        Me.Label32.TabIndex = 38
        Me.Label32.Text = "คำนำหน้า :"
        '
        'butDel2
        '
        Me.butDel2.Location = New System.Drawing.Point(665, 1417)
        Me.butDel2.Margin = New System.Windows.Forms.Padding(6)
        Me.butDel2.Name = "butDel2"
        Me.butDel2.Size = New System.Drawing.Size(160, 58)
        Me.butDel2.TabIndex = 82
        Me.butDel2.Text = "ลบ"
        Me.butDel2.UseVisualStyleBackColor = True
        '
        'GroupBox35
        '
        Me.GroupBox35.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox35.Controls.Add(Me.GroupBox15)
        Me.GroupBox35.Controls.Add(Me.GroupBox14)
        Me.GroupBox35.Controls.Add(Me.GroupBox3)
        Me.GroupBox35.Controls.Add(Me.GroupBox2)
        Me.GroupBox35.Controls.Add(Me.GroupBox1)
        Me.GroupBox35.Controls.Add(Me.GroupBox18)
        Me.GroupBox35.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox35.Location = New System.Drawing.Point(13, 6)
        Me.GroupBox35.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox35.Name = "GroupBox35"
        Me.GroupBox35.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox35.Size = New System.Drawing.Size(1963, 321)
        Me.GroupBox35.TabIndex = 113
        Me.GroupBox35.TabStop = False
        Me.GroupBox35.Text = "ข้อมูลทั่วไป"
        '
        'GroupBox15
        '
        Me.GroupBox15.Controls.Add(Me.Label16)
        Me.GroupBox15.Controls.Add(Me.txtChildlearning)
        Me.GroupBox15.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox15.Location = New System.Drawing.Point(1468, 36)
        Me.GroupBox15.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox15.Size = New System.Drawing.Size(316, 161)
        Me.GroupBox15.TabIndex = 33
        Me.GroupBox15.TabStop = False
        Me.GroupBox15.Text = "จำนวนบุตรที่กำลังศึกษา"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(243, 75)
        Me.Label16.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(47, 43)
        Me.Label16.TabIndex = 60
        Me.Label16.Text = "คน"
        '
        'txtChildlearning
        '
        Me.txtChildlearning.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChildlearning.Location = New System.Drawing.Point(61, 61)
        Me.txtChildlearning.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtChildlearning.Name = "txtChildlearning"
        Me.txtChildlearning.Size = New System.Drawing.Size(164, 51)
        Me.txtChildlearning.TabIndex = 87
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.Label14)
        Me.GroupBox14.Controls.Add(Me.txtChild)
        Me.GroupBox14.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox14.Location = New System.Drawing.Point(1152, 36)
        Me.GroupBox14.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox14.Size = New System.Drawing.Size(288, 161)
        Me.GroupBox14.TabIndex = 32
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "จำนวนบุตร"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.Location = New System.Drawing.Point(213, 75)
        Me.Label14.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(38, 26)
        Me.Label14.TabIndex = 60
        Me.Label14.Text = "คน"
        '
        'txtChild
        '
        Me.txtChild.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtChild.Location = New System.Drawing.Point(36, 61)
        Me.txtChild.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtChild.Name = "txtChild"
        Me.txtChild.Size = New System.Drawing.Size(164, 41)
        Me.txtChild.TabIndex = 86
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rdbEarningno)
        Me.GroupBox3.Controls.Add(Me.rdbEarning)
        Me.GroupBox3.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(796, 36)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox3.Size = New System.Drawing.Size(324, 161)
        Me.GroupBox3.TabIndex = 30
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "คู่สมรสมีรายได้หรือไม่"
        '
        'rdbEarningno
        '
        Me.rdbEarningno.AutoSize = True
        Me.rdbEarningno.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbEarningno.Location = New System.Drawing.Point(68, 104)
        Me.rdbEarningno.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbEarningno.Name = "rdbEarningno"
        Me.rdbEarningno.Size = New System.Drawing.Size(87, 47)
        Me.rdbEarningno.TabIndex = 85
        Me.rdbEarningno.TabStop = True
        Me.rdbEarningno.Text = "ไม่มี"
        Me.rdbEarningno.UseVisualStyleBackColor = True
        '
        'rdbEarning
        '
        Me.rdbEarning.AutoSize = True
        Me.rdbEarning.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbEarning.Location = New System.Drawing.Point(68, 44)
        Me.rdbEarning.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbEarning.Name = "rdbEarning"
        Me.rdbEarning.Size = New System.Drawing.Size(63, 47)
        Me.rdbEarning.TabIndex = 84
        Me.rdbEarning.TabStop = True
        Me.rdbEarning.Text = "มี"
        Me.rdbEarning.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rdbRegisno)
        Me.GroupBox2.Controls.Add(Me.rdbRegis)
        Me.GroupBox2.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(428, 36)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox2.Size = New System.Drawing.Size(336, 161)
        Me.GroupBox2.TabIndex = 29
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "กรณีสมรส"
        '
        'rdbRegisno
        '
        Me.rdbRegisno.AutoSize = True
        Me.rdbRegisno.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbRegisno.Location = New System.Drawing.Point(64, 104)
        Me.rdbRegisno.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbRegisno.Name = "rdbRegisno"
        Me.rdbRegisno.Size = New System.Drawing.Size(168, 47)
        Me.rdbRegisno.TabIndex = 83
        Me.rdbRegisno.TabStop = True
        Me.rdbRegisno.Text = "ไม่จดทะเบียน"
        Me.rdbRegisno.UseVisualStyleBackColor = True
        '
        'rdbRegis
        '
        Me.rdbRegis.AutoSize = True
        Me.rdbRegis.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbRegis.Location = New System.Drawing.Point(64, 44)
        Me.rdbRegis.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbRegis.Name = "rdbRegis"
        Me.rdbRegis.Size = New System.Drawing.Size(144, 47)
        Me.rdbRegis.TabIndex = 82
        Me.rdbRegis.TabStop = True
        Me.rdbRegis.Text = "จดทะเบียน"
        Me.rdbRegis.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdbSeparate)
        Me.GroupBox1.Controls.Add(Me.rdbWidowed)
        Me.GroupBox1.Controls.Add(Me.rdbDivorce)
        Me.GroupBox1.Controls.Add(Me.rdbMarry)
        Me.GroupBox1.Controls.Add(Me.rdbSingle)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(8, 35)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox1.Size = New System.Drawing.Size(379, 161)
        Me.GroupBox1.TabIndex = 28
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "สถานะครอบครัว"
        '
        'rdbSeparate
        '
        Me.rdbSeparate.AutoSize = True
        Me.rdbSeparate.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbSeparate.Location = New System.Drawing.Point(140, 106)
        Me.rdbSeparate.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbSeparate.Name = "rdbSeparate"
        Me.rdbSeparate.Size = New System.Drawing.Size(138, 47)
        Me.rdbSeparate.TabIndex = 81
        Me.rdbSeparate.TabStop = True
        Me.rdbSeparate.Text = "แยกกันอยู่"
        Me.rdbSeparate.UseVisualStyleBackColor = True
        '
        'rdbWidowed
        '
        Me.rdbWidowed.AutoSize = True
        Me.rdbWidowed.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbWidowed.Location = New System.Drawing.Point(16, 104)
        Me.rdbWidowed.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbWidowed.Name = "rdbWidowed"
        Me.rdbWidowed.Size = New System.Drawing.Size(98, 47)
        Me.rdbWidowed.TabIndex = 80
        Me.rdbWidowed.TabStop = True
        Me.rdbWidowed.Text = "หม้าย"
        Me.rdbWidowed.UseVisualStyleBackColor = True
        '
        'rdbDivorce
        '
        Me.rdbDivorce.AutoSize = True
        Me.rdbDivorce.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbDivorce.Location = New System.Drawing.Point(272, 44)
        Me.rdbDivorce.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbDivorce.Name = "rdbDivorce"
        Me.rdbDivorce.Size = New System.Drawing.Size(85, 47)
        Me.rdbDivorce.TabIndex = 79
        Me.rdbDivorce.TabStop = True
        Me.rdbDivorce.Text = "หย่า"
        Me.rdbDivorce.UseVisualStyleBackColor = True
        '
        'rdbMarry
        '
        Me.rdbMarry.AutoSize = True
        Me.rdbMarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbMarry.Location = New System.Drawing.Point(140, 44)
        Me.rdbMarry.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbMarry.Name = "rdbMarry"
        Me.rdbMarry.Size = New System.Drawing.Size(102, 47)
        Me.rdbMarry.TabIndex = 78
        Me.rdbMarry.TabStop = True
        Me.rdbMarry.Text = "สมรส"
        Me.rdbMarry.UseVisualStyleBackColor = True
        '
        'rdbSingle
        '
        Me.rdbSingle.AutoSize = True
        Me.rdbSingle.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbSingle.Location = New System.Drawing.Point(16, 44)
        Me.rdbSingle.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbSingle.Name = "rdbSingle"
        Me.rdbSingle.Size = New System.Drawing.Size(88, 47)
        Me.rdbSingle.TabIndex = 77
        Me.rdbSingle.TabStop = True
        Me.rdbSingle.Text = "โสด"
        Me.rdbSingle.UseVisualStyleBackColor = True
        '
        'GroupBox18
        '
        Me.GroupBox18.Controls.Add(Me.rdbAnotherssol)
        Me.GroupBox18.Controls.Add(Me.rdbPMilitary)
        Me.GroupBox18.Controls.Add(Me.rdbEducate)
        Me.GroupBox18.Controls.Add(Me.rdbExcept)
        Me.GroupBox18.Controls.Add(Me.txtAnotherssol)
        Me.GroupBox18.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox18.Location = New System.Drawing.Point(8, 200)
        Me.GroupBox18.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox18.Name = "GroupBox18"
        Me.GroupBox18.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox18.Size = New System.Drawing.Size(1371, 94)
        Me.GroupBox18.TabIndex = 36
        Me.GroupBox18.TabStop = False
        Me.GroupBox18.Text = "สถานะทางทหาร"
        '
        'rdbAnotherssol
        '
        Me.rdbAnotherssol.AutoSize = True
        Me.rdbAnotherssol.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbAnotherssol.Location = New System.Drawing.Point(757, 40)
        Me.rdbAnotherssol.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbAnotherssol.Name = "rdbAnotherssol"
        Me.rdbAnotherssol.Size = New System.Drawing.Size(182, 47)
        Me.rdbAnotherssol.TabIndex = 91
        Me.rdbAnotherssol.TabStop = True
        Me.rdbAnotherssol.Text = "อื่นๆ โปรดระบุ"
        Me.rdbAnotherssol.UseVisualStyleBackColor = True
        '
        'rdbPMilitary
        '
        Me.rdbPMilitary.AutoSize = True
        Me.rdbPMilitary.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbPMilitary.Location = New System.Drawing.Point(509, 40)
        Me.rdbPMilitary.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbPMilitary.Name = "rdbPMilitary"
        Me.rdbPMilitary.Size = New System.Drawing.Size(221, 47)
        Me.rdbPMilitary.TabIndex = 90
        Me.rdbPMilitary.TabStop = True
        Me.rdbPMilitary.Text = "ผ่านการเกณฑ์ทหาร"
        Me.rdbPMilitary.UseVisualStyleBackColor = True
        '
        'rdbEducate
        '
        Me.rdbEducate.AutoSize = True
        Me.rdbEducate.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbEducate.Location = New System.Drawing.Point(304, 36)
        Me.rdbEducate.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbEducate.Name = "rdbEducate"
        Me.rdbEducate.Size = New System.Drawing.Size(180, 47)
        Me.rdbEducate.TabIndex = 89
        Me.rdbEducate.TabStop = True
        Me.rdbEducate.Text = "ศึกษาวิชาทหาร"
        Me.rdbEducate.UseVisualStyleBackColor = True
        '
        'rdbExcept
        '
        Me.rdbExcept.AutoSize = True
        Me.rdbExcept.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbExcept.Location = New System.Drawing.Point(75, 36)
        Me.rdbExcept.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbExcept.Name = "rdbExcept"
        Me.rdbExcept.Size = New System.Drawing.Size(187, 47)
        Me.rdbExcept.TabIndex = 88
        Me.rdbExcept.TabStop = True
        Me.rdbExcept.Text = "ได้รับการยกเว้น"
        Me.rdbExcept.UseVisualStyleBackColor = True
        '
        'txtAnotherssol
        '
        Me.txtAnotherssol.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtAnotherssol.Location = New System.Drawing.Point(952, 43)
        Me.txtAnotherssol.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtAnotherssol.Name = "txtAnotherssol"
        Me.txtAnotherssol.Size = New System.Drawing.Size(244, 41)
        Me.txtAnotherssol.TabIndex = 92
        '
        'butSave2
        '
        Me.butSave2.Location = New System.Drawing.Point(464, 1417)
        Me.butSave2.Margin = New System.Windows.Forms.Padding(6)
        Me.butSave2.Name = "butSave2"
        Me.butSave2.Size = New System.Drawing.Size(160, 58)
        Me.butSave2.TabIndex = 81
        Me.butSave2.Text = "บันทึก"
        Me.butSave2.UseVisualStyleBackColor = True
        '
        'GroupBox19
        '
        Me.GroupBox19.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox19.Controls.Add(Me.cboEdulevel)
        Me.GroupBox19.Controls.Add(Me.dgvEdu)
        Me.GroupBox19.Controls.Add(Me.txtEdugrade)
        Me.GroupBox19.Controls.Add(Me.butEdudel)
        Me.GroupBox19.Controls.Add(Me.Label43)
        Me.GroupBox19.Controls.Add(Me.txtEdumajor)
        Me.GroupBox19.Controls.Add(Me.butEdusave)
        Me.GroupBox19.Controls.Add(Me.Label42)
        Me.GroupBox19.Controls.Add(Me.Label41)
        Me.GroupBox19.Controls.Add(Me.txtEduend)
        Me.GroupBox19.Controls.Add(Me.Label40)
        Me.GroupBox19.Controls.Add(Me.txtEdustart)
        Me.GroupBox19.Controls.Add(Me.Label39)
        Me.GroupBox19.Controls.Add(Me.txtEduname)
        Me.GroupBox19.Controls.Add(Me.Label38)
        Me.GroupBox19.Controls.Add(Me.Label29)
        Me.GroupBox19.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox19.Location = New System.Drawing.Point(13, 865)
        Me.GroupBox19.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox19.Name = "GroupBox19"
        Me.GroupBox19.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox19.Size = New System.Drawing.Size(1963, 540)
        Me.GroupBox19.TabIndex = 37
        Me.GroupBox19.TabStop = False
        Me.GroupBox19.Text = "ประวัติการศึกษา"
        '
        'cboEdulevel
        '
        Me.cboEdulevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEdulevel.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboEdulevel.FormattingEnabled = True
        Me.cboEdulevel.Items.AddRange(New Object() {"สูงกว่าปริญญาตรี", "ปริญญาตรี", "อนุปริญญา", "ปวส.", "ปวช.", "ม.6", "ม.3", "ป.6", "ป.4", "อื่นๆ"})
        Me.cboEdulevel.Location = New System.Drawing.Point(41, 108)
        Me.cboEdulevel.Margin = New System.Windows.Forms.Padding(6)
        Me.cboEdulevel.Name = "cboEdulevel"
        Me.cboEdulevel.Size = New System.Drawing.Size(259, 51)
        Me.cboEdulevel.TabIndex = 125
        '
        'dgvEdu
        '
        Me.dgvEdu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEdu.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6})
        Me.dgvEdu.Location = New System.Drawing.Point(12, 246)
        Me.dgvEdu.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.dgvEdu.Name = "dgvEdu"
        Me.dgvEdu.Size = New System.Drawing.Size(1933, 269)
        Me.dgvEdu.TabIndex = 85
        '
        'Column1
        '
        Me.Column1.HeaderText = "ระดับการศึกษา"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 250
        '
        'Column2
        '
        Me.Column2.HeaderText = "ชื่อสถาบัน"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 220
        '
        'Column3
        '
        Me.Column3.HeaderText = "จาก พ.ศ."
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 200
        '
        'Column4
        '
        Me.Column4.HeaderText = "ถึง พ.ศ."
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 200
        '
        'Column5
        '
        Me.Column5.HeaderText = "สาขาที่สำเร็จ"
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 250
        '
        'Column6
        '
        Me.Column6.HeaderText = "คะแนนเฉลี่ย"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 200
        '
        'txtEdugrade
        '
        Me.txtEdugrade.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEdugrade.Location = New System.Drawing.Point(1700, 108)
        Me.txtEdugrade.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtEdugrade.Name = "txtEdugrade"
        Me.txtEdugrade.Size = New System.Drawing.Size(204, 51)
        Me.txtEdugrade.TabIndex = 122
        '
        'butEdudel
        '
        Me.butEdudel.Location = New System.Drawing.Point(881, 177)
        Me.butEdudel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butEdudel.Name = "butEdudel"
        Me.butEdudel.Size = New System.Drawing.Size(288, 52)
        Me.butEdudel.TabIndex = 124
        Me.butEdudel.Text = "ลบประวัติการศึกษา"
        Me.butEdudel.UseVisualStyleBackColor = True
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(1741, 48)
        Me.Label43.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(151, 53)
        Me.Label43.TabIndex = 82
        Me.Label43.Text = "คะแนนเฉลี่ย"
        '
        'txtEdumajor
        '
        Me.txtEdumajor.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEdumajor.Location = New System.Drawing.Point(1325, 108)
        Me.txtEdumajor.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtEdumajor.Name = "txtEdumajor"
        Me.txtEdumajor.Size = New System.Drawing.Size(320, 51)
        Me.txtEdumajor.TabIndex = 121
        '
        'butEdusave
        '
        Me.butEdusave.Location = New System.Drawing.Point(541, 177)
        Me.butEdusave.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butEdusave.Name = "butEdusave"
        Me.butEdusave.Size = New System.Drawing.Size(275, 52)
        Me.butEdusave.TabIndex = 123
        Me.butEdusave.Text = "เพิ่มประวัติการศึกษา"
        Me.butEdusave.UseVisualStyleBackColor = True
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(1461, 44)
        Me.Label42.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(77, 53)
        Me.Label42.TabIndex = 80
        Me.Label42.Text = "สาขา"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(832, 114)
        Me.Label41.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(61, 43)
        Me.Label41.TabIndex = 79
        Me.Label41.Text = " พ.ศ."
        '
        'txtEduend
        '
        Me.txtEduend.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEduend.Location = New System.Drawing.Point(1156, 108)
        Me.txtEduend.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtEduend.Name = "txtEduend"
        Me.txtEduend.Size = New System.Drawing.Size(148, 51)
        Me.txtEduend.TabIndex = 120
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(1064, 111)
        Me.Label40.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(84, 43)
        Me.Label40.TabIndex = 77
        Me.Label40.Text = "ถึง พ.ศ."
        '
        'txtEdustart
        '
        Me.txtEdustart.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEdustart.Location = New System.Drawing.Point(900, 106)
        Me.txtEdustart.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtEdustart.Name = "txtEdustart"
        Me.txtEdustart.Size = New System.Drawing.Size(148, 51)
        Me.txtEdustart.TabIndex = 119
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(1051, 44)
        Me.Label39.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(138, 53)
        Me.Label39.TabIndex = 75
        Me.Label39.Text = "ปีการศึกษา"
        '
        'txtEduname
        '
        Me.txtEduname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEduname.Location = New System.Drawing.Point(332, 106)
        Me.txtEduname.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtEduname.Name = "txtEduname"
        Me.txtEduname.Size = New System.Drawing.Size(484, 51)
        Me.txtEduname.TabIndex = 118
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(515, 44)
        Me.Label38.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(129, 53)
        Me.Label38.TabIndex = 69
        Me.Label38.Text = "ชื่อสถาบัน"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(88, 44)
        Me.Label29.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(180, 53)
        Me.Label29.TabIndex = 0
        Me.Label29.Text = "ระดับการศึกษา"
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.butClear1)
        Me.TabPage1.Controls.Add(Me.GroupBox37)
        Me.TabPage1.Controls.Add(Me.GroupBox7)
        Me.TabPage1.Controls.Add(Me.butDel1)
        Me.TabPage1.Controls.Add(Me.GroupBox30)
        Me.TabPage1.Controls.Add(Me.butSave1)
        Me.TabPage1.Controls.Add(Me.GroupBox5)
        Me.TabPage1.Controls.Add(Me.GroupBox6)
        Me.TabPage1.Controls.Add(Me.GroupBox11)
        Me.TabPage1.Controls.Add(Me.GroupBox29)
        Me.TabPage1.Controls.Add(Me.GroupBox12)
        Me.TabPage1.Controls.Add(Me.GroupBox10)
        Me.TabPage1.Controls.Add(Me.GroupBox9)
        Me.TabPage1.Controls.Add(Me.GroupBox8)
        Me.TabPage1.Controls.Add(Me.GroupBox4)
        Me.TabPage1.Controls.Add(Me.picOneinch)
        Me.TabPage1.Font = New System.Drawing.Font("Angsana New", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TabPage1.Location = New System.Drawing.Point(8, 39)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TabPage1.Size = New System.Drawing.Size(2315, 1484)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "หน้า1"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'butClear1
        '
        Me.butClear1.Location = New System.Drawing.Point(849, 1328)
        Me.butClear1.Margin = New System.Windows.Forms.Padding(6)
        Me.butClear1.Name = "butClear1"
        Me.butClear1.Size = New System.Drawing.Size(160, 58)
        Me.butClear1.TabIndex = 77
        Me.butClear1.Text = "เคลียร์"
        Me.butClear1.UseVisualStyleBackColor = True
        '
        'GroupBox37
        '
        Me.GroupBox37.Controls.Add(Me.rdbContain)
        Me.GroupBox37.Controls.Add(Me.rdbTrial)
        Me.GroupBox37.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox37.Location = New System.Drawing.Point(1538, 679)
        Me.GroupBox37.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox37.Name = "GroupBox37"
        Me.GroupBox37.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox37.Size = New System.Drawing.Size(326, 142)
        Me.GroupBox37.TabIndex = 108
        Me.GroupBox37.TabStop = False
        Me.GroupBox37.Text = "สถานะการทำงาน"
        '
        'rdbContain
        '
        Me.rdbContain.AutoSize = True
        Me.rdbContain.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbContain.Location = New System.Drawing.Point(144, 63)
        Me.rdbContain.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbContain.Name = "rdbContain"
        Me.rdbContain.Size = New System.Drawing.Size(97, 47)
        Me.rdbContain.TabIndex = 22
        Me.rdbContain.TabStop = True
        Me.rdbContain.Text = "บรรจุ"
        Me.rdbContain.UseVisualStyleBackColor = True
        '
        'rdbTrial
        '
        Me.rdbTrial.AutoSize = True
        Me.rdbTrial.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbTrial.Location = New System.Drawing.Point(18, 62)
        Me.rdbTrial.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbTrial.Name = "rdbTrial"
        Me.rdbTrial.Size = New System.Drawing.Size(113, 47)
        Me.rdbTrial.TabIndex = 21
        Me.rdbTrial.TabStop = True
        Me.rdbTrial.Text = "ทดลอง"
        Me.rdbTrial.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.rdbSicknessno)
        Me.GroupBox7.Controls.Add(Me.rdbSickness)
        Me.GroupBox7.Controls.Add(Me.txtSickness)
        Me.GroupBox7.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox7.Location = New System.Drawing.Point(7, 661)
        Me.GroupBox7.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox7.Size = New System.Drawing.Size(395, 170)
        Me.GroupBox7.TabIndex = 107
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "โรคประจำตัว"
        '
        'rdbSicknessno
        '
        Me.rdbSicknessno.AutoSize = True
        Me.rdbSicknessno.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbSicknessno.Location = New System.Drawing.Point(46, 44)
        Me.rdbSicknessno.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbSicknessno.Name = "rdbSicknessno"
        Me.rdbSicknessno.Size = New System.Drawing.Size(87, 47)
        Me.rdbSicknessno.TabIndex = 61
        Me.rdbSicknessno.TabStop = True
        Me.rdbSicknessno.Text = "ไม่มี"
        Me.rdbSicknessno.UseVisualStyleBackColor = True
        '
        'rdbSickness
        '
        Me.rdbSickness.AutoSize = True
        Me.rdbSickness.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbSickness.Location = New System.Drawing.Point(46, 97)
        Me.rdbSickness.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbSickness.Name = "rdbSickness"
        Me.rdbSickness.Size = New System.Drawing.Size(78, 47)
        Me.rdbSickness.TabIndex = 62
        Me.rdbSickness.TabStop = True
        Me.rdbSickness.Text = "มี..."
        Me.rdbSickness.UseVisualStyleBackColor = True
        '
        'txtSickness
        '
        Me.txtSickness.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtSickness.Location = New System.Drawing.Point(143, 93)
        Me.txtSickness.Margin = New System.Windows.Forms.Padding(6)
        Me.txtSickness.Name = "txtSickness"
        Me.txtSickness.Size = New System.Drawing.Size(188, 51)
        Me.txtSickness.TabIndex = 63
        '
        'butDel1
        '
        Me.butDel1.Location = New System.Drawing.Point(658, 1328)
        Me.butDel1.Margin = New System.Windows.Forms.Padding(6)
        Me.butDel1.Name = "butDel1"
        Me.butDel1.Size = New System.Drawing.Size(160, 58)
        Me.butDel1.TabIndex = 79
        Me.butDel1.Text = "ลบ"
        Me.butDel1.UseVisualStyleBackColor = True
        '
        'GroupBox30
        '
        Me.GroupBox30.Controls.Add(Me.txtByprovince)
        Me.GroupBox30.Controls.Add(Me.txtBycanton)
        Me.GroupBox30.Controls.Add(Me.dtpBirthday)
        Me.GroupBox30.Controls.Add(Me.dtpEndidcard)
        Me.GroupBox30.Controls.Add(Me.dtpStartidcard)
        Me.GroupBox30.Controls.Add(Me.cboEducation)
        Me.GroupBox30.Controls.Add(Me.lblEducation)
        Me.GroupBox30.Controls.Add(Me.lblM)
        Me.GroupBox30.Controls.Add(Me.lblIssuedatdistrict)
        Me.GroupBox30.Controls.Add(Me.lblKg)
        Me.GroupBox30.Controls.Add(Me.lblIssuedatprovince)
        Me.GroupBox30.Controls.Add(Me.lblYear)
        Me.GroupBox30.Controls.Add(Me.txtNickname)
        Me.GroupBox30.Controls.Add(Me.lblNickname)
        Me.GroupBox30.Controls.Add(Me.cboBlood)
        Me.GroupBox30.Controls.Add(Me.lblGblood)
        Me.GroupBox30.Controls.Add(Me.lblEndcard)
        Me.GroupBox30.Controls.Add(Me.txtHeight)
        Me.GroupBox30.Controls.Add(Me.lblStartcard)
        Me.GroupBox30.Controls.Add(Me.lblHeight)
        Me.GroupBox30.Controls.Add(Me.txtWeight)
        Me.GroupBox30.Controls.Add(Me.txtIdcard)
        Me.GroupBox30.Controls.Add(Me.txtAge)
        Me.GroupBox30.Controls.Add(Me.lblWeight)
        Me.GroupBox30.Controls.Add(Me.lblIdcard)
        Me.GroupBox30.Controls.Add(Me.lblAge)
        Me.GroupBox30.Controls.Add(Me.cboReligion)
        Me.GroupBox30.Controls.Add(Me.lblReligion)
        Me.GroupBox30.Controls.Add(Me.cboNationality)
        Me.GroupBox30.Controls.Add(Me.lblNationality)
        Me.GroupBox30.Controls.Add(Me.lblBirthday)
        Me.GroupBox30.Controls.Add(Me.txtLname)
        Me.GroupBox30.Controls.Add(Me.txtFname)
        Me.GroupBox30.Controls.Add(Me.cboTitle)
        Me.GroupBox30.Controls.Add(Me.lblLastName)
        Me.GroupBox30.Controls.Add(Me.lblTitle)
        Me.GroupBox30.Controls.Add(Me.lblName)
        Me.GroupBox30.Controls.Add(Me.lblEmployeeData)
        Me.GroupBox30.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox30.Location = New System.Drawing.Point(13, 394)
        Me.GroupBox30.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox30.Name = "GroupBox30"
        Me.GroupBox30.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox30.Size = New System.Drawing.Size(2162, 248)
        Me.GroupBox30.TabIndex = 106
        Me.GroupBox30.TabStop = False
        Me.GroupBox30.Text = "ข้อมูลผู้สมัคร"
        '
        'txtByprovince
        '
        Me.txtByprovince.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtByprovince.Location = New System.Drawing.Point(1914, 177)
        Me.txtByprovince.Margin = New System.Windows.Forms.Padding(6)
        Me.txtByprovince.Name = "txtByprovince"
        Me.txtByprovince.Size = New System.Drawing.Size(150, 51)
        Me.txtByprovince.TabIndex = 64
        '
        'txtBycanton
        '
        Me.txtBycanton.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtBycanton.Location = New System.Drawing.Point(1916, 107)
        Me.txtBycanton.Margin = New System.Windows.Forms.Padding(6)
        Me.txtBycanton.Name = "txtBycanton"
        Me.txtBycanton.Size = New System.Drawing.Size(150, 51)
        Me.txtBycanton.TabIndex = 63
        '
        'dtpBirthday
        '
        Me.dtpBirthday.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpBirthday.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBirthday.Location = New System.Drawing.Point(990, 38)
        Me.dtpBirthday.Margin = New System.Windows.Forms.Padding(6)
        Me.dtpBirthday.Name = "dtpBirthday"
        Me.dtpBirthday.Size = New System.Drawing.Size(196, 51)
        Me.dtpBirthday.TabIndex = 26
        '
        'dtpEndidcard
        '
        Me.dtpEndidcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpEndidcard.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndidcard.Location = New System.Drawing.Point(1456, 177)
        Me.dtpEndidcard.Margin = New System.Windows.Forms.Padding(6)
        Me.dtpEndidcard.Name = "dtpEndidcard"
        Me.dtpEndidcard.Size = New System.Drawing.Size(196, 51)
        Me.dtpEndidcard.TabIndex = 38
        '
        'dtpStartidcard
        '
        Me.dtpStartidcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpStartidcard.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartidcard.Location = New System.Drawing.Point(1456, 110)
        Me.dtpStartidcard.Margin = New System.Windows.Forms.Padding(6)
        Me.dtpStartidcard.Name = "dtpStartidcard"
        Me.dtpStartidcard.Size = New System.Drawing.Size(196, 51)
        Me.dtpStartidcard.TabIndex = 32
        '
        'cboEducation
        '
        Me.cboEducation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEducation.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboEducation.FormattingEnabled = True
        Me.cboEducation.Items.AddRange(New Object() {"ปริญญาโท", "ปริญญาตรี", "ปวส.", "ปวช.", "ม.6", "ม.3", "ป.6", "ป.4", "อื่นๆ"})
        Me.cboEducation.Location = New System.Drawing.Point(638, 179)
        Me.cboEducation.Margin = New System.Windows.Forms.Padding(6)
        Me.cboEducation.Name = "cboEducation"
        Me.cboEducation.Size = New System.Drawing.Size(156, 51)
        Me.cboEducation.TabIndex = 36
        '
        'lblEducation
        '
        Me.lblEducation.AutoSize = True
        Me.lblEducation.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblEducation.Location = New System.Drawing.Point(520, 185)
        Me.lblEducation.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblEducation.Name = "lblEducation"
        Me.lblEducation.Size = New System.Drawing.Size(111, 43)
        Me.lblEducation.TabIndex = 58
        Me.lblEducation.Text = "การศึกษา :"
        '
        'lblM
        '
        Me.lblM.AutoSize = True
        Me.lblM.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblM.Location = New System.Drawing.Point(1072, 115)
        Me.lblM.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblM.Name = "lblM"
        Me.lblM.Size = New System.Drawing.Size(51, 43)
        Me.lblM.TabIndex = 57
        Me.lblM.Text = "ซม."
        '
        'lblIssuedatdistrict
        '
        Me.lblIssuedatdistrict.AutoSize = True
        Me.lblIssuedatdistrict.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblIssuedatdistrict.Location = New System.Drawing.Point(1730, 115)
        Me.lblIssuedatdistrict.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblIssuedatdistrict.Name = "lblIssuedatdistrict"
        Me.lblIssuedatdistrict.Size = New System.Drawing.Size(174, 43)
        Me.lblIssuedatdistrict.TabIndex = 62
        Me.lblIssuedatdistrict.Text = "ออกให้ ณ อำเภอ :"
        '
        'lblKg
        '
        Me.lblKg.AutoSize = True
        Me.lblKg.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblKg.Location = New System.Drawing.Point(720, 115)
        Me.lblKg.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblKg.Name = "lblKg"
        Me.lblKg.Size = New System.Drawing.Size(50, 43)
        Me.lblKg.TabIndex = 56
        Me.lblKg.Text = "กก."
        '
        'lblIssuedatprovince
        '
        Me.lblIssuedatprovince.AutoSize = True
        Me.lblIssuedatprovince.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblIssuedatprovince.Location = New System.Drawing.Point(1790, 185)
        Me.lblIssuedatprovince.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblIssuedatprovince.Name = "lblIssuedatprovince"
        Me.lblIssuedatprovince.Size = New System.Drawing.Size(112, 43)
        Me.lblIssuedatprovince.TabIndex = 61
        Me.lblIssuedatprovince.Text = "ณ จังหวัด :"
        '
        'lblYear
        '
        Me.lblYear.AutoSize = True
        Me.lblYear.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblYear.Location = New System.Drawing.Point(436, 115)
        Me.lblYear.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(33, 43)
        Me.lblYear.TabIndex = 55
        Me.lblYear.Text = "ปี"
        '
        'txtNickname
        '
        Me.txtNickname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtNickname.Location = New System.Drawing.Point(128, 110)
        Me.txtNickname.Margin = New System.Windows.Forms.Padding(6)
        Me.txtNickname.Name = "txtNickname"
        Me.txtNickname.Size = New System.Drawing.Size(100, 51)
        Me.txtNickname.TabIndex = 28
        '
        'lblNickname
        '
        Me.lblNickname.AutoSize = True
        Me.lblNickname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblNickname.Location = New System.Drawing.Point(40, 115)
        Me.lblNickname.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblNickname.Name = "lblNickname"
        Me.lblNickname.Size = New System.Drawing.Size(89, 43)
        Me.lblNickname.TabIndex = 53
        Me.lblNickname.Text = "ชื่อเล่น :"
        '
        'cboBlood
        '
        Me.cboBlood.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBlood.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboBlood.FormattingEnabled = True
        Me.cboBlood.Items.AddRange(New Object() {"A", "B", "O", "AB", "อื่นๆ"})
        Me.cboBlood.Location = New System.Drawing.Point(364, 179)
        Me.cboBlood.Margin = New System.Windows.Forms.Padding(6)
        Me.cboBlood.Name = "cboBlood"
        Me.cboBlood.Size = New System.Drawing.Size(80, 51)
        Me.cboBlood.TabIndex = 35
        '
        'lblGblood
        '
        Me.lblGblood.AutoSize = True
        Me.lblGblood.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblGblood.Location = New System.Drawing.Point(250, 185)
        Me.lblGblood.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblGblood.Name = "lblGblood"
        Me.lblGblood.Size = New System.Drawing.Size(112, 43)
        Me.lblGblood.TabIndex = 51
        Me.lblGblood.Text = "กรุ๊ปเลือด :"
        '
        'lblEndcard
        '
        Me.lblEndcard.AutoSize = True
        Me.lblEndcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblEndcard.Location = New System.Drawing.Point(1312, 185)
        Me.lblEndcard.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblEndcard.Name = "lblEndcard"
        Me.lblEndcard.Size = New System.Drawing.Size(130, 43)
        Me.lblEndcard.TabIndex = 57
        Me.lblEndcard.Text = "วันหมดอายุ :"
        '
        'txtHeight
        '
        Me.txtHeight.CausesValidation = False
        Me.txtHeight.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHeight.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtHeight.Location = New System.Drawing.Point(990, 110)
        Me.txtHeight.Margin = New System.Windows.Forms.Padding(6)
        Me.txtHeight.Name = "txtHeight"
        Me.txtHeight.Size = New System.Drawing.Size(66, 51)
        Me.txtHeight.TabIndex = 31
        '
        'lblStartcard
        '
        Me.lblStartcard.AutoSize = True
        Me.lblStartcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStartcard.Location = New System.Drawing.Point(1312, 115)
        Me.lblStartcard.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblStartcard.Name = "lblStartcard"
        Me.lblStartcard.Size = New System.Drawing.Size(133, 43)
        Me.lblStartcard.TabIndex = 55
        Me.lblStartcard.Text = "วันออกบัตร :"
        '
        'lblHeight
        '
        Me.lblHeight.AutoSize = True
        Me.lblHeight.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblHeight.Location = New System.Drawing.Point(900, 115)
        Me.lblHeight.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblHeight.Name = "lblHeight"
        Me.lblHeight.Size = New System.Drawing.Size(94, 43)
        Me.lblHeight.TabIndex = 49
        Me.lblHeight.Text = "ส่วนสูง :"
        '
        'txtWeight
        '
        Me.txtWeight.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtWeight.Location = New System.Drawing.Point(638, 110)
        Me.txtWeight.Margin = New System.Windows.Forms.Padding(6)
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Size = New System.Drawing.Size(68, 51)
        Me.txtWeight.TabIndex = 30
        '
        'txtIdcard
        '
        Me.txtIdcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtIdcard.Location = New System.Drawing.Point(1456, 40)
        Me.txtIdcard.Margin = New System.Windows.Forms.Padding(6)
        Me.txtIdcard.Name = "txtIdcard"
        Me.txtIdcard.Size = New System.Drawing.Size(268, 51)
        Me.txtIdcard.TabIndex = 27
        '
        'txtAge
        '
        Me.txtAge.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtAge.Location = New System.Drawing.Point(364, 110)
        Me.txtAge.Margin = New System.Windows.Forms.Padding(6)
        Me.txtAge.Name = "txtAge"
        Me.txtAge.Size = New System.Drawing.Size(58, 51)
        Me.txtAge.TabIndex = 29
        '
        'lblWeight
        '
        Me.lblWeight.AutoSize = True
        Me.lblWeight.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(542, 115)
        Me.lblWeight.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(97, 43)
        Me.lblWeight.TabIndex = 46
        Me.lblWeight.Text = "น้ำหนัก :"
        '
        'lblIdcard
        '
        Me.lblIdcard.AutoSize = True
        Me.lblIdcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblIdcard.Location = New System.Drawing.Point(1204, 44)
        Me.lblIdcard.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblIdcard.Name = "lblIdcard"
        Me.lblIdcard.Size = New System.Drawing.Size(234, 43)
        Me.lblIdcard.TabIndex = 52
        Me.lblIdcard.Text = "บัตรประจำตัวประชาชน :"
        '
        'lblAge
        '
        Me.lblAge.AutoSize = True
        Me.lblAge.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblAge.Location = New System.Drawing.Point(302, 115)
        Me.lblAge.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(63, 43)
        Me.lblAge.TabIndex = 45
        Me.lblAge.Text = "อายุ :"
        '
        'cboReligion
        '
        Me.cboReligion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReligion.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboReligion.FormattingEnabled = True
        Me.cboReligion.Items.AddRange(New Object() {"พุทธ", "อิสลาม", "คริสต์", "ฮินดู", "ซิก", "อื่นๆ"})
        Me.cboReligion.Location = New System.Drawing.Point(128, 179)
        Me.cboReligion.Margin = New System.Windows.Forms.Padding(6)
        Me.cboReligion.Name = "cboReligion"
        Me.cboReligion.Size = New System.Drawing.Size(100, 51)
        Me.cboReligion.TabIndex = 34
        '
        'lblReligion
        '
        Me.lblReligion.AutoSize = True
        Me.lblReligion.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblReligion.Location = New System.Drawing.Point(38, 185)
        Me.lblReligion.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblReligion.Name = "lblReligion"
        Me.lblReligion.Size = New System.Drawing.Size(89, 43)
        Me.lblReligion.TabIndex = 43
        Me.lblReligion.Text = "ศาสนา :"
        '
        'cboNationality
        '
        Me.cboNationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNationality.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboNationality.FormattingEnabled = True
        Me.cboNationality.Items.AddRange(New Object() {"ไทย", "พม่า", "ลาว", "อื่นๆ"})
        Me.cboNationality.Location = New System.Drawing.Point(990, 179)
        Me.cboNationality.Margin = New System.Windows.Forms.Padding(6)
        Me.cboNationality.Name = "cboNationality"
        Me.cboNationality.Size = New System.Drawing.Size(110, 51)
        Me.cboNationality.TabIndex = 37
        '
        'lblNationality
        '
        Me.lblNationality.AutoSize = True
        Me.lblNationality.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblNationality.Location = New System.Drawing.Point(896, 185)
        Me.lblNationality.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblNationality.Name = "lblNationality"
        Me.lblNationality.Size = New System.Drawing.Size(96, 43)
        Me.lblNationality.TabIndex = 41
        Me.lblNationality.Text = "สัญชาติ :"
        '
        'lblBirthday
        '
        Me.lblBirthday.AutoSize = True
        Me.lblBirthday.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblBirthday.Location = New System.Drawing.Point(804, 44)
        Me.lblBirthday.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblBirthday.Name = "lblBirthday"
        Me.lblBirthday.Size = New System.Drawing.Size(164, 43)
        Me.lblBirthday.TabIndex = 39
        Me.lblBirthday.Text = "วัน/เดือน/ปี เกิด :"
        '
        'txtLname
        '
        Me.txtLname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtLname.Location = New System.Drawing.Point(638, 40)
        Me.txtLname.Margin = New System.Windows.Forms.Padding(6)
        Me.txtLname.Name = "txtLname"
        Me.txtLname.Size = New System.Drawing.Size(148, 51)
        Me.txtLname.TabIndex = 25
        '
        'txtFname
        '
        Me.txtFname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtFname.Location = New System.Drawing.Point(364, 40)
        Me.txtFname.Margin = New System.Windows.Forms.Padding(6)
        Me.txtFname.Name = "txtFname"
        Me.txtFname.Size = New System.Drawing.Size(148, 51)
        Me.txtFname.TabIndex = 24
        '
        'cboTitle
        '
        Me.cboTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTitle.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboTitle.FormattingEnabled = True
        Me.cboTitle.Items.AddRange(New Object() {"นาย", "นาง", "นางสาว", "ว่าที่ร้อยตรี", "Mr.", "Ms.", "Mrs.", ""})
        Me.cboTitle.Location = New System.Drawing.Point(128, 40)
        Me.cboTitle.Margin = New System.Windows.Forms.Padding(6)
        Me.cboTitle.Name = "cboTitle"
        Me.cboTitle.Size = New System.Drawing.Size(100, 51)
        Me.cboTitle.TabIndex = 23
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastName.Location = New System.Drawing.Point(536, 44)
        Me.lblLastName.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(107, 43)
        Me.lblLastName.TabIndex = 35
        Me.lblLastName.Text = "นามสกุล :"
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(12, 44)
        Me.lblTitle.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(115, 43)
        Me.lblTitle.TabIndex = 34
        Me.lblTitle.Text = "คำนำหน้า :"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblName.Location = New System.Drawing.Point(312, 44)
        Me.lblName.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(56, 43)
        Me.lblName.TabIndex = 33
        Me.lblName.Text = "ชื่อ :"
        '
        'lblEmployeeData
        '
        Me.lblEmployeeData.AutoSize = True
        Me.lblEmployeeData.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblEmployeeData.Location = New System.Drawing.Point(116, -6)
        Me.lblEmployeeData.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblEmployeeData.Name = "lblEmployeeData"
        Me.lblEmployeeData.Size = New System.Drawing.Size(0, 26)
        Me.lblEmployeeData.TabIndex = 32
        '
        'butSave1
        '
        Me.butSave1.Location = New System.Drawing.Point(457, 1328)
        Me.butSave1.Margin = New System.Windows.Forms.Padding(6)
        Me.butSave1.Name = "butSave1"
        Me.butSave1.Size = New System.Drawing.Size(160, 58)
        Me.butSave1.TabIndex = 78
        Me.butSave1.Text = "บันทึก"
        Me.butSave1.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtProvince)
        Me.GroupBox5.Controls.Add(Me.txtDistrict)
        Me.GroupBox5.Controls.Add(Me.txtCanton)
        Me.GroupBox5.Controls.Add(Me.txtZipcode)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.txtMobile)
        Me.GroupBox5.Controls.Add(Me.Label18)
        Me.GroupBox5.Controls.Add(Me.txtPhone)
        Me.GroupBox5.Controls.Add(Me.Label19)
        Me.GroupBox5.Controls.Add(Me.txtAddress)
        Me.GroupBox5.Controls.Add(Me.Label20)
        Me.GroupBox5.Controls.Add(Me.Label21)
        Me.GroupBox5.Controls.Add(Me.Label22)
        Me.GroupBox5.Controls.Add(Me.txtMoo)
        Me.GroupBox5.Controls.Add(Me.Label23)
        Me.GroupBox5.Controls.Add(Me.Label76)
        Me.GroupBox5.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(13, 836)
        Me.GroupBox5.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox5.Size = New System.Drawing.Size(1968, 118)
        Me.GroupBox5.TabIndex = 102
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "ที่อยู่ตามทะเบียนบ้าน"
        '
        'txtProvince
        '
        Me.txtProvince.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtProvince.Location = New System.Drawing.Point(924, 42)
        Me.txtProvince.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtProvince.Name = "txtProvince"
        Me.txtProvince.Size = New System.Drawing.Size(160, 51)
        Me.txtProvince.TabIndex = 66
        '
        'txtDistrict
        '
        Me.txtDistrict.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtDistrict.Location = New System.Drawing.Point(683, 42)
        Me.txtDistrict.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtDistrict.Name = "txtDistrict"
        Me.txtDistrict.Size = New System.Drawing.Size(153, 51)
        Me.txtDistrict.TabIndex = 67
        '
        'txtCanton
        '
        Me.txtCanton.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtCanton.Location = New System.Drawing.Point(435, 42)
        Me.txtCanton.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtCanton.Name = "txtCanton"
        Me.txtCanton.Size = New System.Drawing.Size(153, 51)
        Me.txtCanton.TabIndex = 66
        '
        'txtZipcode
        '
        Me.txtZipcode.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtZipcode.Location = New System.Drawing.Point(1248, 42)
        Me.txtZipcode.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtZipcode.Name = "txtZipcode"
        Me.txtZipcode.Size = New System.Drawing.Size(95, 51)
        Me.txtZipcode.TabIndex = 57
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.Location = New System.Drawing.Point(1100, 48)
        Me.Label8.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(137, 43)
        Me.Label8.TabIndex = 64
        Me.Label8.Text = "รหัสไปรษณี :"
        '
        'txtMobile
        '
        Me.txtMobile.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtMobile.Location = New System.Drawing.Point(1525, 44)
        Me.txtMobile.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.Size = New System.Drawing.Size(148, 51)
        Me.txtMobile.TabIndex = 58
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label18.Location = New System.Drawing.Point(1676, 48)
        Me.Label18.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(107, 43)
        Me.Label18.TabIndex = 58
        Me.Label18.Text = "โทรศัพท์ :"
        '
        'txtPhone
        '
        Me.txtPhone.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPhone.Location = New System.Drawing.Point(1789, 44)
        Me.txtPhone.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(148, 51)
        Me.txtPhone.TabIndex = 59
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label19.Location = New System.Drawing.Point(1357, 48)
        Me.Label19.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(159, 43)
        Me.Label19.TabIndex = 51
        Me.Label19.Text = "โทรศัพท์มือถือ :"
        '
        'txtAddress
        '
        Me.txtAddress.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(123, 44)
        Me.txtAddress.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(73, 51)
        Me.txtAddress.TabIndex = 52
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label20.Location = New System.Drawing.Point(836, 48)
        Me.Label20.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(90, 43)
        Me.Label20.TabIndex = 46
        Me.Label20.Text = "จังหวัด :"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label21.Location = New System.Drawing.Point(597, 48)
        Me.Label21.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(83, 43)
        Me.Label21.TabIndex = 44
        Me.Label21.Text = "อำเภอ :"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label22.Location = New System.Drawing.Point(360, 48)
        Me.Label22.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(79, 43)
        Me.Label22.TabIndex = 43
        Me.Label22.Text = "ตำบล :"
        '
        'txtMoo
        '
        Me.txtMoo.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtMoo.Location = New System.Drawing.Point(284, 44)
        Me.txtMoo.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtMoo.Name = "txtMoo"
        Me.txtMoo.Size = New System.Drawing.Size(63, 51)
        Me.txtMoo.TabIndex = 53
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label23.Location = New System.Drawing.Point(11, 50)
        Me.Label23.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(113, 43)
        Me.Label23.TabIndex = 40
        Me.Label23.Text = "บ้านเลขที่ :"
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label76.Location = New System.Drawing.Point(208, 50)
        Me.Label76.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(72, 43)
        Me.Label76.TabIndex = 39
        Me.Label76.Text = "หมู่ที่ :"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.txtNowprovince)
        Me.GroupBox6.Controls.Add(Me.txtNowdistrict)
        Me.GroupBox6.Controls.Add(Me.txtNowphone)
        Me.GroupBox6.Controls.Add(Me.txtNowcanton)
        Me.GroupBox6.Controls.Add(Me.lblPostcode)
        Me.GroupBox6.Controls.Add(Me.lblTelephone)
        Me.GroupBox6.Controls.Add(Me.txtSmartphone)
        Me.GroupBox6.Controls.Add(Me.txtNowaddress)
        Me.GroupBox6.Controls.Add(Me.lblProvince)
        Me.GroupBox6.Controls.Add(Me.lblDistrict)
        Me.GroupBox6.Controls.Add(Me.lblSubdistrict)
        Me.GroupBox6.Controls.Add(Me.txtNowmoo)
        Me.GroupBox6.Controls.Add(Me.lblNum)
        Me.GroupBox6.Controls.Add(Me.lblMu)
        Me.GroupBox6.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(13, 965)
        Me.GroupBox6.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox6.Size = New System.Drawing.Size(1968, 122)
        Me.GroupBox6.TabIndex = 101
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "ที่อยู่ปัจจุบัน"
        '
        'txtNowprovince
        '
        Me.txtNowprovince.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtNowprovince.Location = New System.Drawing.Point(924, 42)
        Me.txtNowprovince.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtNowprovince.Name = "txtNowprovince"
        Me.txtNowprovince.Size = New System.Drawing.Size(160, 51)
        Me.txtNowprovince.TabIndex = 70
        '
        'txtNowdistrict
        '
        Me.txtNowdistrict.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtNowdistrict.Location = New System.Drawing.Point(683, 42)
        Me.txtNowdistrict.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtNowdistrict.Name = "txtNowdistrict"
        Me.txtNowdistrict.Size = New System.Drawing.Size(153, 51)
        Me.txtNowdistrict.TabIndex = 71
        '
        'txtNowphone
        '
        Me.txtNowphone.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtNowphone.Location = New System.Drawing.Point(1248, 42)
        Me.txtNowphone.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtNowphone.Name = "txtNowphone"
        Me.txtNowphone.Size = New System.Drawing.Size(95, 51)
        Me.txtNowphone.TabIndex = 69
        '
        'txtNowcanton
        '
        Me.txtNowcanton.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtNowcanton.Location = New System.Drawing.Point(435, 44)
        Me.txtNowcanton.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtNowcanton.Name = "txtNowcanton"
        Me.txtNowcanton.Size = New System.Drawing.Size(153, 51)
        Me.txtNowcanton.TabIndex = 68
        '
        'lblPostcode
        '
        Me.lblPostcode.AutoSize = True
        Me.lblPostcode.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblPostcode.Location = New System.Drawing.Point(1100, 48)
        Me.lblPostcode.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblPostcode.Name = "lblPostcode"
        Me.lblPostcode.Size = New System.Drawing.Size(137, 43)
        Me.lblPostcode.TabIndex = 64
        Me.lblPostcode.Text = "รหัสไปรษณี :"
        '
        'lblTelephone
        '
        Me.lblTelephone.AutoSize = True
        Me.lblTelephone.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTelephone.Location = New System.Drawing.Point(1408, 48)
        Me.lblTelephone.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblTelephone.Name = "lblTelephone"
        Me.lblTelephone.Size = New System.Drawing.Size(107, 43)
        Me.lblTelephone.TabIndex = 58
        Me.lblTelephone.Text = "โทรศัพท์ :"
        '
        'txtSmartphone
        '
        Me.txtSmartphone.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtSmartphone.Location = New System.Drawing.Point(1525, 42)
        Me.txtSmartphone.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtSmartphone.Name = "txtSmartphone"
        Me.txtSmartphone.Size = New System.Drawing.Size(148, 51)
        Me.txtSmartphone.TabIndex = 66
        '
        'txtNowaddress
        '
        Me.txtNowaddress.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtNowaddress.Location = New System.Drawing.Point(120, 44)
        Me.txtNowaddress.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtNowaddress.Name = "txtNowaddress"
        Me.txtNowaddress.Size = New System.Drawing.Size(76, 51)
        Me.txtNowaddress.TabIndex = 60
        '
        'lblProvince
        '
        Me.lblProvince.AutoSize = True
        Me.lblProvince.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProvince.Location = New System.Drawing.Point(836, 48)
        Me.lblProvince.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblProvince.Name = "lblProvince"
        Me.lblProvince.Size = New System.Drawing.Size(90, 43)
        Me.lblProvince.TabIndex = 46
        Me.lblProvince.Text = "จังหวัด :"
        '
        'lblDistrict
        '
        Me.lblDistrict.AutoSize = True
        Me.lblDistrict.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblDistrict.Location = New System.Drawing.Point(597, 48)
        Me.lblDistrict.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblDistrict.Name = "lblDistrict"
        Me.lblDistrict.Size = New System.Drawing.Size(83, 43)
        Me.lblDistrict.TabIndex = 44
        Me.lblDistrict.Text = "อำเภอ :"
        '
        'lblSubdistrict
        '
        Me.lblSubdistrict.AutoSize = True
        Me.lblSubdistrict.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSubdistrict.Location = New System.Drawing.Point(360, 48)
        Me.lblSubdistrict.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblSubdistrict.Name = "lblSubdistrict"
        Me.lblSubdistrict.Size = New System.Drawing.Size(79, 43)
        Me.lblSubdistrict.TabIndex = 43
        Me.lblSubdistrict.Text = "ตำบล :"
        '
        'txtNowmoo
        '
        Me.txtNowmoo.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtNowmoo.Location = New System.Drawing.Point(284, 44)
        Me.txtNowmoo.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtNowmoo.Name = "txtNowmoo"
        Me.txtNowmoo.Size = New System.Drawing.Size(63, 51)
        Me.txtNowmoo.TabIndex = 61
        '
        'lblNum
        '
        Me.lblNum.AutoSize = True
        Me.lblNum.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblNum.Location = New System.Drawing.Point(11, 48)
        Me.lblNum.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblNum.Name = "lblNum"
        Me.lblNum.Size = New System.Drawing.Size(113, 43)
        Me.lblNum.TabIndex = 40
        Me.lblNum.Text = "บ้านเลขที่ :"
        '
        'lblMu
        '
        Me.lblMu.AutoSize = True
        Me.lblMu.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblMu.Location = New System.Drawing.Point(211, 48)
        Me.lblMu.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.lblMu.Name = "lblMu"
        Me.lblMu.Size = New System.Drawing.Size(72, 43)
        Me.lblMu.TabIndex = 39
        Me.lblMu.Text = "หมู่ที่ :"
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.RadioButton10)
        Me.GroupBox11.Controls.Add(Me.rdbMan)
        Me.GroupBox11.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox11.Location = New System.Drawing.Point(1347, 668)
        Me.GroupBox11.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox11.Size = New System.Drawing.Size(168, 161)
        Me.GroupBox11.TabIndex = 49
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "เพศ"
        '
        'RadioButton10
        '
        Me.RadioButton10.AutoSize = True
        Me.RadioButton10.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.RadioButton10.Location = New System.Drawing.Point(37, 106)
        Me.RadioButton10.Margin = New System.Windows.Forms.Padding(4)
        Me.RadioButton10.Name = "RadioButton10"
        Me.RadioButton10.Size = New System.Drawing.Size(92, 47)
        Me.RadioButton10.TabIndex = 51
        Me.RadioButton10.TabStop = True
        Me.RadioButton10.Text = "หญิง"
        Me.RadioButton10.UseVisualStyleBackColor = True
        '
        'rdbMan
        '
        Me.rdbMan.AutoSize = True
        Me.rdbMan.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbMan.Location = New System.Drawing.Point(37, 44)
        Me.rdbMan.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbMan.Name = "rdbMan"
        Me.rdbMan.Size = New System.Drawing.Size(83, 47)
        Me.rdbMan.TabIndex = 50
        Me.rdbMan.TabStop = True
        Me.rdbMan.Text = "ชาย"
        Me.rdbMan.UseVisualStyleBackColor = True
        '
        'GroupBox29
        '
        Me.GroupBox29.Controls.Add(Me.dtpContain)
        Me.GroupBox29.Controls.Add(Me.dtpStart)
        Me.GroupBox29.Controls.Add(Me.dtpRegis)
        Me.GroupBox29.Controls.Add(Me.txtAccountnum)
        Me.GroupBox29.Controls.Add(Me.Label44)
        Me.GroupBox29.Controls.Add(Me.Label77)
        Me.GroupBox29.Controls.Add(Me.Label78)
        Me.GroupBox29.Controls.Add(Me.Label4)
        Me.GroupBox29.Controls.Add(Me.cboDep)
        Me.GroupBox29.Controls.Add(Me.Label71)
        Me.GroupBox29.Controls.Add(Me.txtId)
        Me.GroupBox29.Controls.Add(Me.Label70)
        Me.GroupBox29.Controls.Add(Me.txtWage)
        Me.GroupBox29.Controls.Add(Me.Label5)
        Me.GroupBox29.Controls.Add(Me.cboPosition)
        Me.GroupBox29.Controls.Add(Me.Label72)
        Me.GroupBox29.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox29.Location = New System.Drawing.Point(13, 8)
        Me.GroupBox29.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox29.Name = "GroupBox29"
        Me.GroupBox29.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox29.Size = New System.Drawing.Size(1660, 208)
        Me.GroupBox29.TabIndex = 39
        Me.GroupBox29.TabStop = False
        Me.GroupBox29.Text = "ข้อมูลทั่วไป"
        '
        'dtpContain
        '
        Me.dtpContain.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpContain.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpContain.Location = New System.Drawing.Point(1180, 131)
        Me.dtpContain.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.dtpContain.Name = "dtpContain"
        Me.dtpContain.Size = New System.Drawing.Size(222, 51)
        Me.dtpContain.TabIndex = 44
        '
        'dtpStart
        '
        Me.dtpStart.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStart.Location = New System.Drawing.Point(638, 134)
        Me.dtpStart.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.dtpStart.Name = "dtpStart"
        Me.dtpStart.Size = New System.Drawing.Size(222, 51)
        Me.dtpStart.TabIndex = 43
        '
        'dtpRegis
        '
        Me.dtpRegis.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpRegis.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpRegis.Location = New System.Drawing.Point(182, 131)
        Me.dtpRegis.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.dtpRegis.Name = "dtpRegis"
        Me.dtpRegis.Size = New System.Drawing.Size(222, 51)
        Me.dtpRegis.TabIndex = 42
        '
        'txtAccountnum
        '
        Me.txtAccountnum.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccountnum.Location = New System.Drawing.Point(1405, 48)
        Me.txtAccountnum.Margin = New System.Windows.Forms.Padding(6)
        Me.txtAccountnum.Name = "txtAccountnum"
        Me.txtAccountnum.Size = New System.Drawing.Size(228, 51)
        Me.txtAccountnum.TabIndex = 41
        Me.txtAccountnum.Text = "1111-2222-3333-4444"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label44.Location = New System.Drawing.Point(13, 140)
        Me.Label44.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(175, 43)
        Me.Label44.TabIndex = 40
        Me.Label44.Text = "วันเขียนใบสมัคร :"
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label77.Location = New System.Drawing.Point(919, 140)
        Me.Label77.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(261, 43)
        Me.Label77.TabIndex = 38
        Me.Label77.Text = "วันเดือนปีที่บรรจุเข้าทำงาน :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label78.Location = New System.Drawing.Point(413, 140)
        Me.Label78.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(216, 43)
        Me.Label78.TabIndex = 36
        Me.Label78.Text = "วันเดือนปีที่เริ่มทำงาน :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(1293, 58)
        Me.Label4.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(119, 43)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "เลขที่บัญชี :"
        '
        'cboDep
        '
        Me.cboDep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDep.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboDep.FormattingEnabled = True
        Me.cboDep.Location = New System.Drawing.Point(457, 50)
        Me.cboDep.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.cboDep.Name = "cboDep"
        Me.cboDep.Size = New System.Drawing.Size(172, 51)
        Me.cboDep.TabIndex = 2
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label71.Location = New System.Drawing.Point(383, 55)
        Me.Label71.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(83, 43)
        Me.Label71.TabIndex = 28
        Me.Label71.Text = "แผนก :"
        '
        'txtId
        '
        Me.txtId.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtId.Location = New System.Drawing.Point(155, 52)
        Me.txtId.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(168, 51)
        Me.txtId.TabIndex = 1
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label70.Location = New System.Drawing.Point(28, 61)
        Me.Label70.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(147, 43)
        Me.Label70.TabIndex = 26
        Me.Label70.Text = "รหัสพนักงาน :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'txtWage
        '
        Me.txtWage.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtWage.Location = New System.Drawing.Point(1136, 51)
        Me.txtWage.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtWage.Name = "txtWage"
        Me.txtWage.Size = New System.Drawing.Size(108, 51)
        Me.txtWage.TabIndex = 4
        Me.txtWage.Text = "100000"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(1028, 56)
        Me.Label5.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 43)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "ค่าจ้าง :"
        '
        'cboPosition
        '
        Me.cboPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPosition.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboPosition.FormattingEnabled = True
        Me.cboPosition.Location = New System.Drawing.Point(797, 46)
        Me.cboPosition.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.cboPosition.Name = "cboPosition"
        Me.cboPosition.Size = New System.Drawing.Size(172, 51)
        Me.cboPosition.TabIndex = 3
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label72.Location = New System.Drawing.Point(692, 54)
        Me.Label72.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(104, 43)
        Me.Label72.TabIndex = 22
        Me.Label72.Text = "ตำแหน่ง :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.chkIdcard)
        Me.GroupBox12.Controls.Add(Me.chkRegisHome)
        Me.GroupBox12.Controls.Add(Me.chkAnother)
        Me.GroupBox12.Controls.Add(Me.chkWork)
        Me.GroupBox12.Controls.Add(Me.chkDrive)
        Me.GroupBox12.Controls.Add(Me.chkMotorcyc)
        Me.GroupBox12.Controls.Add(Me.chkMarry)
        Me.GroupBox12.Controls.Add(Me.chkMilitary)
        Me.GroupBox12.Controls.Add(Me.chkMedicalcer)
        Me.GroupBox12.Controls.Add(Me.chkEducation)
        Me.GroupBox12.Controls.Add(Me.chkPicture)
        Me.GroupBox12.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox12.Location = New System.Drawing.Point(13, 210)
        Me.GroupBox12.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox12.Size = New System.Drawing.Size(1660, 172)
        Me.GroupBox12.TabIndex = 35
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "หลักฐานประกอบการสมัครงาน"
        '
        'chkIdcard
        '
        Me.chkIdcard.AutoSize = True
        Me.chkIdcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkIdcard.Location = New System.Drawing.Point(253, 45)
        Me.chkIdcard.Margin = New System.Windows.Forms.Padding(6)
        Me.chkIdcard.Name = "chkIdcard"
        Me.chkIdcard.Size = New System.Drawing.Size(307, 47)
        Me.chkIdcard.TabIndex = 20
        Me.chkIdcard.Text = "สำเนาบัตรประจำตัวประชาชน"
        Me.chkIdcard.UseVisualStyleBackColor = True
        '
        'chkRegisHome
        '
        Me.chkRegisHome.AutoSize = True
        Me.chkRegisHome.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkRegisHome.Location = New System.Drawing.Point(10, 45)
        Me.chkRegisHome.Margin = New System.Windows.Forms.Padding(6)
        Me.chkRegisHome.Name = "chkRegisHome"
        Me.chkRegisHome.Size = New System.Drawing.Size(211, 47)
        Me.chkRegisHome.TabIndex = 19
        Me.chkRegisHome.Text = "สำเนาทะเบียนบ้าน"
        Me.chkRegisHome.UseVisualStyleBackColor = True
        '
        'chkAnother
        '
        Me.chkAnother.AutoSize = True
        Me.chkAnother.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkAnother.Location = New System.Drawing.Point(1307, 104)
        Me.chkAnother.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.chkAnother.Name = "chkAnother"
        Me.chkAnother.Size = New System.Drawing.Size(93, 47)
        Me.chkAnother.TabIndex = 18
        Me.chkAnother.Text = "อื่นๆ"
        Me.chkAnother.UseVisualStyleBackColor = True
        '
        'chkWork
        '
        Me.chkWork.AutoSize = True
        Me.chkWork.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkWork.Location = New System.Drawing.Point(1088, 104)
        Me.chkWork.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.chkWork.Name = "chkWork"
        Me.chkWork.Size = New System.Drawing.Size(198, 47)
        Me.chkWork.TabIndex = 17
        Me.chkWork.Text = "สำเนาใบผ่านงาน"
        Me.chkWork.UseVisualStyleBackColor = True
        '
        'chkDrive
        '
        Me.chkDrive.AutoSize = True
        Me.chkDrive.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkDrive.Location = New System.Drawing.Point(829, 104)
        Me.chkDrive.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.chkDrive.Name = "chkDrive"
        Me.chkDrive.Size = New System.Drawing.Size(230, 47)
        Me.chkDrive.TabIndex = 16
        Me.chkDrive.Text = "สำเนาใบขับขี่รถยนต์"
        Me.chkDrive.UseVisualStyleBackColor = True
        '
        'chkMotorcyc
        '
        Me.chkMotorcyc.AutoSize = True
        Me.chkMotorcyc.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkMotorcyc.Location = New System.Drawing.Point(251, 104)
        Me.chkMotorcyc.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.chkMotorcyc.Name = "chkMotorcyc"
        Me.chkMotorcyc.Size = New System.Drawing.Size(297, 47)
        Me.chkMotorcyc.TabIndex = 14
        Me.chkMotorcyc.Text = "สำเนาใบขับขี่รถมอเตอร์ไซด์"
        Me.chkMotorcyc.UseVisualStyleBackColor = True
        '
        'chkMarry
        '
        Me.chkMarry.AutoSize = True
        Me.chkMarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkMarry.Location = New System.Drawing.Point(589, 104)
        Me.chkMarry.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.chkMarry.Name = "chkMarry"
        Me.chkMarry.Size = New System.Drawing.Size(179, 47)
        Me.chkMarry.TabIndex = 15
        Me.chkMarry.Text = "สำเนาใบสมรส"
        Me.chkMarry.UseVisualStyleBackColor = True
        '
        'chkMilitary
        '
        Me.chkMilitary.AutoSize = True
        Me.chkMilitary.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkMilitary.Location = New System.Drawing.Point(12, 104)
        Me.chkMilitary.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.chkMilitary.Name = "chkMilitary"
        Me.chkMilitary.Size = New System.Drawing.Size(213, 47)
        Me.chkMilitary.TabIndex = 13
        Me.chkMilitary.Text = "สำเนาใบผ่านทหาร"
        Me.chkMilitary.UseVisualStyleBackColor = True
        '
        'chkMedicalcer
        '
        Me.chkMedicalcer.AutoSize = True
        Me.chkMedicalcer.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkMedicalcer.Location = New System.Drawing.Point(829, 42)
        Me.chkMedicalcer.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.chkMedicalcer.Name = "chkMedicalcer"
        Me.chkMedicalcer.Size = New System.Drawing.Size(186, 47)
        Me.chkMedicalcer.TabIndex = 11
        Me.chkMedicalcer.Text = "ใบรับรองแพทย์"
        Me.chkMedicalcer.UseVisualStyleBackColor = True
        '
        'chkEducation
        '
        Me.chkEducation.AutoSize = True
        Me.chkEducation.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkEducation.Location = New System.Drawing.Point(589, 42)
        Me.chkEducation.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.chkEducation.Name = "chkEducation"
        Me.chkEducation.Size = New System.Drawing.Size(212, 47)
        Me.chkEducation.TabIndex = 10
        Me.chkEducation.Text = "สำเนาวุฒิการศึกษา"
        Me.chkEducation.UseVisualStyleBackColor = True
        '
        'chkPicture
        '
        Me.chkPicture.AutoSize = True
        Me.chkPicture.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkPicture.Location = New System.Drawing.Point(1088, 42)
        Me.chkPicture.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.chkPicture.Name = "chkPicture"
        Me.chkPicture.Size = New System.Drawing.Size(371, 47)
        Me.chkPicture.TabIndex = 12
        Me.chkPicture.Text = "สำเนารูปถ่ายหน้าตรง 1"" จำนวน 1 รูป"
        Me.chkPicture.UseVisualStyleBackColor = True
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.rdbCigaretteno)
        Me.GroupBox10.Controls.Add(Me.rdbCigarette)
        Me.GroupBox10.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox10.Location = New System.Drawing.Point(1051, 668)
        Me.GroupBox10.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox10.Size = New System.Drawing.Size(272, 161)
        Me.GroupBox10.TabIndex = 46
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "ท่านสูบบุหรี่หรือไม่"
        '
        'rdbCigaretteno
        '
        Me.rdbCigaretteno.AutoSize = True
        Me.rdbCigaretteno.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbCigaretteno.Location = New System.Drawing.Point(35, 106)
        Me.rdbCigaretteno.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbCigaretteno.Name = "rdbCigaretteno"
        Me.rdbCigaretteno.Size = New System.Drawing.Size(102, 47)
        Me.rdbCigaretteno.TabIndex = 48
        Me.rdbCigaretteno.TabStop = True
        Me.rdbCigaretteno.Text = "ไม่สูบ"
        Me.rdbCigaretteno.UseVisualStyleBackColor = True
        '
        'rdbCigarette
        '
        Me.rdbCigarette.AutoSize = True
        Me.rdbCigarette.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbCigarette.Location = New System.Drawing.Point(35, 44)
        Me.rdbCigarette.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbCigarette.Name = "rdbCigarette"
        Me.rdbCigarette.Size = New System.Drawing.Size(78, 47)
        Me.rdbCigarette.TabIndex = 47
        Me.rdbCigarette.TabStop = True
        Me.rdbCigarette.Text = "สูบ"
        Me.rdbCigarette.UseVisualStyleBackColor = True
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.rdbGlassesno)
        Me.GroupBox9.Controls.Add(Me.rdbGlasses)
        Me.GroupBox9.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox9.Location = New System.Drawing.Point(427, 668)
        Me.GroupBox9.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox9.Size = New System.Drawing.Size(340, 161)
        Me.GroupBox9.TabIndex = 40
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "สวมแว่นตา/คอนแทคเลนส์"
        '
        'rdbGlassesno
        '
        Me.rdbGlassesno.AutoSize = True
        Me.rdbGlassesno.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbGlassesno.Location = New System.Drawing.Point(43, 106)
        Me.rdbGlassesno.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbGlassesno.Name = "rdbGlassesno"
        Me.rdbGlassesno.Size = New System.Drawing.Size(112, 47)
        Me.rdbGlassesno.TabIndex = 42
        Me.rdbGlassesno.TabStop = True
        Me.rdbGlassesno.Text = "ไม่สวม"
        Me.rdbGlassesno.UseVisualStyleBackColor = True
        '
        'rdbGlasses
        '
        Me.rdbGlasses.AutoSize = True
        Me.rdbGlasses.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbGlasses.Location = New System.Drawing.Point(44, 44)
        Me.rdbGlasses.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbGlasses.Name = "rdbGlasses"
        Me.rdbGlasses.Size = New System.Drawing.Size(88, 47)
        Me.rdbGlasses.TabIndex = 41
        Me.rdbGlasses.TabStop = True
        Me.rdbGlasses.Text = "สวม"
        Me.rdbGlasses.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.rdbPregnantno)
        Me.GroupBox8.Controls.Add(Me.rdbPregnant)
        Me.GroupBox8.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox8.Location = New System.Drawing.Point(805, 668)
        Me.GroupBox8.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox8.Size = New System.Drawing.Size(228, 161)
        Me.GroupBox8.TabIndex = 43
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "ตั้งครรภ์หรือไม่"
        '
        'rdbPregnantno
        '
        Me.rdbPregnantno.AutoSize = True
        Me.rdbPregnantno.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbPregnantno.Location = New System.Drawing.Point(44, 106)
        Me.rdbPregnantno.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbPregnantno.Name = "rdbPregnantno"
        Me.rdbPregnantno.Size = New System.Drawing.Size(146, 48)
        Me.rdbPregnantno.TabIndex = 45
        Me.rdbPregnantno.TabStop = True
        Me.rdbPregnantno.Text = "ไม่ตั้งครรภ์"
        Me.rdbPregnantno.UseVisualStyleBackColor = True
        '
        'rdbPregnant
        '
        Me.rdbPregnant.AutoSize = True
        Me.rdbPregnant.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbPregnant.Location = New System.Drawing.Point(44, 44)
        Me.rdbPregnant.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbPregnant.Name = "rdbPregnant"
        Me.rdbPregnant.Size = New System.Drawing.Size(123, 48)
        Me.rdbPregnant.TabIndex = 44
        Me.rdbPregnant.TabStop = True
        Me.rdbPregnant.Text = "ตั้งครรภ์"
        Me.rdbPregnant.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.rdbOthershome)
        Me.GroupBox4.Controls.Add(Me.rdbMyhome)
        Me.GroupBox4.Controls.Add(Me.rdbFatherhome)
        Me.GroupBox4.Controls.Add(Me.rdbLeasehome)
        Me.GroupBox4.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(16, 1092)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox4.Size = New System.Drawing.Size(824, 122)
        Me.GroupBox4.TabIndex = 27
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "สถานะความเป็นอยู่"
        '
        'rdbOthershome
        '
        Me.rdbOthershome.AutoSize = True
        Me.rdbOthershome.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbOthershome.Location = New System.Drawing.Point(619, 54)
        Me.rdbOthershome.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbOthershome.Name = "rdbOthershome"
        Me.rdbOthershome.Size = New System.Drawing.Size(163, 47)
        Me.rdbOthershome.TabIndex = 71
        Me.rdbOthershome.TabStop = True
        Me.rdbOthershome.Text = "อาศัยกับผู้อื่น"
        Me.rdbOthershome.UseVisualStyleBackColor = True
        '
        'rdbMyhome
        '
        Me.rdbMyhome.AutoSize = True
        Me.rdbMyhome.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbMyhome.Location = New System.Drawing.Point(16, 54)
        Me.rdbMyhome.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbMyhome.Name = "rdbMyhome"
        Me.rdbMyhome.Size = New System.Drawing.Size(152, 47)
        Me.rdbMyhome.TabIndex = 68
        Me.rdbMyhome.TabStop = True
        Me.rdbMyhome.Text = "บ้านส่วนตัว"
        Me.rdbMyhome.UseVisualStyleBackColor = True
        '
        'rdbFatherhome
        '
        Me.rdbFatherhome.AutoSize = True
        Me.rdbFatherhome.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFatherhome.Location = New System.Drawing.Point(371, 54)
        Me.rdbFatherhome.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbFatherhome.Name = "rdbFatherhome"
        Me.rdbFatherhome.Size = New System.Drawing.Size(186, 47)
        Me.rdbFatherhome.TabIndex = 70
        Me.rdbFatherhome.TabStop = True
        Me.rdbFatherhome.Text = "อาศัยบิดามารดา"
        Me.rdbFatherhome.UseVisualStyleBackColor = True
        '
        'rdbLeasehome
        '
        Me.rdbLeasehome.AutoSize = True
        Me.rdbLeasehome.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbLeasehome.Location = New System.Drawing.Point(213, 54)
        Me.rdbLeasehome.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbLeasehome.Name = "rdbLeasehome"
        Me.rdbLeasehome.Size = New System.Drawing.Size(115, 47)
        Me.rdbLeasehome.TabIndex = 69
        Me.rdbLeasehome.TabStop = True
        Me.rdbLeasehome.Text = "บ้านเช่า"
        Me.rdbLeasehome.UseVisualStyleBackColor = True
        '
        'picOneinch
        '
        Me.picOneinch.Location = New System.Drawing.Point(1728, 65)
        Me.picOneinch.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.picOneinch.Name = "picOneinch"
        Me.picOneinch.Size = New System.Drawing.Size(208, 289)
        Me.picOneinch.TabIndex = 20
        Me.picOneinch.TabStop = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Location = New System.Drawing.Point(11, 11)
        Me.TabControl1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(2331, 1531)
        Me.TabControl1.TabIndex = 0
        '
        'frmEmpmonth
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(2537, 1081)
        Me.Controls.Add(Me.TabControl1)
        Me.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.Name = "frmEmpmonth"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "ประวัติพนักงานรายเดือน"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox27.ResumeLayout(False)
        Me.GroupBox27.PerformLayout()
        Me.GroupBox28.ResumeLayout(False)
        Me.GroupBox26.ResumeLayout(False)
        Me.GroupBox26.PerformLayout()
        Me.GroupBox25.ResumeLayout(False)
        Me.GroupBox25.PerformLayout()
        Me.GroupBox24.ResumeLayout(False)
        Me.GroupBox24.PerformLayout()
        Me.GroupBox23.ResumeLayout(False)
        Me.GroupBox23.PerformLayout()
        CType(Me.dgvRef, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox20.ResumeLayout(False)
        Me.GroupBox20.PerformLayout()
        Me.GroupBox31.ResumeLayout(False)
        Me.GroupBox31.PerformLayout()
        Me.GroupBox34.ResumeLayout(False)
        Me.GroupBox34.PerformLayout()
        Me.GroupBox33.ResumeLayout(False)
        Me.GroupBox33.PerformLayout()
        Me.GroupBox32.ResumeLayout(False)
        Me.GroupBox32.PerformLayout()
        CType(Me.dgvLanguage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox22.ResumeLayout(False)
        Me.GroupBox22.PerformLayout()
        Me.GroupBox21.ResumeLayout(False)
        Me.GroupBox21.PerformLayout()
        CType(Me.dgvWorkhis, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox36.ResumeLayout(False)
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox16.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        Me.GroupBox35.ResumeLayout(False)
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox15.PerformLayout()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox18.ResumeLayout(False)
        Me.GroupBox18.PerformLayout()
        Me.GroupBox19.ResumeLayout(False)
        Me.GroupBox19.PerformLayout()
        CType(Me.dgvEdu, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox37.ResumeLayout(False)
        Me.GroupBox37.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox30.ResumeLayout(False)
        Me.GroupBox30.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.GroupBox29.ResumeLayout(False)
        Me.GroupBox29.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.picOneinch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents GroupBox22 As GroupBox
    Friend WithEvents Label57 As Label
    Friend WithEvents Label56 As Label
    Friend WithEvents txtBad As TextBox
    Friend WithEvents txtGood As TextBox
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents GroupBox36 As GroupBox
    Friend WithEvents GroupBox17 As GroupBox
    Friend WithEvents chkDeadmother As CheckBox
    Friend WithEvents txtAgemother As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents txtJobmother As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents txtFnamemother As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents GroupBox16 As GroupBox
    Friend WithEvents chkDeadfather As CheckBox
    Friend WithEvents txtAgefather As TextBox
    Friend WithEvents Label30 As Label
    Friend WithEvents txtJobfather As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents txtFnamefather As TextBox
    Friend WithEvents Label36 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents GroupBox13 As GroupBox
    Friend WithEvents txtOfficezipcode As TextBox
    Friend WithEvents txtOfficeProvince As TextBox
    Friend WithEvents txtOfficecanton As TextBox
    Friend WithEvents txtPhonemarry As TextBox
    Friend WithEvents Label28 As Label
    Friend WithEvents txtOfficemarry As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtOfficedistrict As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtOfficeaddress As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txtOfficemoo As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtJobmarry As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txtFnamemarry As TextBox
    Friend WithEvents Label32 As Label
    Friend WithEvents GroupBox35 As GroupBox
    Friend WithEvents GroupBox15 As GroupBox
    Friend WithEvents Label16 As Label
    Friend WithEvents txtChildlearning As TextBox
    Friend WithEvents GroupBox14 As GroupBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txtChild As TextBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents rdbEarningno As RadioButton
    Friend WithEvents rdbEarning As RadioButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents rdbRegisno As RadioButton
    Friend WithEvents rdbRegis As RadioButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents rdbSeparate As RadioButton
    Friend WithEvents rdbWidowed As RadioButton
    Friend WithEvents rdbDivorce As RadioButton
    Friend WithEvents rdbMarry As RadioButton
    Friend WithEvents rdbSingle As RadioButton
    Friend WithEvents GroupBox18 As GroupBox
    Friend WithEvents rdbAnotherssol As RadioButton
    Friend WithEvents rdbPMilitary As RadioButton
    Friend WithEvents rdbEducate As RadioButton
    Friend WithEvents rdbExcept As RadioButton
    Friend WithEvents txtAnotherssol As TextBox
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents txtProvince As TextBox
    Friend WithEvents txtDistrict As TextBox
    Friend WithEvents txtCanton As TextBox
    Friend WithEvents txtZipcode As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtMobile As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents txtMoo As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents Label76 As Label
    Friend WithEvents GroupBox11 As GroupBox
    Friend WithEvents RadioButton10 As RadioButton
    Friend WithEvents rdbMan As RadioButton
    Friend WithEvents GroupBox29 As GroupBox
    Friend WithEvents Label44 As Label
    Friend WithEvents Label77 As Label
    Friend WithEvents Label78 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents cboDep As ComboBox
    Friend WithEvents Label71 As Label
    Friend WithEvents txtId As TextBox
    Friend WithEvents Label70 As Label
    Friend WithEvents txtWage As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents cboPosition As ComboBox
    Friend WithEvents Label72 As Label
    Friend WithEvents GroupBox12 As GroupBox
    Friend WithEvents chkAnother As CheckBox
    Friend WithEvents chkWork As CheckBox
    Friend WithEvents chkDrive As CheckBox
    Friend WithEvents chkMotorcyc As CheckBox
    Friend WithEvents chkMarry As CheckBox
    Friend WithEvents chkMilitary As CheckBox
    Friend WithEvents chkMedicalcer As CheckBox
    Friend WithEvents chkEducation As CheckBox
    Friend WithEvents chkPicture As CheckBox
    Friend WithEvents GroupBox10 As GroupBox
    Friend WithEvents rdbCigaretteno As RadioButton
    Friend WithEvents rdbCigarette As RadioButton
    Friend WithEvents GroupBox9 As GroupBox
    Friend WithEvents rdbGlassesno As RadioButton
    Friend WithEvents rdbGlasses As RadioButton
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents rdbPregnantno As RadioButton
    Friend WithEvents rdbPregnant As RadioButton
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents rdbOthershome As RadioButton
    Friend WithEvents rdbMyhome As RadioButton
    Friend WithEvents rdbFatherhome As RadioButton
    Friend WithEvents rdbLeasehome As RadioButton
    Friend WithEvents picOneinch As PictureBox
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents GroupBox21 As GroupBox
    Friend WithEvents txtIncome As TextBox
    Friend WithEvents Label55 As Label
    Friend WithEvents dgvWorkhis As DataGridView
    Friend WithEvents txtLastsalary As TextBox
    Friend WithEvents Label47 As Label
    Friend WithEvents txtPosition As TextBox
    Friend WithEvents Label48 As Label
    Friend WithEvents Label51 As Label
    Friend WithEvents txtEnd As TextBox
    Friend WithEvents Label52 As Label
    Friend WithEvents txtStart As TextBox
    Friend WithEvents Label53 As Label
    Friend WithEvents txtOfficename As TextBox
    Friend WithEvents Label54 As Label
    Friend WithEvents GroupBox28 As GroupBox
    Friend WithEvents dtpReady As DateTimePicker
    Friend WithEvents GroupBox26 As GroupBox
    Friend WithEvents txtReltionincom As TextBox
    Friend WithEvents Label69 As Label
    Friend WithEvents txtLnameincom As TextBox
    Friend WithEvents txtFnameincom As TextBox
    Friend WithEvents Label68 As Label
    Friend WithEvents Label67 As Label
    Friend WithEvents GroupBox25 As GroupBox
    Friend WithEvents chkGuarantorno As CheckBox
    Friend WithEvents chkGuarantor As CheckBox
    Friend WithEvents GroupBox24 As GroupBox
    Friend WithEvents Label66 As Label
    Friend WithEvents txtTypingeng As TextBox
    Friend WithEvents chkTypingeng As CheckBox
    Friend WithEvents Label63 As Label
    Friend WithEvents txtTypingth As TextBox
    Friend WithEvents chkTypingth As CheckBox
    Friend WithEvents txtAnotherscom As TextBox
    Friend WithEvents chkAnotherscom As CheckBox
    Friend WithEvents chkPowerpoint As CheckBox
    Friend WithEvents chkExcel As CheckBox
    Friend WithEvents chkWord As CheckBox
    Friend WithEvents GroupBox23 As GroupBox
    Friend WithEvents Label65 As Label
    Friend WithEvents txtPhoneref As TextBox
    Friend WithEvents Label58 As Label
    Friend WithEvents dgvRef As DataGridView
    Friend WithEvents txtPositionref As TextBox
    Friend WithEvents Label59 As Label
    Friend WithEvents txtAddressref As TextBox
    Friend WithEvents Label60 As Label
    Friend WithEvents Label61 As Label
    Friend WithEvents txtRelationref As TextBox
    Friend WithEvents Label62 As Label
    Friend WithEvents txtLnameref As TextBox
    Friend WithEvents txtFnameref As TextBox
    Friend WithEvents Label64 As Label
    Friend WithEvents GroupBox20 As GroupBox
    Friend WithEvents GroupBox31 As GroupBox
    Friend WithEvents rdbSpeakC As RadioButton
    Friend WithEvents rdbSpeakB As RadioButton
    Friend WithEvents rdbSpeakA As RadioButton
    Friend WithEvents GroupBox34 As GroupBox
    Friend WithEvents rdbWriteC As RadioButton
    Friend WithEvents rdbWriteB As RadioButton
    Friend WithEvents rdbWriteA As RadioButton
    Friend WithEvents GroupBox33 As GroupBox
    Friend WithEvents rdbReadC As RadioButton
    Friend WithEvents rdbReadB As RadioButton
    Friend WithEvents rdbReadA As RadioButton
    Friend WithEvents GroupBox32 As GroupBox
    Friend WithEvents rdbUnderstandC As RadioButton
    Friend WithEvents rdbUnderstandB As RadioButton
    Friend WithEvents rdbUnderstandA As RadioButton
    Friend WithEvents dgvLanguage As DataGridView
    Friend WithEvents cboLanguage As ComboBox
    Friend WithEvents Label50 As Label
    Friend WithEvents GroupBox19 As GroupBox
    Friend WithEvents butEdudel As Button
    Friend WithEvents butEdusave As Button
    Friend WithEvents dgvEdu As DataGridView
    Friend WithEvents txtEdugrade As TextBox
    Friend WithEvents Label43 As Label
    Friend WithEvents txtEdumajor As TextBox
    Friend WithEvents Label42 As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents txtEduend As TextBox
    Friend WithEvents Label40 As Label
    Friend WithEvents txtEdustart As TextBox
    Friend WithEvents Label39 As Label
    Friend WithEvents txtEduname As TextBox
    Friend WithEvents Label38 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents txtAccountnum As TextBox
    Friend WithEvents dtpRegis As DateTimePicker
    Friend WithEvents dtpStart As DateTimePicker
    Friend WithEvents dtpContain As DateTimePicker
    Friend WithEvents chkIdcard As CheckBox
    Friend WithEvents chkRegisHome As CheckBox
    Friend WithEvents GroupBox30 As GroupBox
    Friend WithEvents txtByprovince As TextBox
    Friend WithEvents txtBycanton As TextBox
    Friend WithEvents dtpBirthday As DateTimePicker
    Friend WithEvents dtpEndidcard As DateTimePicker
    Friend WithEvents dtpStartidcard As DateTimePicker
    Friend WithEvents cboEducation As ComboBox
    Friend WithEvents lblEducation As Label
    Friend WithEvents lblM As Label
    Friend WithEvents lblIssuedatdistrict As Label
    Friend WithEvents lblKg As Label
    Friend WithEvents lblIssuedatprovince As Label
    Friend WithEvents lblYear As Label
    Friend WithEvents txtNickname As TextBox
    Friend WithEvents lblNickname As Label
    Friend WithEvents cboBlood As ComboBox
    Friend WithEvents lblGblood As Label
    Friend WithEvents lblEndcard As Label
    Friend WithEvents txtHeight As TextBox
    Friend WithEvents lblStartcard As Label
    Friend WithEvents lblHeight As Label
    Friend WithEvents txtWeight As TextBox
    Friend WithEvents txtIdcard As TextBox
    Friend WithEvents txtAge As TextBox
    Friend WithEvents lblWeight As Label
    Friend WithEvents lblIdcard As Label
    Friend WithEvents lblAge As Label
    Friend WithEvents cboReligion As ComboBox
    Friend WithEvents lblReligion As Label
    Friend WithEvents cboNationality As ComboBox
    Friend WithEvents lblNationality As Label
    Friend WithEvents lblBirthday As Label
    Friend WithEvents txtLname As TextBox
    Friend WithEvents txtFname As TextBox
    Friend WithEvents cboTitle As ComboBox
    Friend WithEvents lblLastName As Label
    Friend WithEvents lblTitle As Label
    Friend WithEvents lblName As Label
    Friend WithEvents lblEmployeeData As Label
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents rdbSicknessno As RadioButton
    Friend WithEvents rdbSickness As RadioButton
    Friend WithEvents txtSickness As TextBox
    Friend WithEvents GroupBox37 As GroupBox
    Friend WithEvents rdbContain As RadioButton
    Friend WithEvents rdbTrial As RadioButton
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents txtNowcanton As TextBox
    Friend WithEvents lblPostcode As Label
    Friend WithEvents lblTelephone As Label
    Friend WithEvents txtSmartphone As TextBox
    Friend WithEvents txtNowaddress As TextBox
    Friend WithEvents lblProvince As Label
    Friend WithEvents lblDistrict As Label
    Friend WithEvents lblSubdistrict As Label
    Friend WithEvents txtNowmoo As TextBox
    Friend WithEvents lblNum As Label
    Friend WithEvents lblMu As Label
    Friend WithEvents txtNowprovince As TextBox
    Friend WithEvents txtNowdistrict As TextBox
    Friend WithEvents txtNowphone As TextBox
    Friend WithEvents butClear1 As Button
    Friend WithEvents butDel1 As Button
    Friend WithEvents butSave1 As Button
    Friend WithEvents cboTitlemother As ComboBox
    Friend WithEvents cboTitlefather As ComboBox
    Friend WithEvents cboTitlemarry As ComboBox
    Friend WithEvents cboEdulevel As ComboBox
    Friend WithEvents butLangdel As Button
    Friend WithEvents butLangsave As Button
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents cboTitleref As ComboBox
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents butWorkhisdel As Button
    Friend WithEvents butWorkhissave As Button
    Friend WithEvents butClear4 As Button
    Friend WithEvents butDel4 As Button
    Friend WithEvents butSave4 As Button
    Friend WithEvents butClear3 As Button
    Friend WithEvents butDel3 As Button
    Friend WithEvents butSave3 As Button
    Friend WithEvents GroupBox27 As GroupBox
    Friend WithEvents txtAnothersnew As TextBox
    Friend WithEvents chkAnothersnew As CheckBox
    Friend WithEvents chkEmpOffice As CheckBox
    Friend WithEvents chkAnnouncement As CheckBox
    Friend WithEvents chkSignsrecruit As CheckBox
    Friend WithEvents chkInternet As CheckBox
    Friend WithEvents chkFriend As CheckBox
    Friend WithEvents butDelref As Button
    Friend WithEvents butSaveref As Button
    Friend WithEvents butClear2 As Button
    Friend WithEvents butDel2 As Button
    Friend WithEvents butSave2 As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
End Class
