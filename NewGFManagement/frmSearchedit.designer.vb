﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchedit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.butSearchid = New System.Windows.Forms.Button()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cboDep = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.butSearchdep = New System.Windows.Forms.Button()
        Me.dgvSearch = New System.Windows.Forms.DataGridView()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtLname = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.butSearchname = New System.Windows.Forms.Button()
        Me.txtFname = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.butSearchid)
        Me.GroupBox1.Controls.Add(Me.txtId)
        Me.GroupBox1.Controls.Add(Me.Label70)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 18)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox1.Size = New System.Drawing.Size(800, 156)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "กรอกข้อมูล"
        '
        'butSearchid
        '
        Me.butSearchid.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSearchid.Location = New System.Drawing.Point(525, 39)
        Me.butSearchid.Margin = New System.Windows.Forms.Padding(4)
        Me.butSearchid.Name = "butSearchid"
        Me.butSearchid.Size = New System.Drawing.Size(174, 60)
        Me.butSearchid.TabIndex = 73
        Me.butSearchid.Text = "ค้นหา"
        Me.butSearchid.UseVisualStyleBackColor = True
        '
        'txtId
        '
        Me.txtId.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtId.Location = New System.Drawing.Point(267, 46)
        Me.txtId.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(233, 51)
        Me.txtId.TabIndex = 1
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label70.Location = New System.Drawing.Point(12, 52)
        Me.Label70.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(234, 43)
        Me.Label70.TabIndex = 26
        Me.Label70.Text = "ค้นหาตามรหัสพนักงาน :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label11.Location = New System.Drawing.Point(78, 57)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(164, 43)
        Me.Label11.TabIndex = 70
        Me.Label11.Text = "ค้าหาตามแผนก :"
        '
        'cboDep
        '
        Me.cboDep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDep.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboDep.FormattingEnabled = True
        Me.cboDep.Location = New System.Drawing.Point(267, 54)
        Me.cboDep.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.cboDep.Name = "cboDep"
        Me.cboDep.Size = New System.Drawing.Size(233, 51)
        Me.cboDep.TabIndex = 3
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.butSearchdep)
        Me.GroupBox2.Controls.Add(Me.dgvSearch)
        Me.GroupBox2.Controls.Add(Me.cboDep)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 214)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(1793, 785)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "เลือกรายชื่อที่ต้องการ"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Angsana New", 25.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(663, 429)
        Me.Label3.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(411, 93)
        Me.Label3.TabIndex = 75
        Me.Label3.Text = "ไม่ใช้ แต่เก็บไว้ก่อน"
        '
        'butSearchdep
        '
        Me.butSearchdep.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSearchdep.Location = New System.Drawing.Point(525, 45)
        Me.butSearchdep.Margin = New System.Windows.Forms.Padding(4)
        Me.butSearchdep.Name = "butSearchdep"
        Me.butSearchdep.Size = New System.Drawing.Size(174, 60)
        Me.butSearchdep.TabIndex = 74
        Me.butSearchdep.Text = "ค้นหา"
        Me.butSearchdep.UseVisualStyleBackColor = True
        '
        'dgvSearch
        '
        Me.dgvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSearch.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6})
        Me.dgvSearch.Location = New System.Drawing.Point(60, 121)
        Me.dgvSearch.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvSearch.Name = "dgvSearch"
        Me.dgvSearch.RowTemplate.Height = 33
        Me.dgvSearch.Size = New System.Drawing.Size(1652, 664)
        Me.dgvSearch.TabIndex = 0
        '
        'Column2
        '
        Me.Column2.HeaderText = "รหัส"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "ชื่อ"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 200
        '
        'Column4
        '
        Me.Column4.HeaderText = "สกุล"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 200
        '
        'Column5
        '
        Me.Column5.HeaderText = "ตำแหน่ง"
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 200
        '
        'Column6
        '
        Me.Column6.HeaderText = "แผนก"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 200
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtLname)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.butSearchname)
        Me.GroupBox3.Controls.Add(Me.txtFname)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(863, 18)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox3.Size = New System.Drawing.Size(942, 156)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "กรอกข้อมูล"
        '
        'txtLname
        '
        Me.txtLname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtLname.Location = New System.Drawing.Point(497, 44)
        Me.txtLname.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtLname.Name = "txtLname"
        Me.txtLname.Size = New System.Drawing.Size(233, 51)
        Me.txtLname.TabIndex = 74
        Me.txtLname.Text = "571432060150"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(417, 46)
        Me.Label2.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 43)
        Me.Label2.TabIndex = 75
        Me.Label2.Text = "สกุล :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'butSearchname
        '
        Me.butSearchname.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSearchname.Location = New System.Drawing.Point(739, 44)
        Me.butSearchname.Margin = New System.Windows.Forms.Padding(4)
        Me.butSearchname.Name = "butSearchname"
        Me.butSearchname.Size = New System.Drawing.Size(174, 60)
        Me.butSearchname.TabIndex = 73
        Me.butSearchname.Text = "ค้นหา"
        Me.butSearchname.UseVisualStyleBackColor = True
        '
        'txtFname
        '
        Me.txtFname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtFname.Location = New System.Drawing.Point(165, 44)
        Me.txtFname.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtFname.Name = "txtFname"
        Me.txtFname.Size = New System.Drawing.Size(233, 51)
        Me.txtFname.TabIndex = 1
        Me.txtFname.Text = "571432060150"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 52)
        Me.Label1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(143, 43)
        Me.Label1.TabIndex = 26
        Me.Label1.Text = "ค้นหาตามชื่อ :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'frmSearchedit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1878, 1081)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmSearchedit"
        Me.Text = "frmSearchedit"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtId As TextBox
    Friend WithEvents Label70 As Label
    Friend WithEvents cboDep As ComboBox
    Friend WithEvents butSearchid As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents dgvSearch As DataGridView
    Friend WithEvents butSearchdep As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtLname As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents butSearchname As Button
    Friend WithEvents txtFname As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Label3 As Label
End Class
