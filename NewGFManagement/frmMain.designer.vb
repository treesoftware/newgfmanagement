﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.MenuStrip2 = New System.Windows.Forms.MenuStrip()
        Me.บนทกประวตToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.รายวนToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.รายวนToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.รายเดอนToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.แกไขประวตToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.รายวนToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.รายเดอนToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.กำหนดเงนเดอนToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กำหนดเงนรายวนToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.การนำเขาขอมลToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.นำเขาขอมลจากFlashdriveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.นำเขาขอมลการเขาหองนำToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกขอมลตางๆToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ขอทำลวงเวลารายบคคลToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บทกขอทำสะสมรายบคคลToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกขอออกกอนรายบคคลToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกขอทำชดเชยรายบคคลToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกขอทำสะสมแบบกลมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกขอออกงานกอนแบบกลมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกขอทำชดเชยแบบกลมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกขอเขางานสายToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกการลาToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกคาเสยหายToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.บนทกเปลยนOTเปนสะสมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.XxxToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กลมงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.แตงตงหวหนากลมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.จดกลมงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ยายสมาชกในกลมงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ตงคาToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.สรางชอแผนกและกลมงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.สรางกลมงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กำหนดตำแหนงToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กำหนดเวลาเขาออกงานกะToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กำหนดวนหยดประจำปToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.กำหนดคาประกนสงคมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.รายงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.รายงานการเขาหองนำToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.การคนหาขอมลตงๆToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.คนไทยToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.คนพมาToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.รายงานคาแรงคนไทยToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.คนหาToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.รหสพนกงานToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.รหสกลมToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip2.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 73)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 5, 0, 5)
        Me.MenuStrip1.Size = New System.Drawing.Size(1362, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'MenuStrip2
        '
        Me.MenuStrip2.Font = New System.Drawing.Font("Angsana New", 16.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip2.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.MenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.บนทกประวตToolStripMenuItem, Me.การนำเขาขอมลToolStripMenuItem, Me.บนทกขอมลตางๆToolStripMenuItem, Me.กลมงานToolStripMenuItem, Me.ตงคาToolStripMenuItem, Me.รายงานToolStripMenuItem, Me.คนหาToolStripMenuItem})
        Me.MenuStrip2.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip2.Name = "MenuStrip2"
        Me.MenuStrip2.Padding = New System.Windows.Forms.Padding(7, 5, 0, 5)
        Me.MenuStrip2.Size = New System.Drawing.Size(1362, 73)
        Me.MenuStrip2.TabIndex = 1
        Me.MenuStrip2.Text = "MenuStrip2"
        '
        'บนทกประวตToolStripMenuItem
        '
        Me.บนทกประวตToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.รายวนToolStripMenuItem, Me.แกไขประวตToolStripMenuItem, Me.กำหนดเงนเดอนToolStripMenuItem, Me.กำหนดเงนรายวนToolStripMenuItem})
        Me.บนทกประวตToolStripMenuItem.Font = New System.Drawing.Font("Angsana New", 16.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.บนทกประวตToolStripMenuItem.Name = "บนทกประวตToolStripMenuItem"
        Me.บนทกประวตToolStripMenuItem.Size = New System.Drawing.Size(119, 63)
        Me.บนทกประวตToolStripMenuItem.Text = "ประวัติ"
        '
        'รายวนToolStripMenuItem
        '
        Me.รายวนToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.รายวนToolStripMenuItem2, Me.รายเดอนToolStripMenuItem2})
        Me.รายวนToolStripMenuItem.Name = "รายวนToolStripMenuItem"
        Me.รายวนToolStripMenuItem.Size = New System.Drawing.Size(340, 64)
        Me.รายวนToolStripMenuItem.Text = "บันทึกประวัติ"
        '
        'รายวนToolStripMenuItem2
        '
        Me.รายวนToolStripMenuItem2.Name = "รายวนToolStripMenuItem2"
        Me.รายวนToolStripMenuItem2.Size = New System.Drawing.Size(239, 64)
        Me.รายวนToolStripMenuItem2.Text = "รายวัน"
        '
        'รายเดอนToolStripMenuItem2
        '
        Me.รายเดอนToolStripMenuItem2.Name = "รายเดอนToolStripMenuItem2"
        Me.รายเดอนToolStripMenuItem2.Size = New System.Drawing.Size(239, 64)
        Me.รายเดอนToolStripMenuItem2.Text = "รายเดือน"
        '
        'แกไขประวตToolStripMenuItem
        '
        Me.แกไขประวตToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.รายวนToolStripMenuItem1, Me.รายเดอนToolStripMenuItem1})
        Me.แกไขประวตToolStripMenuItem.Name = "แกไขประวตToolStripMenuItem"
        Me.แกไขประวตToolStripMenuItem.Size = New System.Drawing.Size(340, 64)
        Me.แกไขประวตToolStripMenuItem.Text = "แก้ไขประวัติ"
        '
        'รายวนToolStripMenuItem1
        '
        Me.รายวนToolStripMenuItem1.Name = "รายวนToolStripMenuItem1"
        Me.รายวนToolStripMenuItem1.Size = New System.Drawing.Size(239, 64)
        Me.รายวนToolStripMenuItem1.Text = "รายวัน"
        '
        'รายเดอนToolStripMenuItem1
        '
        Me.รายเดอนToolStripMenuItem1.Name = "รายเดอนToolStripMenuItem1"
        Me.รายเดอนToolStripMenuItem1.Size = New System.Drawing.Size(239, 64)
        Me.รายเดอนToolStripMenuItem1.Text = "รายเดือน"
        '
        'กำหนดเงนเดอนToolStripMenuItem
        '
        Me.กำหนดเงนเดอนToolStripMenuItem.Name = "กำหนดเงนเดอนToolStripMenuItem"
        Me.กำหนดเงนเดอนToolStripMenuItem.Size = New System.Drawing.Size(340, 64)
        Me.กำหนดเงนเดอนToolStripMenuItem.Text = "กำหนดเงินเดือน"
        '
        'กำหนดเงนรายวนToolStripMenuItem
        '
        Me.กำหนดเงนรายวนToolStripMenuItem.Name = "กำหนดเงนรายวนToolStripMenuItem"
        Me.กำหนดเงนรายวนToolStripMenuItem.Size = New System.Drawing.Size(340, 64)
        Me.กำหนดเงนรายวนToolStripMenuItem.Text = "กำหนดเงินรายวัน"
        '
        'การนำเขาขอมลToolStripMenuItem
        '
        Me.การนำเขาขอมลToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.นำเขาขอมลจากFlashdriveToolStripMenuItem, Me.นำเขาขอมลการเขาหองนำToolStripMenuItem})
        Me.การนำเขาขอมลToolStripMenuItem.Name = "การนำเขาขอมลToolStripMenuItem"
        Me.การนำเขาขอมลToolStripMenuItem.Size = New System.Drawing.Size(218, 63)
        Me.การนำเขาขอมลToolStripMenuItem.Text = "การนำเข้าข้อมูล"
        '
        'นำเขาขอมลจากFlashdriveToolStripMenuItem
        '
        Me.นำเขาขอมลจากFlashdriveToolStripMenuItem.Name = "นำเขาขอมลจากFlashdriveToolStripMenuItem"
        Me.นำเขาขอมลจากFlashdriveToolStripMenuItem.Size = New System.Drawing.Size(435, 64)
        Me.นำเขาขอมลจากFlashdriveToolStripMenuItem.Text = "นำเข้าข้อมูลการเข้างาน"
        '
        'นำเขาขอมลการเขาหองนำToolStripMenuItem
        '
        Me.นำเขาขอมลการเขาหองนำToolStripMenuItem.Name = "นำเขาขอมลการเขาหองนำToolStripMenuItem"
        Me.นำเขาขอมลการเขาหองนำToolStripMenuItem.Size = New System.Drawing.Size(435, 64)
        Me.นำเขาขอมลการเขาหองนำToolStripMenuItem.Text = "นำเข้าข้อมูลการเข้าห้องน้ำ"
        '
        'บนทกขอมลตางๆToolStripMenuItem
        '
        Me.บนทกขอมลตางๆToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ขอทำลวงเวลารายบคคลToolStripMenuItem, Me.บทกขอทำสะสมรายบคคลToolStripMenuItem, Me.บนทกขอออกกอนรายบคคลToolStripMenuItem, Me.บนทกขอทำชดเชยรายบคคลToolStripMenuItem, Me.บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem, Me.บนทกขอทำสะสมแบบกลมToolStripMenuItem, Me.บนทกขอออกงานกอนแบบกลมToolStripMenuItem, Me.บนทกขอทำชดเชยแบบกลมToolStripMenuItem, Me.บนทกขอเขางานสายToolStripMenuItem, Me.บนทกการลาToolStripMenuItem, Me.บนทกคาเสยหายToolStripMenuItem, Me.บนทกเปลยนOTเปนสะสมToolStripMenuItem, Me.XxxToolStripMenuItem})
        Me.บนทกขอมลตางๆToolStripMenuItem.Name = "บนทกขอมลตางๆToolStripMenuItem"
        Me.บนทกขอมลตางๆToolStripMenuItem.Size = New System.Drawing.Size(239, 63)
        Me.บนทกขอมลตางๆToolStripMenuItem.Text = "บันทึกข้อมูลต่างๆ"
        '
        'ขอทำลวงเวลารายบคคลToolStripMenuItem
        '
        Me.ขอทำลวงเวลารายบคคลToolStripMenuItem.Name = "ขอทำลวงเวลารายบคคลToolStripMenuItem"
        Me.ขอทำลวงเวลารายบคคลToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.ขอทำลวงเวลารายบคคลToolStripMenuItem.Text = "บันทึกขอทำล่วงเวลารายบุคคล"
        '
        'บทกขอทำสะสมรายบคคลToolStripMenuItem
        '
        Me.บทกขอทำสะสมรายบคคลToolStripMenuItem.Name = "บทกขอทำสะสมรายบคคลToolStripMenuItem"
        Me.บทกขอทำสะสมรายบคคลToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.บทกขอทำสะสมรายบคคลToolStripMenuItem.Text = "บันทึกขอทำสะสมรายบุคคล"
        '
        'บนทกขอออกกอนรายบคคลToolStripMenuItem
        '
        Me.บนทกขอออกกอนรายบคคลToolStripMenuItem.Name = "บนทกขอออกกอนรายบคคลToolStripMenuItem"
        Me.บนทกขอออกกอนรายบคคลToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.บนทกขอออกกอนรายบคคลToolStripMenuItem.Text = "บันทึกขอออกก่อนรายบุคคล"
        '
        'บนทกขอทำชดเชยรายบคคลToolStripMenuItem
        '
        Me.บนทกขอทำชดเชยรายบคคลToolStripMenuItem.Name = "บนทกขอทำชดเชยรายบคคลToolStripMenuItem"
        Me.บนทกขอทำชดเชยรายบคคลToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.บนทกขอทำชดเชยรายบคคลToolStripMenuItem.Text = "บันทึกขอทำชดเชยรายบุคคล"
        '
        'บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem
        '
        Me.บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem.Name = "บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem"
        Me.บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem.Text = "บันทึกขอทำล่วงเวลาแบบกลุ่ม"
        '
        'บนทกขอทำสะสมแบบกลมToolStripMenuItem
        '
        Me.บนทกขอทำสะสมแบบกลมToolStripMenuItem.Name = "บนทกขอทำสะสมแบบกลมToolStripMenuItem"
        Me.บนทกขอทำสะสมแบบกลมToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.บนทกขอทำสะสมแบบกลมToolStripMenuItem.Text = "บันทึกขอทำสะสมแบบกลุ่ม"
        '
        'บนทกขอออกงานกอนแบบกลมToolStripMenuItem
        '
        Me.บนทกขอออกงานกอนแบบกลมToolStripMenuItem.Name = "บนทกขอออกงานกอนแบบกลมToolStripMenuItem"
        Me.บนทกขอออกงานกอนแบบกลมToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.บนทกขอออกงานกอนแบบกลมToolStripMenuItem.Text = "บันทึกขอออกงานก่อนแบบกลุ่ม"
        '
        'บนทกขอทำชดเชยแบบกลมToolStripMenuItem
        '
        Me.บนทกขอทำชดเชยแบบกลมToolStripMenuItem.Name = "บนทกขอทำชดเชยแบบกลมToolStripMenuItem"
        Me.บนทกขอทำชดเชยแบบกลมToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.บนทกขอทำชดเชยแบบกลมToolStripMenuItem.Text = "บันทึกขอทำชดเชยแบบกลุ่ม"
        '
        'บนทกขอเขางานสายToolStripMenuItem
        '
        Me.บนทกขอเขางานสายToolStripMenuItem.Name = "บนทกขอเขางานสายToolStripMenuItem"
        Me.บนทกขอเขางานสายToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.บนทกขอเขางานสายToolStripMenuItem.Text = "บันทึกขอเข้างานสาย"
        '
        'บนทกการลาToolStripMenuItem
        '
        Me.บนทกการลาToolStripMenuItem.Name = "บนทกการลาToolStripMenuItem"
        Me.บนทกการลาToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.บนทกการลาToolStripMenuItem.Text = "บันทึกการลา"
        '
        'บนทกคาเสยหายToolStripMenuItem
        '
        Me.บนทกคาเสยหายToolStripMenuItem.Name = "บนทกคาเสยหายToolStripMenuItem"
        Me.บนทกคาเสยหายToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.บนทกคาเสยหายToolStripMenuItem.Text = "บันทึกค่าเสียหาย"
        '
        'บนทกเปลยนOTเปนสะสมToolStripMenuItem
        '
        Me.บนทกเปลยนOTเปนสะสมToolStripMenuItem.Name = "บนทกเปลยนOTเปนสะสมToolStripMenuItem"
        Me.บนทกเปลยนOTเปนสะสมToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.บนทกเปลยนOTเปนสะสมToolStripMenuItem.Text = "บันทึกเปลี่ยนOTเป็นสะสม"
        '
        'XxxToolStripMenuItem
        '
        Me.XxxToolStripMenuItem.Name = "XxxToolStripMenuItem"
        Me.XxxToolStripMenuItem.Size = New System.Drawing.Size(493, 64)
        Me.XxxToolStripMenuItem.Text = "xxx"
        '
        'กลมงานToolStripMenuItem
        '
        Me.กลมงานToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.แตงตงหวหนากลมToolStripMenuItem, Me.จดกลมงานToolStripMenuItem, Me.ยายสมาชกในกลมงานToolStripMenuItem})
        Me.กลมงานToolStripMenuItem.Name = "กลมงานToolStripMenuItem"
        Me.กลมงานToolStripMenuItem.Size = New System.Drawing.Size(134, 63)
        Me.กลมงานToolStripMenuItem.Text = "กลุ่มงาน"
        '
        'แตงตงหวหนากลมToolStripMenuItem
        '
        Me.แตงตงหวหนากลมToolStripMenuItem.Name = "แตงตงหวหนากลมToolStripMenuItem"
        Me.แตงตงหวหนากลมToolStripMenuItem.Size = New System.Drawing.Size(388, 64)
        Me.แตงตงหวหนากลมToolStripMenuItem.Text = "กำหนดหัวหน้ากลุ่ม"
        '
        'จดกลมงานToolStripMenuItem
        '
        Me.จดกลมงานToolStripMenuItem.Name = "จดกลมงานToolStripMenuItem"
        Me.จดกลมงานToolStripMenuItem.Size = New System.Drawing.Size(388, 64)
        Me.จดกลมงานToolStripMenuItem.Text = "จัดกลุ่มงาน"
        '
        'ยายสมาชกในกลมงานToolStripMenuItem
        '
        Me.ยายสมาชกในกลมงานToolStripMenuItem.Name = "ยายสมาชกในกลมงานToolStripMenuItem"
        Me.ยายสมาชกในกลมงานToolStripMenuItem.Size = New System.Drawing.Size(388, 64)
        Me.ยายสมาชกในกลมงานToolStripMenuItem.Text = "ย้ายสมาชิกในกลุ่มงาน"
        '
        'ตงคาToolStripMenuItem
        '
        Me.ตงคาToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.สรางชอแผนกและกลมงานToolStripMenuItem, Me.สรางกลมงานToolStripMenuItem, Me.กำหนดตำแหนงToolStripMenuItem, Me.กำหนดเวลาเขาออกงานกะToolStripMenuItem, Me.กำหนดวนหยดประจำปToolStripMenuItem, Me.กำหนดคาประกนสงคมToolStripMenuItem})
        Me.ตงคาToolStripMenuItem.Name = "ตงคาToolStripMenuItem"
        Me.ตงคาToolStripMenuItem.Size = New System.Drawing.Size(98, 63)
        Me.ตงคาToolStripMenuItem.Text = "ตั้งค่า"
        '
        'สรางชอแผนกและกลมงานToolStripMenuItem
        '
        Me.สรางชอแผนกและกลมงานToolStripMenuItem.Name = "สรางชอแผนกและกลมงานToolStripMenuItem"
        Me.สรางชอแผนกและกลมงานToolStripMenuItem.Size = New System.Drawing.Size(451, 64)
        Me.สรางชอแผนกและกลมงานToolStripMenuItem.Text = "สร้างแผนก"
        '
        'สรางกลมงานToolStripMenuItem
        '
        Me.สรางกลมงานToolStripMenuItem.Name = "สรางกลมงานToolStripMenuItem"
        Me.สรางกลมงานToolStripMenuItem.Size = New System.Drawing.Size(451, 64)
        Me.สรางกลมงานToolStripMenuItem.Text = "สร้างกลุ่มงาน"
        '
        'กำหนดตำแหนงToolStripMenuItem
        '
        Me.กำหนดตำแหนงToolStripMenuItem.Name = "กำหนดตำแหนงToolStripMenuItem"
        Me.กำหนดตำแหนงToolStripMenuItem.Size = New System.Drawing.Size(451, 64)
        Me.กำหนดตำแหนงToolStripMenuItem.Text = "กำหนดตำแหน่ง"
        '
        'กำหนดเวลาเขาออกงานกะToolStripMenuItem
        '
        Me.กำหนดเวลาเขาออกงานกะToolStripMenuItem.Name = "กำหนดเวลาเขาออกงานกะToolStripMenuItem"
        Me.กำหนดเวลาเขาออกงานกะToolStripMenuItem.Size = New System.Drawing.Size(451, 64)
        Me.กำหนดเวลาเขาออกงานกะToolStripMenuItem.Text = "กำหนดเวลาเข้าออกงาน(กะ)"
        '
        'กำหนดวนหยดประจำปToolStripMenuItem
        '
        Me.กำหนดวนหยดประจำปToolStripMenuItem.Name = "กำหนดวนหยดประจำปToolStripMenuItem"
        Me.กำหนดวนหยดประจำปToolStripMenuItem.Size = New System.Drawing.Size(451, 64)
        Me.กำหนดวนหยดประจำปToolStripMenuItem.Text = "กำหนดวันหยุดประจำปี"
        '
        'กำหนดคาประกนสงคมToolStripMenuItem
        '
        Me.กำหนดคาประกนสงคมToolStripMenuItem.Name = "กำหนดคาประกนสงคมToolStripMenuItem"
        Me.กำหนดคาประกนสงคมToolStripMenuItem.Size = New System.Drawing.Size(451, 64)
        Me.กำหนดคาประกนสงคมToolStripMenuItem.Text = "กำหนดค่าประกันสังคม"
        '
        'รายงานToolStripMenuItem
        '
        Me.รายงานToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.รายงานการเขาหองนำToolStripMenuItem, Me.การคนหาขอมลตงๆToolStripMenuItem, Me.รายงานคาแรงคนไทยToolStripMenuItem})
        Me.รายงานToolStripMenuItem.Name = "รายงานToolStripMenuItem"
        Me.รายงานToolStripMenuItem.Size = New System.Drawing.Size(126, 63)
        Me.รายงานToolStripMenuItem.Text = "รายงาน"
        '
        'รายงานการเขาหองนำToolStripMenuItem
        '
        Me.รายงานการเขาหองนำToolStripMenuItem.Name = "รายงานการเขาหองนำToolStripMenuItem"
        Me.รายงานการเขาหองนำToolStripMenuItem.Size = New System.Drawing.Size(401, 64)
        Me.รายงานการเขาหองนำToolStripMenuItem.Text = "รายงานการเข้าห้องน้ำ"
        '
        'การคนหาขอมลตงๆToolStripMenuItem
        '
        Me.การคนหาขอมลตงๆToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.คนไทยToolStripMenuItem, Me.คนพมาToolStripMenuItem})
        Me.การคนหาขอมลตงๆToolStripMenuItem.Name = "การคนหาขอมลตงๆToolStripMenuItem"
        Me.การคนหาขอมลตงๆToolStripMenuItem.Size = New System.Drawing.Size(401, 64)
        Me.การคนหาขอมลตงๆToolStripMenuItem.Text = "รายงานค่าแรงรายวัน"
        '
        'คนไทยToolStripMenuItem
        '
        Me.คนไทยToolStripMenuItem.Name = "คนไทยToolStripMenuItem"
        Me.คนไทยToolStripMenuItem.Size = New System.Drawing.Size(269, 64)
        Me.คนไทยToolStripMenuItem.Text = "คนไทย"
        '
        'คนพมาToolStripMenuItem
        '
        Me.คนพมาToolStripMenuItem.Name = "คนพมาToolStripMenuItem"
        Me.คนพมาToolStripMenuItem.Size = New System.Drawing.Size(269, 64)
        Me.คนพมาToolStripMenuItem.Text = "คนพม่า"
        '
        'รายงานคาแรงคนไทยToolStripMenuItem
        '
        Me.รายงานคาแรงคนไทยToolStripMenuItem.Name = "รายงานคาแรงคนไทยToolStripMenuItem"
        Me.รายงานคาแรงคนไทยToolStripMenuItem.Size = New System.Drawing.Size(401, 64)
        Me.รายงานคาแรงคนไทยToolStripMenuItem.Text = "รายงานค่าแรงรายเดือน"
        '
        'คนหาToolStripMenuItem
        '
        Me.คนหาToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.รหสพนกงานToolStripMenuItem, Me.รหสกลมToolStripMenuItem})
        Me.คนหาToolStripMenuItem.Name = "คนหาToolStripMenuItem"
        Me.คนหาToolStripMenuItem.Size = New System.Drawing.Size(106, 63)
        Me.คนหาToolStripMenuItem.Text = "ค้นหา"
        '
        'รหสพนกงานToolStripMenuItem
        '
        Me.รหสพนกงานToolStripMenuItem.Name = "รหสพนกงานToolStripMenuItem"
        Me.รหสพนกงานToolStripMenuItem.Size = New System.Drawing.Size(291, 64)
        Me.รหสพนกงานToolStripMenuItem.Text = "รหัสพนักงาน"
        '
        'รหสกลมToolStripMenuItem
        '
        Me.รหสกลมToolStripMenuItem.Name = "รหสกลมToolStripMenuItem"
        Me.รหสกลมToolStripMenuItem.Size = New System.Drawing.Size(291, 64)
        Me.รหสกลมToolStripMenuItem.Text = "รหัสกลุ่ม"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(17.0!, 59.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1362, 741)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.MenuStrip2)
        Me.Font = New System.Drawing.Font("Angsana New", 16.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmMain"
        Me.MenuStrip2.ResumeLayout(False)
        Me.MenuStrip2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents MenuStrip2 As MenuStrip
    Friend WithEvents บนทกประวตToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents รายวนToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ตงคาToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents สรางชอแผนกและกลมงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กำหนดตำแหนงToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กำหนดเวลาเขาออกงานกะToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กำหนดวนหยดประจำปToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กำหนดคาประกนสงคมToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents การนำเขาขอมลToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents นำเขาขอมลจากFlashdriveToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกขอมลตางๆToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ขอทำลวงเวลารายบคคลToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกการทำงานลวงเวลาแบบกลมToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกขอออกงานกอนแบบกลมToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกคาเสยหายToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents รายงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents รายงานการเขาหองนำToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents การคนหาขอมลตงๆToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกการลาToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents สรางกลมงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกเปลยนOTเปนสะสมToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กำหนดเงนเดอนToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กำหนดเงนรายวนToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกขอเขางานสายToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents กลมงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents จดกลมงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ยายสมาชกในกลมงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents แตงตงหวหนากลมToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกขอทำชดเชยรายบคคลToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บทกขอทำสะสมรายบคคลToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกขอทำสะสมแบบกลมToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกขอทำชดเชยแบบกลมToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents บนทกขอออกกอนรายบคคลToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents XxxToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents นำเขาขอมลการเขาหองนำToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents แกไขประวตToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents รายวนToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents รายเดอนToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents รายวนToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents รายเดอนToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents คนไทยToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents คนพมาToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents รายงานคาแรงคนไทยToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents คนหาToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents รหสพนกงานToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents รหสกลมToolStripMenuItem As ToolStripMenuItem
End Class
