﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpday
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtAccountnum = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtSkill = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboDep = New System.Windows.Forms.ComboBox()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.txtWage = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboPosition = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dtpContain = New System.Windows.Forms.DateTimePicker()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpStart = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtpRegis = New System.Windows.Forms.DateTimePicker()
        Me.chkMedicalcer = New System.Windows.Forms.CheckBox()
        Me.chkEducation = New System.Windows.Forms.CheckBox()
        Me.chkIdcard = New System.Windows.Forms.CheckBox()
        Me.chkRegisHome = New System.Windows.Forms.CheckBox()
        Me.chkPicture = New System.Windows.Forms.CheckBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtAnothersnew = New System.Windows.Forms.TextBox()
        Me.chkAnothersnew = New System.Windows.Forms.CheckBox()
        Me.chkEmpOffice = New System.Windows.Forms.CheckBox()
        Me.chkAnnouncement = New System.Windows.Forms.CheckBox()
        Me.chkSignsrecruit = New System.Windows.Forms.CheckBox()
        Me.chkInternet = New System.Windows.Forms.CheckBox()
        Me.chkAdvicefriend = New System.Windows.Forms.CheckBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.txtDistrict = New System.Windows.Forms.TextBox()
        Me.txtCanton = New System.Windows.Forms.TextBox()
        Me.txtZipcode = New System.Windows.Forms.TextBox()
        Me.lblPostcode = New System.Windows.Forms.Label()
        Me.txtTelephone = New System.Windows.Forms.TextBox()
        Me.lblTelephone = New System.Windows.Forms.Label()
        Me.txtMobile = New System.Windows.Forms.TextBox()
        Me.lblSmartphone = New System.Windows.Forms.Label()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.lblProvince = New System.Windows.Forms.Label()
        Me.lblDistrict = New System.Windows.Forms.Label()
        Me.lblSubdistrict = New System.Windows.Forms.Label()
        Me.txtMoo = New System.Windows.Forms.TextBox()
        Me.lblNum = New System.Windows.Forms.Label()
        Me.lblMu = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.txtProvincemarry = New System.Windows.Forms.TextBox()
        Me.txtDdistrictmarry = New System.Windows.Forms.TextBox()
        Me.txtCantonmarry = New System.Windows.Forms.TextBox()
        Me.lblSpouseprovince = New System.Windows.Forms.Label()
        Me.lblSpousepostcode = New System.Windows.Forms.Label()
        Me.txtAddressmarry = New System.Windows.Forms.TextBox()
        Me.txtZipcodemarry = New System.Windows.Forms.TextBox()
        Me.txtTephonemarry = New System.Windows.Forms.TextBox()
        Me.lblSpousetelephone = New System.Windows.Forms.Label()
        Me.txtMobilemarry = New System.Windows.Forms.TextBox()
        Me.lblSpousesmartphone = New System.Windows.Forms.Label()
        Me.lblSpousedistrict = New System.Windows.Forms.Label()
        Me.lblSpousesubdistrict = New System.Windows.Forms.Label()
        Me.txtMoomarry = New System.Windows.Forms.TextBox()
        Me.lblSpouseNum = New System.Windows.Forms.Label()
        Me.lblSpousemu = New System.Windows.Forms.Label()
        Me.txtNicknamemarry = New System.Windows.Forms.TextBox()
        Me.lblSpousenickname = New System.Windows.Forms.Label()
        Me.txtLnamemarry = New System.Windows.Forms.TextBox()
        Me.txtFnamemarry = New System.Windows.Forms.TextBox()
        Me.cboTitlemarry = New System.Windows.Forms.ComboBox()
        Me.lblSpouselastname = New System.Windows.Forms.Label()
        Me.lblSpousetitle = New System.Windows.Forms.Label()
        Me.lblSpousename = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.RadioButton10 = New System.Windows.Forms.RadioButton()
        Me.rdbS = New System.Windows.Forms.RadioButton()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.rdbWomen = New System.Windows.Forms.RadioButton()
        Me.rdbMan = New System.Windows.Forms.RadioButton()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.txtHiswork = New System.Windows.Forms.TextBox()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.rdbSicknessno = New System.Windows.Forms.RadioButton()
        Me.rdbSickness = New System.Windows.Forms.RadioButton()
        Me.txtSickness = New System.Windows.Forms.TextBox()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.rdbCigaretteno = New System.Windows.Forms.RadioButton()
        Me.rdbCigarette = New System.Windows.Forms.RadioButton()
        Me.rdbEat = New System.Windows.Forms.GroupBox()
        Me.rdbEatno = New System.Windows.Forms.RadioButton()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.rdbPrisonno = New System.Windows.Forms.RadioButton()
        Me.rdbPrison = New System.Windows.Forms.RadioButton()
        Me.butSave = New System.Windows.Forms.Button()
        Me.butDel = New System.Windows.Forms.Button()
        Me.butClear = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupBox14 = New System.Windows.Forms.GroupBox()
        Me.rdbContain = New System.Windows.Forms.RadioButton()
        Me.rdbTrial = New System.Windows.Forms.RadioButton()
        Me.GroupBox30 = New System.Windows.Forms.GroupBox()
        Me.txtByprovince = New System.Windows.Forms.TextBox()
        Me.txtBycanton = New System.Windows.Forms.TextBox()
        Me.dtpBirthday = New System.Windows.Forms.DateTimePicker()
        Me.dtpEndidcard = New System.Windows.Forms.DateTimePicker()
        Me.dtpStartidcard = New System.Windows.Forms.DateTimePicker()
        Me.cboEducation = New System.Windows.Forms.ComboBox()
        Me.lblEducation = New System.Windows.Forms.Label()
        Me.lblM = New System.Windows.Forms.Label()
        Me.lblIssuedatdistrict = New System.Windows.Forms.Label()
        Me.lblKg = New System.Windows.Forms.Label()
        Me.lblIssuedatprovince = New System.Windows.Forms.Label()
        Me.lblYear = New System.Windows.Forms.Label()
        Me.txtNickname = New System.Windows.Forms.TextBox()
        Me.lblNickname = New System.Windows.Forms.Label()
        Me.cboBlood = New System.Windows.Forms.ComboBox()
        Me.lblGblood = New System.Windows.Forms.Label()
        Me.lblEndcard = New System.Windows.Forms.Label()
        Me.txtHeight = New System.Windows.Forms.TextBox()
        Me.lblStartcard = New System.Windows.Forms.Label()
        Me.lblHeight = New System.Windows.Forms.Label()
        Me.txtWeight = New System.Windows.Forms.TextBox()
        Me.txtIdcard = New System.Windows.Forms.TextBox()
        Me.txtAge = New System.Windows.Forms.TextBox()
        Me.lblWeight = New System.Windows.Forms.Label()
        Me.lblIdcard = New System.Windows.Forms.Label()
        Me.lblAge = New System.Windows.Forms.Label()
        Me.cboReligion = New System.Windows.Forms.ComboBox()
        Me.lblReligion = New System.Windows.Forms.Label()
        Me.cboNationality = New System.Windows.Forms.ComboBox()
        Me.lblNationality = New System.Windows.Forms.Label()
        Me.lblBirthday = New System.Windows.Forms.Label()
        Me.txtLname = New System.Windows.Forms.TextBox()
        Me.txtFname = New System.Windows.Forms.TextBox()
        Me.cboTitle = New System.Windows.Forms.ComboBox()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblEmployeeData = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.rdbEat.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox14.SuspendLayout()
        Me.GroupBox30.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtAccountnum)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtSkill)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.cboDep)
        Me.GroupBox1.Controls.Add(Me.Label71)
        Me.GroupBox1.Controls.Add(Me.txtId)
        Me.GroupBox1.Controls.Add(Me.Label70)
        Me.GroupBox1.Controls.Add(Me.txtWage)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.cboPosition)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(18, 6)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox1.Size = New System.Drawing.Size(1978, 129)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "กรอกข้อมูล"
        '
        'txtAccountnum
        '
        Me.txtAccountnum.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccountnum.Location = New System.Drawing.Point(1532, 46)
        Me.txtAccountnum.Margin = New System.Windows.Forms.Padding(6)
        Me.txtAccountnum.Name = "txtAccountnum"
        Me.txtAccountnum.Size = New System.Drawing.Size(228, 51)
        Me.txtAccountnum.TabIndex = 6
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(1412, 52)
        Me.Label9.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(119, 43)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "เลขที่บัญชี :"
        '
        'txtSkill
        '
        Me.txtSkill.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSkill.Location = New System.Drawing.Point(1296, 46)
        Me.txtSkill.Margin = New System.Windows.Forms.Padding(6)
        Me.txtSkill.Name = "txtSkill"
        Me.txtSkill.Size = New System.Drawing.Size(102, 51)
        Me.txtSkill.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(1192, 52)
        Me.Label6.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(104, 43)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "ค่าทักษะ :"
        '
        'cboDep
        '
        Me.cboDep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDep.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDep.FormattingEnabled = True
        Me.cboDep.Location = New System.Drawing.Point(426, 46)
        Me.cboDep.Margin = New System.Windows.Forms.Padding(6)
        Me.cboDep.Name = "cboDep"
        Me.cboDep.Size = New System.Drawing.Size(178, 51)
        Me.cboDep.TabIndex = 2
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label71.Location = New System.Drawing.Point(340, 52)
        Me.Label71.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(83, 43)
        Me.Label71.TabIndex = 28
        Me.Label71.Text = "แผนก :"
        '
        'txtId
        '
        Me.txtId.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtId.Location = New System.Drawing.Point(160, 46)
        Me.txtId.Margin = New System.Windows.Forms.Padding(6)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(168, 51)
        Me.txtId.TabIndex = 1
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label70.Location = New System.Drawing.Point(12, 52)
        Me.Label70.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(147, 43)
        Me.Label70.TabIndex = 26
        Me.Label70.Text = "รหัสพนักงาน :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'txtWage
        '
        Me.txtWage.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWage.Location = New System.Drawing.Point(1064, 46)
        Me.txtWage.Margin = New System.Windows.Forms.Padding(6)
        Me.txtWage.Name = "txtWage"
        Me.txtWage.Size = New System.Drawing.Size(108, 51)
        Me.txtWage.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(916, 52)
        Me.Label5.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(139, 43)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "ค่าจ้างรายวัน :"
        '
        'cboPosition
        '
        Me.cboPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPosition.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPosition.FormattingEnabled = True
        Me.cboPosition.Location = New System.Drawing.Point(724, 46)
        Me.cboPosition.Margin = New System.Windows.Forms.Padding(6)
        Me.cboPosition.Name = "cboPosition"
        Me.cboPosition.Size = New System.Drawing.Size(172, 51)
        Me.cboPosition.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(618, 52)
        Me.Label4.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 43)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "ตำแหน่ง :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dtpContain)
        Me.GroupBox2.Controls.Add(Me.Label77)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.dtpStart)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.dtpRegis)
        Me.GroupBox2.Controls.Add(Me.chkMedicalcer)
        Me.GroupBox2.Controls.Add(Me.chkEducation)
        Me.GroupBox2.Controls.Add(Me.chkIdcard)
        Me.GroupBox2.Controls.Add(Me.chkRegisHome)
        Me.GroupBox2.Controls.Add(Me.chkPicture)
        Me.GroupBox2.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(16, 115)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox2.Size = New System.Drawing.Size(1980, 139)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "หลักฐานประกอบการสมัครงาน"
        '
        'dtpContain
        '
        Me.dtpContain.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpContain.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpContain.Location = New System.Drawing.Point(1732, 36)
        Me.dtpContain.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.dtpContain.Name = "dtpContain"
        Me.dtpContain.Size = New System.Drawing.Size(222, 51)
        Me.dtpContain.TabIndex = 41
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label77.Location = New System.Drawing.Point(1452, 40)
        Me.Label77.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(261, 43)
        Me.Label77.TabIndex = 42
        Me.Label77.Text = "วันเดือนปีที่บรรจุเข้าทำงาน :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(1060, 92)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(119, 43)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "วันเริ่มงาน :"
        '
        'dtpStart
        '
        Me.dtpStart.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStart.Location = New System.Drawing.Point(1190, 85)
        Me.dtpStart.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.dtpStart.Name = "dtpStart"
        Me.dtpStart.Size = New System.Drawing.Size(222, 51)
        Me.dtpStart.TabIndex = 13
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.Location = New System.Drawing.Point(1008, 40)
        Me.Label7.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(175, 43)
        Me.Label7.TabIndex = 38
        Me.Label7.Text = "วันเขียนใบสมัคร :"
        '
        'dtpRegis
        '
        Me.dtpRegis.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpRegis.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpRegis.Location = New System.Drawing.Point(1190, 35)
        Me.dtpRegis.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.dtpRegis.Name = "dtpRegis"
        Me.dtpRegis.Size = New System.Drawing.Size(222, 51)
        Me.dtpRegis.TabIndex = 10
        '
        'chkMedicalcer
        '
        Me.chkMedicalcer.AutoSize = True
        Me.chkMedicalcer.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkMedicalcer.Location = New System.Drawing.Point(268, 88)
        Me.chkMedicalcer.Margin = New System.Windows.Forms.Padding(6)
        Me.chkMedicalcer.Name = "chkMedicalcer"
        Me.chkMedicalcer.Size = New System.Drawing.Size(186, 47)
        Me.chkMedicalcer.TabIndex = 12
        Me.chkMedicalcer.Text = "ใบรับรองแพทย์"
        Me.chkMedicalcer.UseVisualStyleBackColor = True
        '
        'chkEducation
        '
        Me.chkEducation.AutoSize = True
        Me.chkEducation.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkEducation.Location = New System.Drawing.Point(18, 90)
        Me.chkEducation.Margin = New System.Windows.Forms.Padding(6)
        Me.chkEducation.Name = "chkEducation"
        Me.chkEducation.Size = New System.Drawing.Size(212, 47)
        Me.chkEducation.TabIndex = 11
        Me.chkEducation.Text = "สำเนาวุฒิการศึกษา"
        Me.chkEducation.UseVisualStyleBackColor = True
        '
        'chkIdcard
        '
        Me.chkIdcard.AutoSize = True
        Me.chkIdcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkIdcard.Location = New System.Drawing.Point(268, 40)
        Me.chkIdcard.Margin = New System.Windows.Forms.Padding(6)
        Me.chkIdcard.Name = "chkIdcard"
        Me.chkIdcard.Size = New System.Drawing.Size(307, 47)
        Me.chkIdcard.TabIndex = 8
        Me.chkIdcard.Text = "สำเนาบัตรประจำตัวประชาชน"
        Me.chkIdcard.UseVisualStyleBackColor = True
        '
        'chkRegisHome
        '
        Me.chkRegisHome.AutoSize = True
        Me.chkRegisHome.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkRegisHome.Location = New System.Drawing.Point(18, 40)
        Me.chkRegisHome.Margin = New System.Windows.Forms.Padding(6)
        Me.chkRegisHome.Name = "chkRegisHome"
        Me.chkRegisHome.Size = New System.Drawing.Size(211, 47)
        Me.chkRegisHome.TabIndex = 7
        Me.chkRegisHome.Text = "สำเนาทะเบียนบ้าน"
        Me.chkRegisHome.UseVisualStyleBackColor = True
        '
        'chkPicture
        '
        Me.chkPicture.AutoSize = True
        Me.chkPicture.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkPicture.Location = New System.Drawing.Point(594, 40)
        Me.chkPicture.Margin = New System.Windows.Forms.Padding(6)
        Me.chkPicture.Name = "chkPicture"
        Me.chkPicture.Size = New System.Drawing.Size(371, 47)
        Me.chkPicture.TabIndex = 9
        Me.chkPicture.Text = "สำเนารูปถ่ายหน้าตรง 1"" จำนวน 1 รูป"
        Me.chkPicture.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtAnothersnew)
        Me.GroupBox3.Controls.Add(Me.chkAnothersnew)
        Me.GroupBox3.Controls.Add(Me.chkEmpOffice)
        Me.GroupBox3.Controls.Add(Me.chkAnnouncement)
        Me.GroupBox3.Controls.Add(Me.chkSignsrecruit)
        Me.GroupBox3.Controls.Add(Me.chkInternet)
        Me.GroupBox3.Controls.Add(Me.chkAdvicefriend)
        Me.GroupBox3.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(18, 265)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox3.Size = New System.Drawing.Size(988, 142)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "ท่านทราบข่าวการรับสมัครงานจากที่ไหน"
        '
        'txtAnothersnew
        '
        Me.txtAnothersnew.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtAnothersnew.Location = New System.Drawing.Point(774, 83)
        Me.txtAnothersnew.Margin = New System.Windows.Forms.Padding(6)
        Me.txtAnothersnew.Name = "txtAnothersnew"
        Me.txtAnothersnew.Size = New System.Drawing.Size(204, 51)
        Me.txtAnothersnew.TabIndex = 20
        '
        'chkAnothersnew
        '
        Me.chkAnothersnew.AutoSize = True
        Me.chkAnothersnew.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkAnothersnew.Location = New System.Drawing.Point(588, 87)
        Me.chkAnothersnew.Margin = New System.Windows.Forms.Padding(6)
        Me.chkAnothersnew.Name = "chkAnothersnew"
        Me.chkAnothersnew.Size = New System.Drawing.Size(193, 47)
        Me.chkAnothersnew.TabIndex = 19
        Me.chkAnothersnew.Text = "อื่นๆโปรดระบุ..."
        Me.chkAnothersnew.UseVisualStyleBackColor = True
        '
        'chkEmpOffice
        '
        Me.chkEmpOffice.AutoSize = True
        Me.chkEmpOffice.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkEmpOffice.Location = New System.Drawing.Point(16, 87)
        Me.chkEmpOffice.Margin = New System.Windows.Forms.Padding(6)
        Me.chkEmpOffice.Name = "chkEmpOffice"
        Me.chkEmpOffice.Size = New System.Drawing.Size(218, 47)
        Me.chkEmpOffice.TabIndex = 17
        Me.chkEmpOffice.Text = "สำนักงานจัดหางาน"
        Me.chkEmpOffice.UseVisualStyleBackColor = True
        '
        'chkAnnouncement
        '
        Me.chkAnnouncement.AutoSize = True
        Me.chkAnnouncement.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkAnnouncement.Location = New System.Drawing.Point(262, 87)
        Me.chkAnnouncement.Margin = New System.Windows.Forms.Padding(6)
        Me.chkAnnouncement.Name = "chkAnnouncement"
        Me.chkAnnouncement.Size = New System.Drawing.Size(226, 47)
        Me.chkAnnouncement.TabIndex = 18
        Me.chkAnnouncement.Text = "เสียงตามสายหมู่บ้าน"
        Me.chkAnnouncement.UseVisualStyleBackColor = True
        '
        'chkSignsrecruit
        '
        Me.chkSignsrecruit.AutoSize = True
        Me.chkSignsrecruit.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkSignsrecruit.Location = New System.Drawing.Point(262, 40)
        Me.chkSignsrecruit.Margin = New System.Windows.Forms.Padding(6)
        Me.chkSignsrecruit.Name = "chkSignsrecruit"
        Me.chkSignsrecruit.Size = New System.Drawing.Size(287, 47)
        Me.chkSignsrecruit.TabIndex = 15
        Me.chkSignsrecruit.Text = "ป้ายรับสมัครงานหน้าบริษัท"
        Me.chkSignsrecruit.UseVisualStyleBackColor = True
        '
        'chkInternet
        '
        Me.chkInternet.AutoSize = True
        Me.chkInternet.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkInternet.Location = New System.Drawing.Point(16, 40)
        Me.chkInternet.Margin = New System.Windows.Forms.Padding(6)
        Me.chkInternet.Name = "chkInternet"
        Me.chkInternet.Size = New System.Drawing.Size(139, 47)
        Me.chkInternet.TabIndex = 14
        Me.chkInternet.Text = "อิเตอร์เน็ต"
        Me.chkInternet.UseVisualStyleBackColor = True
        '
        'chkAdvicefriend
        '
        Me.chkAdvicefriend.AutoSize = True
        Me.chkAdvicefriend.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkAdvicefriend.Location = New System.Drawing.Point(588, 40)
        Me.chkAdvicefriend.Margin = New System.Windows.Forms.Padding(6)
        Me.chkAdvicefriend.Name = "chkAdvicefriend"
        Me.chkAdvicefriend.Size = New System.Drawing.Size(252, 47)
        Me.chkAdvicefriend.TabIndex = 16
        Me.chkAdvicefriend.Text = "จากเพื่อนหรือบุคคลอื่น"
        Me.chkAdvicefriend.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.TextBox3)
        Me.GroupBox5.Controls.Add(Me.txtDistrict)
        Me.GroupBox5.Controls.Add(Me.txtCanton)
        Me.GroupBox5.Controls.Add(Me.txtZipcode)
        Me.GroupBox5.Controls.Add(Me.lblPostcode)
        Me.GroupBox5.Controls.Add(Me.txtTelephone)
        Me.GroupBox5.Controls.Add(Me.lblTelephone)
        Me.GroupBox5.Controls.Add(Me.txtMobile)
        Me.GroupBox5.Controls.Add(Me.lblSmartphone)
        Me.GroupBox5.Controls.Add(Me.txtAddress)
        Me.GroupBox5.Controls.Add(Me.lblProvince)
        Me.GroupBox5.Controls.Add(Me.lblDistrict)
        Me.GroupBox5.Controls.Add(Me.lblSubdistrict)
        Me.GroupBox5.Controls.Add(Me.txtMoo)
        Me.GroupBox5.Controls.Add(Me.lblNum)
        Me.GroupBox5.Controls.Add(Me.lblMu)
        Me.GroupBox5.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(16, 675)
        Me.GroupBox5.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox5.Size = New System.Drawing.Size(2164, 102)
        Me.GroupBox5.TabIndex = 4
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "ที่อยู่ปัจจุบัน"
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(1058, 38)
        Me.TextBox3.Margin = New System.Windows.Forms.Padding(6)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(120, 51)
        Me.TextBox3.TabIndex = 67
        '
        'txtDistrict
        '
        Me.txtDistrict.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtDistrict.Location = New System.Drawing.Point(783, 38)
        Me.txtDistrict.Margin = New System.Windows.Forms.Padding(6)
        Me.txtDistrict.Name = "txtDistrict"
        Me.txtDistrict.Size = New System.Drawing.Size(120, 51)
        Me.txtDistrict.TabIndex = 66
        '
        'txtCanton
        '
        Me.txtCanton.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtCanton.Location = New System.Drawing.Point(515, 41)
        Me.txtCanton.Margin = New System.Windows.Forms.Padding(6)
        Me.txtCanton.Name = "txtCanton"
        Me.txtCanton.Size = New System.Drawing.Size(120, 51)
        Me.txtCanton.TabIndex = 65
        '
        'txtZipcode
        '
        Me.txtZipcode.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtZipcode.Location = New System.Drawing.Point(1394, 38)
        Me.txtZipcode.Margin = New System.Windows.Forms.Padding(6)
        Me.txtZipcode.Name = "txtZipcode"
        Me.txtZipcode.Size = New System.Drawing.Size(96, 51)
        Me.txtZipcode.TabIndex = 45
        '
        'lblPostcode
        '
        Me.lblPostcode.AutoSize = True
        Me.lblPostcode.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblPostcode.Location = New System.Drawing.Point(1254, 44)
        Me.lblPostcode.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblPostcode.Name = "lblPostcode"
        Me.lblPostcode.Size = New System.Drawing.Size(137, 43)
        Me.lblPostcode.TabIndex = 64
        Me.lblPostcode.Text = "รหัสไปรษณี :"
        '
        'txtTelephone
        '
        Me.txtTelephone.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTelephone.Location = New System.Drawing.Point(1643, 39)
        Me.txtTelephone.Margin = New System.Windows.Forms.Padding(6)
        Me.txtTelephone.Name = "txtTelephone"
        Me.txtTelephone.Size = New System.Drawing.Size(166, 51)
        Me.txtTelephone.TabIndex = 46
        '
        'lblTelephone
        '
        Me.lblTelephone.AutoSize = True
        Me.lblTelephone.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTelephone.Location = New System.Drawing.Point(1524, 46)
        Me.lblTelephone.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblTelephone.Name = "lblTelephone"
        Me.lblTelephone.Size = New System.Drawing.Size(107, 43)
        Me.lblTelephone.TabIndex = 58
        Me.lblTelephone.Text = "โทรศัพท์ :"
        '
        'txtMobile
        '
        Me.txtMobile.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtMobile.Location = New System.Drawing.Point(1984, 38)
        Me.txtMobile.Margin = New System.Windows.Forms.Padding(6)
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.Size = New System.Drawing.Size(148, 51)
        Me.txtMobile.TabIndex = 47
        '
        'lblSmartphone
        '
        Me.lblSmartphone.AutoSize = True
        Me.lblSmartphone.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSmartphone.Location = New System.Drawing.Point(1821, 43)
        Me.lblSmartphone.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSmartphone.Name = "lblSmartphone"
        Me.lblSmartphone.Size = New System.Drawing.Size(159, 43)
        Me.lblSmartphone.TabIndex = 51
        Me.lblSmartphone.Text = "โทรศัพท์มือถือ :"
        '
        'txtAddress
        '
        Me.txtAddress.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(128, 43)
        Me.txtAddress.Margin = New System.Windows.Forms.Padding(6)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(102, 51)
        Me.txtAddress.TabIndex = 40
        '
        'lblProvince
        '
        Me.lblProvince.AutoSize = True
        Me.lblProvince.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblProvince.Location = New System.Drawing.Point(958, 44)
        Me.lblProvince.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblProvince.Name = "lblProvince"
        Me.lblProvince.Size = New System.Drawing.Size(90, 43)
        Me.lblProvince.TabIndex = 46
        Me.lblProvince.Text = "จังหวัด :"
        '
        'lblDistrict
        '
        Me.lblDistrict.AutoSize = True
        Me.lblDistrict.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblDistrict.Location = New System.Drawing.Point(688, 44)
        Me.lblDistrict.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblDistrict.Name = "lblDistrict"
        Me.lblDistrict.Size = New System.Drawing.Size(83, 43)
        Me.lblDistrict.TabIndex = 44
        Me.lblDistrict.Text = "อำเภอ :"
        '
        'lblSubdistrict
        '
        Me.lblSubdistrict.AutoSize = True
        Me.lblSubdistrict.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSubdistrict.Location = New System.Drawing.Point(430, 44)
        Me.lblSubdistrict.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSubdistrict.Name = "lblSubdistrict"
        Me.lblSubdistrict.Size = New System.Drawing.Size(79, 43)
        Me.lblSubdistrict.TabIndex = 43
        Me.lblSubdistrict.Text = "ตำบล :"
        '
        'txtMoo
        '
        Me.txtMoo.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtMoo.Location = New System.Drawing.Point(338, 42)
        Me.txtMoo.Margin = New System.Windows.Forms.Padding(6)
        Me.txtMoo.Name = "txtMoo"
        Me.txtMoo.Size = New System.Drawing.Size(62, 51)
        Me.txtMoo.TabIndex = 41
        '
        'lblNum
        '
        Me.lblNum.AutoSize = True
        Me.lblNum.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblNum.Location = New System.Drawing.Point(14, 44)
        Me.lblNum.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblNum.Name = "lblNum"
        Me.lblNum.Size = New System.Drawing.Size(113, 43)
        Me.lblNum.TabIndex = 40
        Me.lblNum.Text = "บ้านเลขที่ :"
        '
        'lblMu
        '
        Me.lblMu.AutoSize = True
        Me.lblMu.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblMu.Location = New System.Drawing.Point(264, 44)
        Me.lblMu.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblMu.Name = "lblMu"
        Me.lblMu.Size = New System.Drawing.Size(72, 43)
        Me.lblMu.TabIndex = 39
        Me.lblMu.Text = "หมู่ที่ :"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.txtProvincemarry)
        Me.GroupBox6.Controls.Add(Me.txtDdistrictmarry)
        Me.GroupBox6.Controls.Add(Me.txtCantonmarry)
        Me.GroupBox6.Controls.Add(Me.lblSpouseprovince)
        Me.GroupBox6.Controls.Add(Me.lblSpousepostcode)
        Me.GroupBox6.Controls.Add(Me.txtAddressmarry)
        Me.GroupBox6.Controls.Add(Me.txtZipcodemarry)
        Me.GroupBox6.Controls.Add(Me.txtTephonemarry)
        Me.GroupBox6.Controls.Add(Me.lblSpousetelephone)
        Me.GroupBox6.Controls.Add(Me.txtMobilemarry)
        Me.GroupBox6.Controls.Add(Me.lblSpousesmartphone)
        Me.GroupBox6.Controls.Add(Me.lblSpousedistrict)
        Me.GroupBox6.Controls.Add(Me.lblSpousesubdistrict)
        Me.GroupBox6.Controls.Add(Me.txtMoomarry)
        Me.GroupBox6.Controls.Add(Me.lblSpouseNum)
        Me.GroupBox6.Controls.Add(Me.lblSpousemu)
        Me.GroupBox6.Controls.Add(Me.txtNicknamemarry)
        Me.GroupBox6.Controls.Add(Me.lblSpousenickname)
        Me.GroupBox6.Controls.Add(Me.txtLnamemarry)
        Me.GroupBox6.Controls.Add(Me.txtFnamemarry)
        Me.GroupBox6.Controls.Add(Me.cboTitlemarry)
        Me.GroupBox6.Controls.Add(Me.lblSpouselastname)
        Me.GroupBox6.Controls.Add(Me.lblSpousetitle)
        Me.GroupBox6.Controls.Add(Me.lblSpousename)
        Me.GroupBox6.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(18, 773)
        Me.GroupBox6.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox6.Size = New System.Drawing.Size(2164, 190)
        Me.GroupBox6.TabIndex = 5
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "ข้อมูลคู่สมรส"
        '
        'txtProvincemarry
        '
        Me.txtProvincemarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtProvincemarry.Location = New System.Drawing.Point(1162, 113)
        Me.txtProvincemarry.Margin = New System.Windows.Forms.Padding(6)
        Me.txtProvincemarry.Name = "txtProvincemarry"
        Me.txtProvincemarry.Size = New System.Drawing.Size(166, 51)
        Me.txtProvincemarry.TabIndex = 88
        '
        'txtDdistrictmarry
        '
        Me.txtDdistrictmarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtDdistrictmarry.Location = New System.Drawing.Point(858, 118)
        Me.txtDdistrictmarry.Margin = New System.Windows.Forms.Padding(6)
        Me.txtDdistrictmarry.Name = "txtDdistrictmarry"
        Me.txtDdistrictmarry.Size = New System.Drawing.Size(148, 51)
        Me.txtDdistrictmarry.TabIndex = 87
        '
        'txtCantonmarry
        '
        Me.txtCantonmarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtCantonmarry.Location = New System.Drawing.Point(568, 121)
        Me.txtCantonmarry.Margin = New System.Windows.Forms.Padding(6)
        Me.txtCantonmarry.Name = "txtCantonmarry"
        Me.txtCantonmarry.Size = New System.Drawing.Size(166, 51)
        Me.txtCantonmarry.TabIndex = 86
        '
        'lblSpouseprovince
        '
        Me.lblSpouseprovince.AutoSize = True
        Me.lblSpouseprovince.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSpouseprovince.Location = New System.Drawing.Point(1074, 121)
        Me.lblSpouseprovince.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSpouseprovince.Name = "lblSpouseprovince"
        Me.lblSpouseprovince.Size = New System.Drawing.Size(90, 43)
        Me.lblSpouseprovince.TabIndex = 85
        Me.lblSpouseprovince.Text = "จังหวัด :"
        '
        'lblSpousepostcode
        '
        Me.lblSpousepostcode.AutoSize = True
        Me.lblSpousepostcode.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSpousepostcode.Location = New System.Drawing.Point(1412, 121)
        Me.lblSpousepostcode.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSpousepostcode.Name = "lblSpousepostcode"
        Me.lblSpousepostcode.Size = New System.Drawing.Size(137, 43)
        Me.lblSpousepostcode.TabIndex = 84
        Me.lblSpousepostcode.Text = "รหัสไปรษณี :"
        '
        'txtAddressmarry
        '
        Me.txtAddressmarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtAddressmarry.Location = New System.Drawing.Point(126, 115)
        Me.txtAddressmarry.Margin = New System.Windows.Forms.Padding(6)
        Me.txtAddressmarry.Name = "txtAddressmarry"
        Me.txtAddressmarry.Size = New System.Drawing.Size(100, 51)
        Me.txtAddressmarry.TabIndex = 54
        '
        'txtZipcodemarry
        '
        Me.txtZipcodemarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtZipcodemarry.Location = New System.Drawing.Point(1560, 115)
        Me.txtZipcodemarry.Margin = New System.Windows.Forms.Padding(6)
        Me.txtZipcodemarry.Name = "txtZipcodemarry"
        Me.txtZipcodemarry.Size = New System.Drawing.Size(96, 51)
        Me.txtZipcodemarry.TabIndex = 59
        '
        'txtTephonemarry
        '
        Me.txtTephonemarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTephonemarry.Location = New System.Drawing.Point(1162, 40)
        Me.txtTephonemarry.Margin = New System.Windows.Forms.Padding(6)
        Me.txtTephonemarry.Name = "txtTephonemarry"
        Me.txtTephonemarry.Size = New System.Drawing.Size(166, 51)
        Me.txtTephonemarry.TabIndex = 52
        '
        'lblSpousetelephone
        '
        Me.lblSpousetelephone.AutoSize = True
        Me.lblSpousetelephone.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSpousetelephone.Location = New System.Drawing.Point(1052, 46)
        Me.lblSpousetelephone.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSpousetelephone.Name = "lblSpousetelephone"
        Me.lblSpousetelephone.Size = New System.Drawing.Size(107, 43)
        Me.lblSpousetelephone.TabIndex = 79
        Me.lblSpousetelephone.Text = "โทรศัพท์ :"
        '
        'txtMobilemarry
        '
        Me.txtMobilemarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtMobilemarry.Location = New System.Drawing.Point(1556, 40)
        Me.txtMobilemarry.Margin = New System.Windows.Forms.Padding(6)
        Me.txtMobilemarry.Name = "txtMobilemarry"
        Me.txtMobilemarry.Size = New System.Drawing.Size(166, 51)
        Me.txtMobilemarry.TabIndex = 53
        '
        'lblSpousesmartphone
        '
        Me.lblSpousesmartphone.AutoSize = True
        Me.lblSpousesmartphone.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSpousesmartphone.Location = New System.Drawing.Point(1386, 46)
        Me.lblSpousesmartphone.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSpousesmartphone.Name = "lblSpousesmartphone"
        Me.lblSpousesmartphone.Size = New System.Drawing.Size(159, 43)
        Me.lblSpousesmartphone.TabIndex = 77
        Me.lblSpousesmartphone.Text = "โทรศัพท์มือถือ :"
        '
        'lblSpousedistrict
        '
        Me.lblSpousedistrict.AutoSize = True
        Me.lblSpousedistrict.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSpousedistrict.Location = New System.Drawing.Point(770, 121)
        Me.lblSpousedistrict.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSpousedistrict.Name = "lblSpousedistrict"
        Me.lblSpousedistrict.Size = New System.Drawing.Size(83, 43)
        Me.lblSpousedistrict.TabIndex = 74
        Me.lblSpousedistrict.Text = "อำเภอ :"
        '
        'lblSpousesubdistrict
        '
        Me.lblSpousesubdistrict.AutoSize = True
        Me.lblSpousesubdistrict.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSpousesubdistrict.Location = New System.Drawing.Point(490, 121)
        Me.lblSpousesubdistrict.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSpousesubdistrict.Name = "lblSpousesubdistrict"
        Me.lblSpousesubdistrict.Size = New System.Drawing.Size(79, 43)
        Me.lblSpousesubdistrict.TabIndex = 73
        Me.lblSpousesubdistrict.Text = "ตำบล :"
        '
        'txtMoomarry
        '
        Me.txtMoomarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtMoomarry.Location = New System.Drawing.Point(334, 115)
        Me.txtMoomarry.Margin = New System.Windows.Forms.Padding(6)
        Me.txtMoomarry.Name = "txtMoomarry"
        Me.txtMoomarry.Size = New System.Drawing.Size(62, 51)
        Me.txtMoomarry.TabIndex = 55
        '
        'lblSpouseNum
        '
        Me.lblSpouseNum.AutoSize = True
        Me.lblSpouseNum.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSpouseNum.Location = New System.Drawing.Point(14, 121)
        Me.lblSpouseNum.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSpouseNum.Name = "lblSpouseNum"
        Me.lblSpouseNum.Size = New System.Drawing.Size(113, 43)
        Me.lblSpouseNum.TabIndex = 71
        Me.lblSpouseNum.Text = "บ้านเลขที่ :"
        '
        'lblSpousemu
        '
        Me.lblSpousemu.AutoSize = True
        Me.lblSpousemu.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSpousemu.Location = New System.Drawing.Point(260, 121)
        Me.lblSpousemu.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSpousemu.Name = "lblSpousemu"
        Me.lblSpousemu.Size = New System.Drawing.Size(72, 43)
        Me.lblSpousemu.TabIndex = 70
        Me.lblSpousemu.Text = "หมู่ที่ :"
        '
        'txtNicknamemarry
        '
        Me.txtNicknamemarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtNicknamemarry.Location = New System.Drawing.Point(858, 42)
        Me.txtNicknamemarry.Margin = New System.Windows.Forms.Padding(6)
        Me.txtNicknamemarry.Name = "txtNicknamemarry"
        Me.txtNicknamemarry.Size = New System.Drawing.Size(148, 51)
        Me.txtNicknamemarry.TabIndex = 51
        '
        'lblSpousenickname
        '
        Me.lblSpousenickname.AutoSize = True
        Me.lblSpousenickname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSpousenickname.Location = New System.Drawing.Point(770, 46)
        Me.lblSpousenickname.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSpousenickname.Name = "lblSpousenickname"
        Me.lblSpousenickname.Size = New System.Drawing.Size(89, 43)
        Me.lblSpousenickname.TabIndex = 67
        Me.lblSpousenickname.Text = "ชื่อเล่น :"
        '
        'txtLnamemarry
        '
        Me.txtLnamemarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtLnamemarry.Location = New System.Drawing.Point(568, 42)
        Me.txtLnamemarry.Margin = New System.Windows.Forms.Padding(6)
        Me.txtLnamemarry.Name = "txtLnamemarry"
        Me.txtLnamemarry.Size = New System.Drawing.Size(166, 51)
        Me.txtLnamemarry.TabIndex = 50
        '
        'txtFnamemarry
        '
        Me.txtFnamemarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtFnamemarry.Location = New System.Drawing.Point(336, 42)
        Me.txtFnamemarry.Margin = New System.Windows.Forms.Padding(6)
        Me.txtFnamemarry.Name = "txtFnamemarry"
        Me.txtFnamemarry.Size = New System.Drawing.Size(120, 51)
        Me.txtFnamemarry.TabIndex = 49
        '
        'cboTitlemarry
        '
        Me.cboTitlemarry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTitlemarry.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboTitlemarry.FormattingEnabled = True
        Me.cboTitlemarry.Location = New System.Drawing.Point(128, 42)
        Me.cboTitlemarry.Margin = New System.Windows.Forms.Padding(6)
        Me.cboTitlemarry.Name = "cboTitlemarry"
        Me.cboTitlemarry.Size = New System.Drawing.Size(100, 51)
        Me.cboTitlemarry.TabIndex = 48
        '
        'lblSpouselastname
        '
        Me.lblSpouselastname.AutoSize = True
        Me.lblSpouselastname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSpouselastname.Location = New System.Drawing.Point(500, 46)
        Me.lblSpouselastname.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSpouselastname.Name = "lblSpouselastname"
        Me.lblSpouselastname.Size = New System.Drawing.Size(70, 43)
        Me.lblSpouselastname.TabIndex = 63
        Me.lblSpouselastname.Text = "สกุล :"
        '
        'lblSpousetitle
        '
        Me.lblSpousetitle.AutoSize = True
        Me.lblSpousetitle.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSpousetitle.Location = New System.Drawing.Point(12, 46)
        Me.lblSpousetitle.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSpousetitle.Name = "lblSpousetitle"
        Me.lblSpousetitle.Size = New System.Drawing.Size(115, 43)
        Me.lblSpousetitle.TabIndex = 62
        Me.lblSpousetitle.Text = "คำนำหน้า :"
        '
        'lblSpousename
        '
        Me.lblSpousename.AutoSize = True
        Me.lblSpousename.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSpousename.Location = New System.Drawing.Point(278, 46)
        Me.lblSpousename.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblSpousename.Name = "lblSpousename"
        Me.lblSpousename.Size = New System.Drawing.Size(56, 43)
        Me.lblSpousename.TabIndex = 61
        Me.lblSpousename.Text = "ชื่อ :"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.RadioButton10)
        Me.GroupBox7.Controls.Add(Me.rdbS)
        Me.GroupBox7.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox7.Location = New System.Drawing.Point(1638, 1087)
        Me.GroupBox7.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox7.Size = New System.Drawing.Size(332, 125)
        Me.GroupBox7.TabIndex = 70
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "มีประกันสังคมหรือไม่"
        '
        'RadioButton10
        '
        Me.RadioButton10.AutoSize = True
        Me.RadioButton10.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.RadioButton10.Location = New System.Drawing.Point(146, 48)
        Me.RadioButton10.Margin = New System.Windows.Forms.Padding(4)
        Me.RadioButton10.Name = "RadioButton10"
        Me.RadioButton10.Size = New System.Drawing.Size(87, 47)
        Me.RadioButton10.TabIndex = 71
        Me.RadioButton10.TabStop = True
        Me.RadioButton10.Text = "ไม่มี"
        Me.RadioButton10.UseVisualStyleBackColor = True
        '
        'rdbS
        '
        Me.rdbS.AutoSize = True
        Me.rdbS.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbS.Location = New System.Drawing.Point(40, 48)
        Me.rdbS.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbS.Name = "rdbS"
        Me.rdbS.Size = New System.Drawing.Size(63, 47)
        Me.rdbS.TabIndex = 70
        Me.rdbS.TabStop = True
        Me.rdbS.Text = "มี"
        Me.rdbS.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.rdbWomen)
        Me.GroupBox8.Controls.Add(Me.rdbMan)
        Me.GroupBox8.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox8.Location = New System.Drawing.Point(1060, 265)
        Me.GroupBox8.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox8.Size = New System.Drawing.Size(248, 142)
        Me.GroupBox8.TabIndex = 21
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "เพศ"
        '
        'rdbWomen
        '
        Me.rdbWomen.AutoSize = True
        Me.rdbWomen.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbWomen.Location = New System.Drawing.Point(122, 62)
        Me.rdbWomen.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbWomen.Name = "rdbWomen"
        Me.rdbWomen.Size = New System.Drawing.Size(92, 47)
        Me.rdbWomen.TabIndex = 22
        Me.rdbWomen.TabStop = True
        Me.rdbWomen.Text = "หญิง"
        Me.rdbWomen.UseVisualStyleBackColor = True
        '
        'rdbMan
        '
        Me.rdbMan.AutoSize = True
        Me.rdbMan.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbMan.Location = New System.Drawing.Point(18, 62)
        Me.rdbMan.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbMan.Name = "rdbMan"
        Me.rdbMan.Size = New System.Drawing.Size(83, 47)
        Me.rdbMan.TabIndex = 21
        Me.rdbMan.TabStop = True
        Me.rdbMan.Text = "ชาย"
        Me.rdbMan.UseVisualStyleBackColor = True
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.txtHiswork)
        Me.GroupBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.GroupBox9.Location = New System.Drawing.Point(16, 975)
        Me.GroupBox9.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox9.Size = New System.Drawing.Size(2160, 100)
        Me.GroupBox9.TabIndex = 32
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "ประวัติการทำงาน"
        '
        'txtHiswork
        '
        Me.txtHiswork.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtHiswork.Location = New System.Drawing.Point(16, 42)
        Me.txtHiswork.Margin = New System.Windows.Forms.Padding(6)
        Me.txtHiswork.Name = "txtHiswork"
        Me.txtHiswork.Size = New System.Drawing.Size(1928, 32)
        Me.txtHiswork.TabIndex = 60
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.rdbSicknessno)
        Me.GroupBox10.Controls.Add(Me.rdbSickness)
        Me.GroupBox10.Controls.Add(Me.txtSickness)
        Me.GroupBox10.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox10.Location = New System.Drawing.Point(18, 1087)
        Me.GroupBox10.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox10.Size = New System.Drawing.Size(498, 125)
        Me.GroupBox10.TabIndex = 61
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "โรคประจำตัว"
        '
        'rdbSicknessno
        '
        Me.rdbSicknessno.AutoSize = True
        Me.rdbSicknessno.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbSicknessno.Location = New System.Drawing.Point(46, 44)
        Me.rdbSicknessno.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbSicknessno.Name = "rdbSicknessno"
        Me.rdbSicknessno.Size = New System.Drawing.Size(87, 47)
        Me.rdbSicknessno.TabIndex = 61
        Me.rdbSicknessno.TabStop = True
        Me.rdbSicknessno.Text = "ไม่มี"
        Me.rdbSicknessno.UseVisualStyleBackColor = True
        '
        'rdbSickness
        '
        Me.rdbSickness.AutoSize = True
        Me.rdbSickness.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbSickness.Location = New System.Drawing.Point(172, 44)
        Me.rdbSickness.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbSickness.Name = "rdbSickness"
        Me.rdbSickness.Size = New System.Drawing.Size(78, 47)
        Me.rdbSickness.TabIndex = 62
        Me.rdbSickness.TabStop = True
        Me.rdbSickness.Text = "มี..."
        Me.rdbSickness.UseVisualStyleBackColor = True
        '
        'txtSickness
        '
        Me.txtSickness.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtSickness.Location = New System.Drawing.Point(268, 44)
        Me.txtSickness.Margin = New System.Windows.Forms.Padding(6)
        Me.txtSickness.Name = "txtSickness"
        Me.txtSickness.Size = New System.Drawing.Size(188, 51)
        Me.txtSickness.TabIndex = 63
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.rdbCigaretteno)
        Me.GroupBox11.Controls.Add(Me.rdbCigarette)
        Me.GroupBox11.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox11.Location = New System.Drawing.Point(598, 1087)
        Me.GroupBox11.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox11.Size = New System.Drawing.Size(262, 125)
        Me.GroupBox11.TabIndex = 62
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "สูบบุหรี่หรือไม่"
        '
        'rdbCigaretteno
        '
        Me.rdbCigaretteno.AutoSize = True
        Me.rdbCigaretteno.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbCigaretteno.Location = New System.Drawing.Point(128, 48)
        Me.rdbCigaretteno.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbCigaretteno.Name = "rdbCigaretteno"
        Me.rdbCigaretteno.Size = New System.Drawing.Size(102, 47)
        Me.rdbCigaretteno.TabIndex = 65
        Me.rdbCigaretteno.TabStop = True
        Me.rdbCigaretteno.Text = "ไม่สูบ"
        Me.rdbCigaretteno.UseVisualStyleBackColor = True
        '
        'rdbCigarette
        '
        Me.rdbCigarette.AutoSize = True
        Me.rdbCigarette.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbCigarette.Location = New System.Drawing.Point(22, 48)
        Me.rdbCigarette.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbCigarette.Name = "rdbCigarette"
        Me.rdbCigarette.Size = New System.Drawing.Size(78, 47)
        Me.rdbCigarette.TabIndex = 64
        Me.rdbCigarette.TabStop = True
        Me.rdbCigarette.Text = "สูบ"
        Me.rdbCigarette.UseVisualStyleBackColor = True
        '
        'rdbEat
        '
        Me.rdbEat.Controls.Add(Me.rdbEatno)
        Me.rdbEat.Controls.Add(Me.RadioButton5)
        Me.rdbEat.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbEat.Location = New System.Drawing.Point(944, 1087)
        Me.rdbEat.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.rdbEat.Name = "rdbEat"
        Me.rdbEat.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.rdbEat.Size = New System.Drawing.Size(262, 125)
        Me.rdbEat.TabIndex = 66
        Me.rdbEat.TabStop = False
        Me.rdbEat.Text = "กินหมากหรือไม่"
        '
        'rdbEatno
        '
        Me.rdbEatno.AutoSize = True
        Me.rdbEatno.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbEatno.Location = New System.Drawing.Point(130, 48)
        Me.rdbEatno.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbEatno.Name = "rdbEatno"
        Me.rdbEatno.Size = New System.Drawing.Size(102, 47)
        Me.rdbEatno.TabIndex = 67
        Me.rdbEatno.TabStop = True
        Me.rdbEatno.Text = "ไม่กิน"
        Me.rdbEatno.UseVisualStyleBackColor = True
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = True
        Me.RadioButton5.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.RadioButton5.Location = New System.Drawing.Point(34, 48)
        Me.RadioButton5.Margin = New System.Windows.Forms.Padding(4)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(78, 47)
        Me.RadioButton5.TabIndex = 66
        Me.RadioButton5.TabStop = True
        Me.RadioButton5.Text = "กิน"
        Me.RadioButton5.UseVisualStyleBackColor = True
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.rdbPrisonno)
        Me.GroupBox13.Controls.Add(Me.rdbPrison)
        Me.GroupBox13.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox13.Location = New System.Drawing.Point(1290, 1087)
        Me.GroupBox13.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox13.Size = New System.Drawing.Size(262, 125)
        Me.GroupBox13.TabIndex = 68
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "ท่านต้องโทษหรือไม่"
        '
        'rdbPrisonno
        '
        Me.rdbPrisonno.AutoSize = True
        Me.rdbPrisonno.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbPrisonno.Location = New System.Drawing.Point(140, 48)
        Me.rdbPrisonno.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbPrisonno.Name = "rdbPrisonno"
        Me.rdbPrisonno.Size = New System.Drawing.Size(103, 47)
        Me.rdbPrisonno.TabIndex = 69
        Me.rdbPrisonno.TabStop = True
        Me.rdbPrisonno.Text = "ไม่เคย"
        Me.rdbPrisonno.UseVisualStyleBackColor = True
        '
        'rdbPrison
        '
        Me.rdbPrison.AutoSize = True
        Me.rdbPrison.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbPrison.Location = New System.Drawing.Point(44, 48)
        Me.rdbPrison.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbPrison.Name = "rdbPrison"
        Me.rdbPrison.Size = New System.Drawing.Size(79, 47)
        Me.rdbPrison.TabIndex = 68
        Me.rdbPrison.TabStop = True
        Me.rdbPrison.Text = "เคย"
        Me.rdbPrison.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.Location = New System.Drawing.Point(620, 1256)
        Me.butSave.Margin = New System.Windows.Forms.Padding(6)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(160, 58)
        Me.butSave.TabIndex = 73
        Me.butSave.Text = "บันทึก"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'butDel
        '
        Me.butDel.Location = New System.Drawing.Point(821, 1256)
        Me.butDel.Margin = New System.Windows.Forms.Padding(6)
        Me.butDel.Name = "butDel"
        Me.butDel.Size = New System.Drawing.Size(160, 58)
        Me.butDel.TabIndex = 75
        Me.butDel.Text = "ลบ"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'butClear
        '
        Me.butClear.Location = New System.Drawing.Point(1032, 1256)
        Me.butClear.Margin = New System.Windows.Forms.Padding(6)
        Me.butClear.Name = "butClear"
        Me.butClear.Size = New System.Drawing.Size(160, 58)
        Me.butClear.TabIndex = 72
        Me.butClear.Text = "เคลียร์"
        Me.butClear.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(2016, 13)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(218, 256)
        Me.PictureBox1.TabIndex = 44
        Me.PictureBox1.TabStop = False
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.rdbContain)
        Me.GroupBox14.Controls.Add(Me.rdbTrial)
        Me.GroupBox14.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox14.Location = New System.Drawing.Point(1360, 265)
        Me.GroupBox14.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox14.Size = New System.Drawing.Size(326, 142)
        Me.GroupBox14.TabIndex = 78
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "สถานะการทำงาน"
        '
        'rdbContain
        '
        Me.rdbContain.AutoSize = True
        Me.rdbContain.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbContain.Location = New System.Drawing.Point(144, 63)
        Me.rdbContain.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbContain.Name = "rdbContain"
        Me.rdbContain.Size = New System.Drawing.Size(97, 47)
        Me.rdbContain.TabIndex = 22
        Me.rdbContain.TabStop = True
        Me.rdbContain.Text = "บรรจุ"
        Me.rdbContain.UseVisualStyleBackColor = True
        '
        'rdbTrial
        '
        Me.rdbTrial.AutoSize = True
        Me.rdbTrial.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.rdbTrial.Location = New System.Drawing.Point(18, 62)
        Me.rdbTrial.Margin = New System.Windows.Forms.Padding(4)
        Me.rdbTrial.Name = "rdbTrial"
        Me.rdbTrial.Size = New System.Drawing.Size(113, 47)
        Me.rdbTrial.TabIndex = 21
        Me.rdbTrial.TabStop = True
        Me.rdbTrial.Text = "ทดลอง"
        Me.rdbTrial.UseVisualStyleBackColor = True
        '
        'GroupBox30
        '
        Me.GroupBox30.Controls.Add(Me.txtByprovince)
        Me.GroupBox30.Controls.Add(Me.txtBycanton)
        Me.GroupBox30.Controls.Add(Me.dtpBirthday)
        Me.GroupBox30.Controls.Add(Me.dtpEndidcard)
        Me.GroupBox30.Controls.Add(Me.dtpStartidcard)
        Me.GroupBox30.Controls.Add(Me.cboEducation)
        Me.GroupBox30.Controls.Add(Me.lblEducation)
        Me.GroupBox30.Controls.Add(Me.lblM)
        Me.GroupBox30.Controls.Add(Me.lblIssuedatdistrict)
        Me.GroupBox30.Controls.Add(Me.lblKg)
        Me.GroupBox30.Controls.Add(Me.lblIssuedatprovince)
        Me.GroupBox30.Controls.Add(Me.lblYear)
        Me.GroupBox30.Controls.Add(Me.txtNickname)
        Me.GroupBox30.Controls.Add(Me.lblNickname)
        Me.GroupBox30.Controls.Add(Me.cboBlood)
        Me.GroupBox30.Controls.Add(Me.lblGblood)
        Me.GroupBox30.Controls.Add(Me.lblEndcard)
        Me.GroupBox30.Controls.Add(Me.txtHeight)
        Me.GroupBox30.Controls.Add(Me.lblStartcard)
        Me.GroupBox30.Controls.Add(Me.lblHeight)
        Me.GroupBox30.Controls.Add(Me.txtWeight)
        Me.GroupBox30.Controls.Add(Me.txtIdcard)
        Me.GroupBox30.Controls.Add(Me.txtAge)
        Me.GroupBox30.Controls.Add(Me.lblWeight)
        Me.GroupBox30.Controls.Add(Me.lblIdcard)
        Me.GroupBox30.Controls.Add(Me.lblAge)
        Me.GroupBox30.Controls.Add(Me.cboReligion)
        Me.GroupBox30.Controls.Add(Me.lblReligion)
        Me.GroupBox30.Controls.Add(Me.cboNationality)
        Me.GroupBox30.Controls.Add(Me.lblNationality)
        Me.GroupBox30.Controls.Add(Me.lblBirthday)
        Me.GroupBox30.Controls.Add(Me.txtLname)
        Me.GroupBox30.Controls.Add(Me.txtFname)
        Me.GroupBox30.Controls.Add(Me.cboTitle)
        Me.GroupBox30.Controls.Add(Me.lblLastName)
        Me.GroupBox30.Controls.Add(Me.lblTitle)
        Me.GroupBox30.Controls.Add(Me.lblName)
        Me.GroupBox30.Controls.Add(Me.lblEmployeeData)
        Me.GroupBox30.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox30.Location = New System.Drawing.Point(20, 419)
        Me.GroupBox30.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox30.Name = "GroupBox30"
        Me.GroupBox30.Padding = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.GroupBox30.Size = New System.Drawing.Size(2162, 248)
        Me.GroupBox30.TabIndex = 107
        Me.GroupBox30.TabStop = False
        Me.GroupBox30.Text = "ข้อมูลผู้สมัคร"
        '
        'txtByprovince
        '
        Me.txtByprovince.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtByprovince.Location = New System.Drawing.Point(1914, 177)
        Me.txtByprovince.Margin = New System.Windows.Forms.Padding(6)
        Me.txtByprovince.Name = "txtByprovince"
        Me.txtByprovince.Size = New System.Drawing.Size(150, 51)
        Me.txtByprovince.TabIndex = 64
        '
        'txtBycanton
        '
        Me.txtBycanton.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtBycanton.Location = New System.Drawing.Point(1916, 107)
        Me.txtBycanton.Margin = New System.Windows.Forms.Padding(6)
        Me.txtBycanton.Name = "txtBycanton"
        Me.txtBycanton.Size = New System.Drawing.Size(150, 51)
        Me.txtBycanton.TabIndex = 63
        '
        'dtpBirthday
        '
        Me.dtpBirthday.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpBirthday.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBirthday.Location = New System.Drawing.Point(990, 38)
        Me.dtpBirthday.Margin = New System.Windows.Forms.Padding(6)
        Me.dtpBirthday.Name = "dtpBirthday"
        Me.dtpBirthday.Size = New System.Drawing.Size(196, 51)
        Me.dtpBirthday.TabIndex = 26
        '
        'dtpEndidcard
        '
        Me.dtpEndidcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpEndidcard.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndidcard.Location = New System.Drawing.Point(1456, 177)
        Me.dtpEndidcard.Margin = New System.Windows.Forms.Padding(6)
        Me.dtpEndidcard.Name = "dtpEndidcard"
        Me.dtpEndidcard.Size = New System.Drawing.Size(196, 51)
        Me.dtpEndidcard.TabIndex = 38
        '
        'dtpStartidcard
        '
        Me.dtpStartidcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpStartidcard.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartidcard.Location = New System.Drawing.Point(1456, 110)
        Me.dtpStartidcard.Margin = New System.Windows.Forms.Padding(6)
        Me.dtpStartidcard.Name = "dtpStartidcard"
        Me.dtpStartidcard.Size = New System.Drawing.Size(196, 51)
        Me.dtpStartidcard.TabIndex = 32
        '
        'cboEducation
        '
        Me.cboEducation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEducation.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboEducation.FormattingEnabled = True
        Me.cboEducation.Items.AddRange(New Object() {"ปริญญาโท", "ปริญญาตรี", "ปวส.", "ปวช.", "ม.6", "ม.3", "ป.6", "ป.4", "อื่นๆ"})
        Me.cboEducation.Location = New System.Drawing.Point(638, 179)
        Me.cboEducation.Margin = New System.Windows.Forms.Padding(6)
        Me.cboEducation.Name = "cboEducation"
        Me.cboEducation.Size = New System.Drawing.Size(156, 51)
        Me.cboEducation.TabIndex = 36
        '
        'lblEducation
        '
        Me.lblEducation.AutoSize = True
        Me.lblEducation.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblEducation.Location = New System.Drawing.Point(520, 185)
        Me.lblEducation.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblEducation.Name = "lblEducation"
        Me.lblEducation.Size = New System.Drawing.Size(111, 43)
        Me.lblEducation.TabIndex = 58
        Me.lblEducation.Text = "การศึกษา :"
        '
        'lblM
        '
        Me.lblM.AutoSize = True
        Me.lblM.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblM.Location = New System.Drawing.Point(1072, 115)
        Me.lblM.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblM.Name = "lblM"
        Me.lblM.Size = New System.Drawing.Size(51, 43)
        Me.lblM.TabIndex = 57
        Me.lblM.Text = "ซม."
        '
        'lblIssuedatdistrict
        '
        Me.lblIssuedatdistrict.AutoSize = True
        Me.lblIssuedatdistrict.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblIssuedatdistrict.Location = New System.Drawing.Point(1730, 115)
        Me.lblIssuedatdistrict.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblIssuedatdistrict.Name = "lblIssuedatdistrict"
        Me.lblIssuedatdistrict.Size = New System.Drawing.Size(174, 43)
        Me.lblIssuedatdistrict.TabIndex = 62
        Me.lblIssuedatdistrict.Text = "ออกให้ ณ อำเภอ :"
        '
        'lblKg
        '
        Me.lblKg.AutoSize = True
        Me.lblKg.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblKg.Location = New System.Drawing.Point(720, 115)
        Me.lblKg.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblKg.Name = "lblKg"
        Me.lblKg.Size = New System.Drawing.Size(50, 43)
        Me.lblKg.TabIndex = 56
        Me.lblKg.Text = "กก."
        '
        'lblIssuedatprovince
        '
        Me.lblIssuedatprovince.AutoSize = True
        Me.lblIssuedatprovince.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblIssuedatprovince.Location = New System.Drawing.Point(1790, 185)
        Me.lblIssuedatprovince.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblIssuedatprovince.Name = "lblIssuedatprovince"
        Me.lblIssuedatprovince.Size = New System.Drawing.Size(112, 43)
        Me.lblIssuedatprovince.TabIndex = 61
        Me.lblIssuedatprovince.Text = "ณ จังหวัด :"
        '
        'lblYear
        '
        Me.lblYear.AutoSize = True
        Me.lblYear.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblYear.Location = New System.Drawing.Point(436, 115)
        Me.lblYear.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(33, 43)
        Me.lblYear.TabIndex = 55
        Me.lblYear.Text = "ปี"
        '
        'txtNickname
        '
        Me.txtNickname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtNickname.Location = New System.Drawing.Point(128, 110)
        Me.txtNickname.Margin = New System.Windows.Forms.Padding(6)
        Me.txtNickname.Name = "txtNickname"
        Me.txtNickname.Size = New System.Drawing.Size(100, 51)
        Me.txtNickname.TabIndex = 28
        '
        'lblNickname
        '
        Me.lblNickname.AutoSize = True
        Me.lblNickname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblNickname.Location = New System.Drawing.Point(40, 115)
        Me.lblNickname.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblNickname.Name = "lblNickname"
        Me.lblNickname.Size = New System.Drawing.Size(89, 43)
        Me.lblNickname.TabIndex = 53
        Me.lblNickname.Text = "ชื่อเล่น :"
        '
        'cboBlood
        '
        Me.cboBlood.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBlood.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboBlood.FormattingEnabled = True
        Me.cboBlood.Items.AddRange(New Object() {"A", "B", "O", "AB", "อื่นๆ"})
        Me.cboBlood.Location = New System.Drawing.Point(364, 179)
        Me.cboBlood.Margin = New System.Windows.Forms.Padding(6)
        Me.cboBlood.Name = "cboBlood"
        Me.cboBlood.Size = New System.Drawing.Size(80, 51)
        Me.cboBlood.TabIndex = 35
        '
        'lblGblood
        '
        Me.lblGblood.AutoSize = True
        Me.lblGblood.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblGblood.Location = New System.Drawing.Point(250, 185)
        Me.lblGblood.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblGblood.Name = "lblGblood"
        Me.lblGblood.Size = New System.Drawing.Size(112, 43)
        Me.lblGblood.TabIndex = 51
        Me.lblGblood.Text = "กรุ๊ปเลือด :"
        '
        'lblEndcard
        '
        Me.lblEndcard.AutoSize = True
        Me.lblEndcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblEndcard.Location = New System.Drawing.Point(1312, 185)
        Me.lblEndcard.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblEndcard.Name = "lblEndcard"
        Me.lblEndcard.Size = New System.Drawing.Size(130, 43)
        Me.lblEndcard.TabIndex = 57
        Me.lblEndcard.Text = "วันหมดอายุ :"
        '
        'txtHeight
        '
        Me.txtHeight.CausesValidation = False
        Me.txtHeight.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHeight.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtHeight.Location = New System.Drawing.Point(990, 110)
        Me.txtHeight.Margin = New System.Windows.Forms.Padding(6)
        Me.txtHeight.Name = "txtHeight"
        Me.txtHeight.Size = New System.Drawing.Size(66, 51)
        Me.txtHeight.TabIndex = 31
        '
        'lblStartcard
        '
        Me.lblStartcard.AutoSize = True
        Me.lblStartcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStartcard.Location = New System.Drawing.Point(1312, 115)
        Me.lblStartcard.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblStartcard.Name = "lblStartcard"
        Me.lblStartcard.Size = New System.Drawing.Size(133, 43)
        Me.lblStartcard.TabIndex = 55
        Me.lblStartcard.Text = "วันออกบัตร :"
        '
        'lblHeight
        '
        Me.lblHeight.AutoSize = True
        Me.lblHeight.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblHeight.Location = New System.Drawing.Point(900, 115)
        Me.lblHeight.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblHeight.Name = "lblHeight"
        Me.lblHeight.Size = New System.Drawing.Size(94, 43)
        Me.lblHeight.TabIndex = 49
        Me.lblHeight.Text = "ส่วนสูง :"
        '
        'txtWeight
        '
        Me.txtWeight.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtWeight.Location = New System.Drawing.Point(638, 110)
        Me.txtWeight.Margin = New System.Windows.Forms.Padding(6)
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Size = New System.Drawing.Size(68, 51)
        Me.txtWeight.TabIndex = 30
        '
        'txtIdcard
        '
        Me.txtIdcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtIdcard.Location = New System.Drawing.Point(1456, 40)
        Me.txtIdcard.Margin = New System.Windows.Forms.Padding(6)
        Me.txtIdcard.Name = "txtIdcard"
        Me.txtIdcard.Size = New System.Drawing.Size(268, 51)
        Me.txtIdcard.TabIndex = 27
        '
        'txtAge
        '
        Me.txtAge.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtAge.Location = New System.Drawing.Point(364, 110)
        Me.txtAge.Margin = New System.Windows.Forms.Padding(6)
        Me.txtAge.Name = "txtAge"
        Me.txtAge.Size = New System.Drawing.Size(58, 51)
        Me.txtAge.TabIndex = 29
        '
        'lblWeight
        '
        Me.lblWeight.AutoSize = True
        Me.lblWeight.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(542, 115)
        Me.lblWeight.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(97, 43)
        Me.lblWeight.TabIndex = 46
        Me.lblWeight.Text = "น้ำหนัก :"
        '
        'lblIdcard
        '
        Me.lblIdcard.AutoSize = True
        Me.lblIdcard.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblIdcard.Location = New System.Drawing.Point(1204, 44)
        Me.lblIdcard.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblIdcard.Name = "lblIdcard"
        Me.lblIdcard.Size = New System.Drawing.Size(234, 43)
        Me.lblIdcard.TabIndex = 52
        Me.lblIdcard.Text = "บัตรประจำตัวประชาชน :"
        '
        'lblAge
        '
        Me.lblAge.AutoSize = True
        Me.lblAge.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblAge.Location = New System.Drawing.Point(302, 115)
        Me.lblAge.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(63, 43)
        Me.lblAge.TabIndex = 45
        Me.lblAge.Text = "อายุ :"
        '
        'cboReligion
        '
        Me.cboReligion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReligion.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboReligion.FormattingEnabled = True
        Me.cboReligion.Items.AddRange(New Object() {"พุทธ", "อิสลาม", "คริสต์", "ฮินดู", "ซิก", "อื่นๆ"})
        Me.cboReligion.Location = New System.Drawing.Point(128, 179)
        Me.cboReligion.Margin = New System.Windows.Forms.Padding(6)
        Me.cboReligion.Name = "cboReligion"
        Me.cboReligion.Size = New System.Drawing.Size(100, 51)
        Me.cboReligion.TabIndex = 34
        '
        'lblReligion
        '
        Me.lblReligion.AutoSize = True
        Me.lblReligion.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblReligion.Location = New System.Drawing.Point(38, 185)
        Me.lblReligion.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblReligion.Name = "lblReligion"
        Me.lblReligion.Size = New System.Drawing.Size(89, 43)
        Me.lblReligion.TabIndex = 43
        Me.lblReligion.Text = "ศาสนา :"
        '
        'cboNationality
        '
        Me.cboNationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNationality.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboNationality.FormattingEnabled = True
        Me.cboNationality.Items.AddRange(New Object() {"ไทย", "พม่า", "ลาว", "อื่นๆ"})
        Me.cboNationality.Location = New System.Drawing.Point(990, 179)
        Me.cboNationality.Margin = New System.Windows.Forms.Padding(6)
        Me.cboNationality.Name = "cboNationality"
        Me.cboNationality.Size = New System.Drawing.Size(110, 51)
        Me.cboNationality.TabIndex = 37
        '
        'lblNationality
        '
        Me.lblNationality.AutoSize = True
        Me.lblNationality.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblNationality.Location = New System.Drawing.Point(896, 185)
        Me.lblNationality.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblNationality.Name = "lblNationality"
        Me.lblNationality.Size = New System.Drawing.Size(96, 43)
        Me.lblNationality.TabIndex = 41
        Me.lblNationality.Text = "สัญชาติ :"
        '
        'lblBirthday
        '
        Me.lblBirthday.AutoSize = True
        Me.lblBirthday.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblBirthday.Location = New System.Drawing.Point(804, 44)
        Me.lblBirthday.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblBirthday.Name = "lblBirthday"
        Me.lblBirthday.Size = New System.Drawing.Size(164, 43)
        Me.lblBirthday.TabIndex = 39
        Me.lblBirthday.Text = "วัน/เดือน/ปี เกิด :"
        '
        'txtLname
        '
        Me.txtLname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtLname.Location = New System.Drawing.Point(638, 40)
        Me.txtLname.Margin = New System.Windows.Forms.Padding(6)
        Me.txtLname.Name = "txtLname"
        Me.txtLname.Size = New System.Drawing.Size(148, 51)
        Me.txtLname.TabIndex = 25
        '
        'txtFname
        '
        Me.txtFname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtFname.Location = New System.Drawing.Point(364, 40)
        Me.txtFname.Margin = New System.Windows.Forms.Padding(6)
        Me.txtFname.Name = "txtFname"
        Me.txtFname.Size = New System.Drawing.Size(148, 51)
        Me.txtFname.TabIndex = 24
        '
        'cboTitle
        '
        Me.cboTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTitle.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboTitle.FormattingEnabled = True
        Me.cboTitle.Items.AddRange(New Object() {"นาย", "นาง", "นางสาว", "ว่าที่ร้อยตรี", "Mr.", "Ms.", "Mrs.", ""})
        Me.cboTitle.Location = New System.Drawing.Point(128, 40)
        Me.cboTitle.Margin = New System.Windows.Forms.Padding(6)
        Me.cboTitle.Name = "cboTitle"
        Me.cboTitle.Size = New System.Drawing.Size(100, 51)
        Me.cboTitle.TabIndex = 23
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblLastName.Location = New System.Drawing.Point(536, 44)
        Me.lblLastName.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(107, 43)
        Me.lblLastName.TabIndex = 35
        Me.lblLastName.Text = "นามสกุล :"
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(12, 44)
        Me.lblTitle.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(115, 43)
        Me.lblTitle.TabIndex = 34
        Me.lblTitle.Text = "คำนำหน้า :"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblName.Location = New System.Drawing.Point(312, 44)
        Me.lblName.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(56, 43)
        Me.lblName.TabIndex = 33
        Me.lblName.Text = "ชื่อ :"
        '
        'lblEmployeeData
        '
        Me.lblEmployeeData.AutoSize = True
        Me.lblEmployeeData.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblEmployeeData.Location = New System.Drawing.Point(116, -6)
        Me.lblEmployeeData.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblEmployeeData.Name = "lblEmployeeData"
        Me.lblEmployeeData.Size = New System.Drawing.Size(0, 26)
        Me.lblEmployeeData.TabIndex = 32
        '
        'frmEmpday
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(2396, 1379)
        Me.Controls.Add(Me.GroupBox30)
        Me.Controls.Add(Me.GroupBox14)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.butClear)
        Me.Controls.Add(Me.butDel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.GroupBox13)
        Me.Controls.Add(Me.rdbEat)
        Me.Controls.Add(Me.GroupBox11)
        Me.Controls.Add(Me.GroupBox10)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox9)
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.Name = "frmEmpday"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "frmEmployeeday"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.rdbEat.ResumeLayout(False)
        Me.rdbEat.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.GroupBox30.ResumeLayout(False)
        Me.GroupBox30.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtSkill As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cboDep As ComboBox
    Friend WithEvents Label71 As Label
    Friend WithEvents txtId As TextBox
    Friend WithEvents Label70 As Label
    Friend WithEvents txtWage As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents cboPosition As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents dtpStart As DateTimePicker
    Friend WithEvents Label7 As Label
    Friend WithEvents dtpRegis As DateTimePicker
    Friend WithEvents chkMedicalcer As CheckBox
    Friend WithEvents chkEducation As CheckBox
    Friend WithEvents chkIdcard As CheckBox
    Friend WithEvents chkRegisHome As CheckBox
    Friend WithEvents chkPicture As CheckBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents chkAnothersnew As CheckBox
    Friend WithEvents chkEmpOffice As CheckBox
    Friend WithEvents chkAnnouncement As CheckBox
    Friend WithEvents chkSignsrecruit As CheckBox
    Friend WithEvents chkInternet As CheckBox
    Friend WithEvents chkAdvicefriend As CheckBox
    Friend WithEvents txtAnothersnew As TextBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents lblProvince As Label
    Friend WithEvents lblDistrict As Label
    Friend WithEvents lblSubdistrict As Label
    Friend WithEvents txtMoo As TextBox
    Friend WithEvents lblNum As Label
    Friend WithEvents lblMu As Label
    Friend WithEvents txtZipcode As TextBox
    Friend WithEvents lblPostcode As Label
    Friend WithEvents txtTelephone As TextBox
    Friend WithEvents lblTelephone As Label
    Friend WithEvents txtMobile As TextBox
    Friend WithEvents lblSmartphone As Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents lblSpouseprovince As Label
    Friend WithEvents lblSpousepostcode As Label
    Friend WithEvents txtAddressmarry As TextBox
    Friend WithEvents txtZipcodemarry As TextBox
    Friend WithEvents txtTephonemarry As TextBox
    Friend WithEvents lblSpousetelephone As Label
    Friend WithEvents txtMobilemarry As TextBox
    Friend WithEvents lblSpousesmartphone As Label
    Friend WithEvents lblSpousedistrict As Label
    Friend WithEvents lblSpousesubdistrict As Label
    Friend WithEvents txtMoomarry As TextBox
    Friend WithEvents lblSpouseNum As Label
    Friend WithEvents lblSpousemu As Label
    Friend WithEvents txtNicknamemarry As TextBox
    Friend WithEvents lblSpousenickname As Label
    Friend WithEvents txtLnamemarry As TextBox
    Friend WithEvents txtFnamemarry As TextBox
    Friend WithEvents cboTitlemarry As ComboBox
    Friend WithEvents lblSpouselastname As Label
    Friend WithEvents lblSpousetitle As Label
    Friend WithEvents lblSpousename As Label
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents GroupBox9 As GroupBox
    Friend WithEvents txtHiswork As TextBox
    Friend WithEvents GroupBox10 As GroupBox
    Friend WithEvents txtSickness As TextBox
    Friend WithEvents GroupBox11 As GroupBox
    Friend WithEvents rdbEat As GroupBox
    Friend WithEvents GroupBox13 As GroupBox
    Friend WithEvents butSave As Button
    Friend WithEvents butDel As Button
    Friend WithEvents butClear As Button
    Friend WithEvents txtAccountnum As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents RadioButton10 As RadioButton
    Friend WithEvents rdbS As RadioButton
    Friend WithEvents rdbWomen As RadioButton
    Friend WithEvents rdbMan As RadioButton
    Friend WithEvents rdbSicknessno As RadioButton
    Friend WithEvents rdbSickness As RadioButton
    Friend WithEvents rdbCigaretteno As RadioButton
    Friend WithEvents rdbCigarette As RadioButton
    Friend WithEvents rdbEatno As RadioButton
    Friend WithEvents RadioButton5 As RadioButton
    Friend WithEvents rdbPrisonno As RadioButton
    Friend WithEvents rdbPrison As RadioButton
    Friend WithEvents dtpContain As DateTimePicker
    Friend WithEvents Label77 As Label
    Friend WithEvents GroupBox14 As GroupBox
    Friend WithEvents rdbContain As RadioButton
    Friend WithEvents rdbTrial As RadioButton
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents txtDistrict As TextBox
    Friend WithEvents txtCanton As TextBox
    Friend WithEvents txtProvincemarry As TextBox
    Friend WithEvents txtDdistrictmarry As TextBox
    Friend WithEvents txtCantonmarry As TextBox
    Friend WithEvents GroupBox30 As GroupBox
    Friend WithEvents txtByprovince As TextBox
    Friend WithEvents txtBycanton As TextBox
    Friend WithEvents dtpBirthday As DateTimePicker
    Friend WithEvents dtpEndidcard As DateTimePicker
    Friend WithEvents dtpStartidcard As DateTimePicker
    Friend WithEvents cboEducation As ComboBox
    Friend WithEvents lblEducation As Label
    Friend WithEvents lblM As Label
    Friend WithEvents lblIssuedatdistrict As Label
    Friend WithEvents lblKg As Label
    Friend WithEvents lblIssuedatprovince As Label
    Friend WithEvents lblYear As Label
    Friend WithEvents txtNickname As TextBox
    Friend WithEvents lblNickname As Label
    Friend WithEvents cboBlood As ComboBox
    Friend WithEvents lblGblood As Label
    Friend WithEvents lblEndcard As Label
    Friend WithEvents txtHeight As TextBox
    Friend WithEvents lblStartcard As Label
    Friend WithEvents lblHeight As Label
    Friend WithEvents txtWeight As TextBox
    Friend WithEvents txtIdcard As TextBox
    Friend WithEvents txtAge As TextBox
    Friend WithEvents lblWeight As Label
    Friend WithEvents lblIdcard As Label
    Friend WithEvents lblAge As Label
    Friend WithEvents cboReligion As ComboBox
    Friend WithEvents lblReligion As Label
    Friend WithEvents cboNationality As ComboBox
    Friend WithEvents lblNationality As Label
    Friend WithEvents lblBirthday As Label
    Friend WithEvents txtLname As TextBox
    Friend WithEvents txtFname As TextBox
    Friend WithEvents cboTitle As ComboBox
    Friend WithEvents lblLastName As Label
    Friend WithEvents lblTitle As Label
    Friend WithEvents lblName As Label
    Friend WithEvents lblEmployeeData As Label
End Class
