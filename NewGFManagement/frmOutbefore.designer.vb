﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOutbefore
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.butEdit = New System.Windows.Forms.Button()
        Me.butDel = New System.Windows.Forms.Button()
        Me.butSave = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtFname = New System.Windows.Forms.TextBox()
        Me.txtTimeout = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtTimein = New System.Windows.Forms.TextBox()
        Me.txtLname = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.butSearch = New System.Windows.Forms.Button()
        Me.txtDep = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPosition = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtGroup = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtSasom = New System.Windows.Forms.TextBox()
        Me.txtLeader = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtComment = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtNumber = New System.Windows.Forms.TextBox()
        Me.dtpDtaebefore = New System.Windows.Forms.DateTimePicker()
        Me.dtpTimebefore = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtBeforenow = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.dgvAtone = New System.Windows.Forms.DataGridView()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvAtone, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.butEdit)
        Me.GroupBox1.Controls.Add(Me.butDel)
        Me.GroupBox1.Controls.Add(Me.butSave)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.dgvAtone)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(28, 31)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1966, 1350)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "บันทึกขอออกก่อนเวลารายบุคคล"
        '
        'butEdit
        '
        Me.butEdit.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butEdit.Location = New System.Drawing.Point(1038, 634)
        Me.butEdit.Margin = New System.Windows.Forms.Padding(2)
        Me.butEdit.Name = "butEdit"
        Me.butEdit.Size = New System.Drawing.Size(132, 60)
        Me.butEdit.TabIndex = 150
        Me.butEdit.Text = "แก้ไข"
        Me.butEdit.UseVisualStyleBackColor = True
        '
        'butDel
        '
        Me.butDel.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butDel.Location = New System.Drawing.Point(858, 634)
        Me.butDel.Margin = New System.Windows.Forms.Padding(2)
        Me.butDel.Name = "butDel"
        Me.butDel.Size = New System.Drawing.Size(114, 60)
        Me.butDel.TabIndex = 149
        Me.butDel.Text = "ลบ"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSave.Location = New System.Drawing.Point(461, 634)
        Me.butSave.Margin = New System.Windows.Forms.Padding(2)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(331, 60)
        Me.butSave.TabIndex = 148
        Me.butSave.Text = "บันทึกขอออกก่อน"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtFname)
        Me.GroupBox2.Controls.Add(Me.txtTimeout)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtTimein)
        Me.GroupBox2.Controls.Add(Me.txtLname)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.txtId)
        Me.GroupBox2.Controls.Add(Me.butSearch)
        Me.GroupBox2.Controls.Add(Me.txtDep)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtPosition)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.txtGroup)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txtSasom)
        Me.GroupBox2.Controls.Add(Me.txtLeader)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(10, 52)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox2.Size = New System.Drawing.Size(1776, 296)
        Me.GroupBox2.TabIndex = 147
        Me.GroupBox2.TabStop = False
        '
        'txtFname
        '
        Me.txtFname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtFname.Location = New System.Drawing.Point(78, 113)
        Me.txtFname.Margin = New System.Windows.Forms.Padding(2)
        Me.txtFname.Name = "txtFname"
        Me.txtFname.ReadOnly = True
        Me.txtFname.Size = New System.Drawing.Size(186, 51)
        Me.txtFname.TabIndex = 102
        '
        'txtTimeout
        '
        Me.txtTimeout.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTimeout.Location = New System.Drawing.Point(542, 192)
        Me.txtTimeout.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTimeout.Name = "txtTimeout"
        Me.txtTimeout.ReadOnly = True
        Me.txtTimeout.Size = New System.Drawing.Size(186, 51)
        Me.txtTimeout.TabIndex = 134
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label11.Location = New System.Drawing.Point(22, 119)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(56, 43)
        Me.Label11.TabIndex = 98
        Me.Label11.Text = "ชื่อ :"
        '
        'txtTimein
        '
        Me.txtTimein.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTimein.Location = New System.Drawing.Point(174, 198)
        Me.txtTimein.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTimein.Name = "txtTimein"
        Me.txtTimein.ReadOnly = True
        Me.txtTimein.Size = New System.Drawing.Size(186, 51)
        Me.txtTimein.TabIndex = 133
        '
        'txtLname
        '
        Me.txtLname.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtLname.Location = New System.Drawing.Point(380, 113)
        Me.txtLname.Margin = New System.Windows.Forms.Padding(2)
        Me.txtLname.Name = "txtLname"
        Me.txtLname.ReadOnly = True
        Me.txtLname.Size = New System.Drawing.Size(186, 51)
        Me.txtLname.TabIndex = 99
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.Location = New System.Drawing.Point(272, 119)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(107, 43)
        Me.Label10.TabIndex = 100
        Me.Label10.Text = "นามสกุล :"
        '
        'txtId
        '
        Me.txtId.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtId.Location = New System.Drawing.Point(262, 41)
        Me.txtId.Margin = New System.Windows.Forms.Padding(2)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(178, 51)
        Me.txtId.TabIndex = 101
        '
        'butSearch
        '
        Me.butSearch.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSearch.Location = New System.Drawing.Point(467, 41)
        Me.butSearch.Margin = New System.Windows.Forms.Padding(2)
        Me.butSearch.Name = "butSearch"
        Me.butSearch.Size = New System.Drawing.Size(140, 57)
        Me.butSearch.TabIndex = 103
        Me.butSearch.Text = "ค้นหา"
        Me.butSearch.UseVisualStyleBackColor = True
        '
        'txtDep
        '
        Me.txtDep.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtDep.Location = New System.Drawing.Point(686, 115)
        Me.txtDep.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDep.Name = "txtDep"
        Me.txtDep.ReadOnly = True
        Me.txtDep.Size = New System.Drawing.Size(96, 51)
        Me.txtDep.TabIndex = 104
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(1398, 81)
        Me.Label19.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(0, 53)
        Me.Label19.TabIndex = 124
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(592, 119)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 43)
        Me.Label2.TabIndex = 105
        Me.Label2.Text = "แผนก :"
        '
        'txtPosition
        '
        Me.txtPosition.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPosition.Location = New System.Drawing.Point(1486, 121)
        Me.txtPosition.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPosition.Name = "txtPosition"
        Me.txtPosition.ReadOnly = True
        Me.txtPosition.Size = New System.Drawing.Size(186, 51)
        Me.txtPosition.TabIndex = 118
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(16, 44)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(249, 43)
        Me.Label9.TabIndex = 106
        Me.Label9.Text = "ค้นหาตามรหัสพนักงาน  :  "
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label18.Location = New System.Drawing.Point(1372, 127)
        Me.Label18.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(109, 43)
        Me.Label18.TabIndex = 117
        Me.Label18.Text = "ตำแหน่ง  :"
        '
        'txtGroup
        '
        Me.txtGroup.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtGroup.Location = New System.Drawing.Point(884, 115)
        Me.txtGroup.Margin = New System.Windows.Forms.Padding(2)
        Me.txtGroup.Name = "txtGroup"
        Me.txtGroup.ReadOnly = True
        Me.txtGroup.Size = New System.Drawing.Size(96, 51)
        Me.txtGroup.TabIndex = 107
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.Location = New System.Drawing.Point(740, 202)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(194, 43)
        Me.Label6.TabIndex = 116
        Me.Label6.Text = "เวลาสะสมคงเหลือ  :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(814, 119)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 43)
        Me.Label3.TabIndex = 108
        Me.Label3.Text = "กลุ่ม :"
        '
        'txtSasom
        '
        Me.txtSasom.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtSasom.Location = New System.Drawing.Point(942, 196)
        Me.txtSasom.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSasom.Name = "txtSasom"
        Me.txtSasom.ReadOnly = True
        Me.txtSasom.Size = New System.Drawing.Size(186, 51)
        Me.txtSasom.TabIndex = 115
        '
        'txtLeader
        '
        Me.txtLeader.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtLeader.Location = New System.Drawing.Point(1154, 115)
        Me.txtLeader.Margin = New System.Windows.Forms.Padding(2)
        Me.txtLeader.Name = "txtLeader"
        Me.txtLeader.ReadOnly = True
        Me.txtLeader.Size = New System.Drawing.Size(186, 51)
        Me.txtLeader.TabIndex = 109
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(380, 198)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(146, 43)
        Me.Label4.TabIndex = 113
        Me.Label4.Text = "เวลาออกงาน  :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(1018, 119)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(134, 43)
        Me.Label5.TabIndex = 110
        Me.Label5.Text = "หัวหน้ากลุ่ม :"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label20.Location = New System.Drawing.Point(28, 202)
        Me.Label20.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(134, 43)
        Me.Label20.TabIndex = 111
        Me.Label20.Text = "เวลาเข้างาน  :"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.TextBox1)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.txtComment)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.txtNumber)
        Me.GroupBox3.Controls.Add(Me.dtpDtaebefore)
        Me.GroupBox3.Controls.Add(Me.dtpTimebefore)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.txtBeforenow)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Location = New System.Drawing.Point(19, 353)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox3.Size = New System.Drawing.Size(1767, 257)
        Me.GroupBox3.TabIndex = 146
        Me.GroupBox3.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(1347, 45)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(191, 43)
        Me.Label1.TabIndex = 150
        Me.Label1.Text = "รวมสะสมคงเหลือ  :"
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(1549, 39)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(186, 51)
        Me.TextBox1.TabIndex = 149
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label16.Location = New System.Drawing.Point(555, 141)
        Me.Label16.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(111, 43)
        Me.Label16.TabIndex = 148
        Me.Label16.Text = "หมายเหตุ :"
        '
        'txtComment
        '
        Me.txtComment.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtComment.Location = New System.Drawing.Point(694, 133)
        Me.txtComment.Margin = New System.Windows.Forms.Padding(2)
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(560, 51)
        Me.txtComment.TabIndex = 147
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.Location = New System.Drawing.Point(17, 141)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(209, 43)
        Me.Label15.TabIndex = 146
        Me.Label15.Text = "เลขที่หนังสืออนุญาติ :"
        '
        'txtNumber
        '
        Me.txtNumber.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtNumber.Location = New System.Drawing.Point(249, 136)
        Me.txtNumber.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.Size = New System.Drawing.Size(214, 51)
        Me.txtNumber.TabIndex = 145
        '
        'dtpDtaebefore
        '
        Me.dtpDtaebefore.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpDtaebefore.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDtaebefore.Location = New System.Drawing.Point(248, 33)
        Me.dtpDtaebefore.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpDtaebefore.Name = "dtpDtaebefore"
        Me.dtpDtaebefore.Size = New System.Drawing.Size(210, 51)
        Me.dtpDtaebefore.TabIndex = 79
        '
        'dtpTimebefore
        '
        Me.dtpTimebefore.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpTimebefore.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpTimebefore.Location = New System.Drawing.Point(694, 33)
        Me.dtpTimebefore.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpTimebefore.Name = "dtpTimebefore"
        Me.dtpTimebefore.Size = New System.Drawing.Size(216, 51)
        Me.dtpTimebefore.TabIndex = 144
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.Location = New System.Drawing.Point(12, 38)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(222, 43)
        Me.Label7.TabIndex = 80
        Me.Label7.Text = "วันเดือนปีขอออกก่อน :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.Location = New System.Drawing.Point(474, 38)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(208, 43)
        Me.Label8.TabIndex = 83
        Me.Label8.Text = "เวลาขอออกงานก่อน :"
        '
        'txtBeforenow
        '
        Me.txtBeforenow.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtBeforenow.Location = New System.Drawing.Point(1136, 38)
        Me.txtBeforenow.Margin = New System.Windows.Forms.Padding(2)
        Me.txtBeforenow.Name = "txtBeforenow"
        Me.txtBeforenow.Size = New System.Drawing.Size(118, 51)
        Me.txtBeforenow.TabIndex = 85
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.Location = New System.Drawing.Point(949, 41)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(185, 43)
        Me.Label13.TabIndex = 86
        Me.Label13.Text = "ขอออกก่อนครั้งนี้ :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.Location = New System.Drawing.Point(1262, 39)
        Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(50, 43)
        Me.Label14.TabIndex = 87
        Me.Label14.Text = "ชม."
        '
        'dgvAtone
        '
        Me.dgvAtone.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAtone.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.Column6, Me.Column7, Me.Column8, Me.Column3, Me.Column9, Me.Column4})
        Me.dgvAtone.Location = New System.Drawing.Point(19, 739)
        Me.dgvAtone.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvAtone.Name = "dgvAtone"
        Me.dgvAtone.RowTemplate.Height = 28
        Me.dgvAtone.Size = New System.Drawing.Size(1898, 263)
        Me.dgvAtone.TabIndex = 94
        '
        'Column2
        '
        Me.Column2.HeaderText = "ว/ด/ป ออกก่อน"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 300
        '
        'Column6
        '
        Me.Column6.HeaderText = "เวลาขอออกงาน"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 250
        '
        'Column7
        '
        Me.Column7.HeaderText = "ครั้งนี้ออกก่อน"
        Me.Column7.Name = "Column7"
        Me.Column7.Width = 300
        '
        'Column8
        '
        Me.Column8.HeaderText = "เลขที่หนังสือ"
        Me.Column8.Name = "Column8"
        Me.Column8.Width = 300
        '
        'Column3
        '
        Me.Column3.HeaderText = "สะสมคงเหลือ"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 250
        '
        'Column9
        '
        Me.Column9.HeaderText = "หมายเหตุ"
        Me.Column9.Name = "Column9"
        Me.Column9.Width = 200
        '
        'Column4
        '
        Me.Column4.FillWeight = 120.0!
        Me.Column4.HeaderText = "ว/ด/ปที่บันทึก"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 300
        '
        'frmOutbefore
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(2016, 1383)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmOutbefore"
        Me.Text = "frmOutbefore"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.dgvAtone, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents dgvAtone As DataGridView
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents txtBeforenow As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents dtpDtaebefore As DateTimePicker
    Friend WithEvents dtpTimebefore As DateTimePicker
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txtFname As TextBox
    Friend WithEvents txtTimeout As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtTimein As TextBox
    Friend WithEvents txtLname As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtId As TextBox
    Friend WithEvents butSearch As Button
    Friend WithEvents txtDep As TextBox
    Public WithEvents Label19 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtPosition As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents txtGroup As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtSasom As TextBox
    Friend WithEvents txtLeader As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents txtComment As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txtNumber As TextBox
    Friend WithEvents butEdit As Button
    Friend WithEvents butDel As Button
    Friend WithEvents butSave As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column9 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
End Class
