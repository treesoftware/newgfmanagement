﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSocialsecurity
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtPersen = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpSetdate = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtMin = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMax = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.butSave = New System.Windows.Forms.Button()
        Me.dgvSos = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.butEdit = New System.Windows.Forms.Button()
        Me.butDel = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvSos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtPersen
        '
        Me.txtPersen.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPersen.Location = New System.Drawing.Point(387, 102)
        Me.txtPersen.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtPersen.Name = "txtPersen"
        Me.txtPersen.Size = New System.Drawing.Size(208, 51)
        Me.txtPersen.TabIndex = 1
        Me.txtPersen.Text = "5"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(105, 111)
        Me.Label1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(253, 43)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "ตั้งค่าประกันสังคม ร้อยละ :"
        '
        'dtpSetdate
        '
        Me.dtpSetdate.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpSetdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpSetdate.Location = New System.Drawing.Point(387, 165)
        Me.dtpSetdate.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.dtpSetdate.Name = "dtpSetdate"
        Me.dtpSetdate.Size = New System.Drawing.Size(208, 51)
        Me.dtpSetdate.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(190, 173)
        Me.Label2.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(168, 43)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "วันเดือนปีเริ่มใช้ :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(622, 108)
        Me.Label3.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(186, 43)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "ได้รับค่าจ้างต่ำกว่า :"
        '
        'txtMin
        '
        Me.txtMin.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtMin.Location = New System.Drawing.Point(852, 102)
        Me.txtMin.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtMin.Name = "txtMin"
        Me.txtMin.Size = New System.Drawing.Size(208, 51)
        Me.txtMin.TabIndex = 6
        Me.txtMin.Text = "1650"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(632, 174)
        Me.Label4.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(174, 43)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "ฐานสูงสุดไม่เกิน :"
        '
        'txtMax
        '
        Me.txtMax.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtMax.Location = New System.Drawing.Point(852, 168)
        Me.txtMax.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.txtMax.Name = "txtMax"
        Me.txtMax.Size = New System.Drawing.Size(208, 51)
        Me.txtMax.TabIndex = 8
        Me.txtMax.Text = "15000"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.butSave)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.dgvSos)
        Me.GroupBox1.Controls.Add(Me.txtMax)
        Me.GroupBox1.Controls.Add(Me.butEdit)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.butDel)
        Me.GroupBox1.Controls.Add(Me.txtPersen)
        Me.GroupBox1.Controls.Add(Me.txtMin)
        Me.GroupBox1.Controls.Add(Me.dtpSetdate)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(17, 18)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox1.Size = New System.Drawing.Size(1431, 673)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "ตั้งค่าประกันสังคม"
        '
        'butSave
        '
        Me.butSave.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSave.Location = New System.Drawing.Point(469, 284)
        Me.butSave.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(117, 56)
        Me.butSave.TabIndex = 11
        Me.butSave.Text = "บันทึก"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'dgvSos
        '
        Me.dgvSos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column5, Me.Column3, Me.Column4, Me.Column6})
        Me.dgvSos.Location = New System.Drawing.Point(9, 392)
        Me.dgvSos.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvSos.Name = "dgvSos"
        Me.dgvSos.RowTemplate.Height = 28
        Me.dgvSos.Size = New System.Drawing.Size(1368, 238)
        Me.dgvSos.TabIndex = 10
        '
        'Column1
        '
        Me.Column1.HeaderText = "ลำดับ"
        Me.Column1.Name = "Column1"
        '
        'Column2
        '
        Me.Column2.HeaderText = "ร้อยละ"
        Me.Column2.Name = "Column2"
        '
        'Column5
        '
        Me.Column5.HeaderText = "วันเดือนปีที่เริ่มใช้"
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 350
        '
        'Column3
        '
        Me.Column3.HeaderText = "ค่าจ้างต่ำกว่า"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 200
        '
        'Column4
        '
        Me.Column4.HeaderText = "ค่าจ้างสูงกว่า"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 200
        '
        'Column6
        '
        Me.Column6.HeaderText = "วันเดือนปีบันทึก"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 300
        '
        'butEdit
        '
        Me.butEdit.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butEdit.Location = New System.Drawing.Point(630, 284)
        Me.butEdit.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butEdit.Name = "butEdit"
        Me.butEdit.Size = New System.Drawing.Size(117, 56)
        Me.butEdit.TabIndex = 12
        Me.butEdit.Text = "แก้ไข"
        Me.butEdit.UseVisualStyleBackColor = True
        '
        'butDel
        '
        Me.butDel.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butDel.Location = New System.Drawing.Point(800, 284)
        Me.butDel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butDel.Name = "butDel"
        Me.butDel.Size = New System.Drawing.Size(117, 56)
        Me.butDel.TabIndex = 13
        Me.butDel.Text = "ลบ"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'frmSocialsecurity
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1527, 751)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.Name = "frmSocialsecurity"
        Me.Text = "frmSocialsecurity"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvSos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtPersen As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents dtpSetdate As DateTimePicker
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtMin As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtMax As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents dgvSos As DataGridView
    Friend WithEvents butEdit As Button
    Friend WithEvents butDel As Button
    Friend WithEvents butSave As Button
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
End Class
