﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGroupoffset
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtTimeout = New System.Windows.Forms.TextBox()
        Me.txtTimein = New System.Windows.Forms.TextBox()
        Me.txtGroupid = New System.Windows.Forms.TextBox()
        Me.butSearch = New System.Windows.Forms.Button()
        Me.txtDep = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPosition = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtGroup = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtLeader = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dtpTimeout = New System.Windows.Forms.DateTimePicker()
        Me.dtpTimein = New System.Windows.Forms.DateTimePicker()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtComment = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtNumber = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtOffsetnow = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.dtpDatein = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dgvGroupoffset = New System.Windows.Forms.DataGridView()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.butEdit = New System.Windows.Forms.Button()
        Me.butDel = New System.Windows.Forms.Button()
        Me.butSave = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvGroupoffset, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox4)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.dgvGroupoffset)
        Me.GroupBox1.Controls.Add(Me.butEdit)
        Me.GroupBox1.Controls.Add(Me.butDel)
        Me.GroupBox1.Controls.Add(Me.butSave)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(27, 34)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1853, 1335)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "บันทึกขอชดเชยเวลาแบบกลุ่ม"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.DataGridView1)
        Me.GroupBox4.Controls.Add(Me.Label20)
        Me.GroupBox4.Location = New System.Drawing.Point(1306, 46)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(524, 709)
        Me.GroupBox4.TabIndex = 136
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "ชื่อสมาชิก"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column10, Me.Column13, Me.Column11, Me.Column12})
        Me.DataGridView1.Location = New System.Drawing.Point(6, 53)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 33
        Me.DataGridView1.Size = New System.Drawing.Size(488, 574)
        Me.DataGridView1.TabIndex = 126
        '
        'Column10
        '
        Me.Column10.HeaderText = "ลำดับ"
        Me.Column10.Name = "Column10"
        '
        'Column13
        '
        Me.Column13.HeaderText = "รหัส"
        Me.Column13.Name = "Column13"
        '
        'Column11
        '
        Me.Column11.HeaderText = "ชื่อ"
        Me.Column11.Name = "Column11"
        '
        'Column12
        '
        Me.Column12.HeaderText = "สกุล"
        Me.Column12.Name = "Column12"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label20.Location = New System.Drawing.Point(56, 175)
        Me.Label20.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(0, 43)
        Me.Label20.TabIndex = 125
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtTimeout)
        Me.GroupBox3.Controls.Add(Me.txtTimein)
        Me.GroupBox3.Controls.Add(Me.txtGroupid)
        Me.GroupBox3.Controls.Add(Me.butSearch)
        Me.GroupBox3.Controls.Add(Me.txtDep)
        Me.GroupBox3.Controls.Add(Me.Label19)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.txtPosition)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label18)
        Me.GroupBox3.Controls.Add(Me.txtGroup)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.txtLeader)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(24, 46)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox3.Size = New System.Drawing.Size(1273, 296)
        Me.GroupBox3.TabIndex = 135
        Me.GroupBox3.TabStop = False
        '
        'txtTimeout
        '
        Me.txtTimeout.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTimeout.Location = New System.Drawing.Point(654, 196)
        Me.txtTimeout.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTimeout.Name = "txtTimeout"
        Me.txtTimeout.ReadOnly = True
        Me.txtTimeout.Size = New System.Drawing.Size(186, 51)
        Me.txtTimeout.TabIndex = 134
        '
        'txtTimein
        '
        Me.txtTimein.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTimein.Location = New System.Drawing.Point(218, 194)
        Me.txtTimein.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTimein.Name = "txtTimein"
        Me.txtTimein.ReadOnly = True
        Me.txtTimein.Size = New System.Drawing.Size(186, 51)
        Me.txtTimein.TabIndex = 133
        '
        'txtGroupid
        '
        Me.txtGroupid.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtGroupid.Location = New System.Drawing.Point(218, 36)
        Me.txtGroupid.Margin = New System.Windows.Forms.Padding(2)
        Me.txtGroupid.Name = "txtGroupid"
        Me.txtGroupid.Size = New System.Drawing.Size(186, 51)
        Me.txtGroupid.TabIndex = 101
        '
        'butSearch
        '
        Me.butSearch.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSearch.Location = New System.Drawing.Point(467, 41)
        Me.butSearch.Margin = New System.Windows.Forms.Padding(2)
        Me.butSearch.Name = "butSearch"
        Me.butSearch.Size = New System.Drawing.Size(140, 57)
        Me.butSearch.TabIndex = 103
        Me.butSearch.Text = "ค้นหา"
        Me.butSearch.UseVisualStyleBackColor = True
        '
        'txtDep
        '
        Me.txtDep.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtDep.Location = New System.Drawing.Point(525, 119)
        Me.txtDep.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDep.Name = "txtDep"
        Me.txtDep.ReadOnly = True
        Me.txtDep.Size = New System.Drawing.Size(96, 51)
        Me.txtDep.TabIndex = 104
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(904, 79)
        Me.Label19.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(0, 53)
        Me.Label19.TabIndex = 124
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(431, 123)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 43)
        Me.Label2.TabIndex = 105
        Me.Label2.Text = "แผนก :"
        '
        'txtPosition
        '
        Me.txtPosition.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPosition.Location = New System.Drawing.Point(992, 119)
        Me.txtPosition.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPosition.Name = "txtPosition"
        Me.txtPosition.ReadOnly = True
        Me.txtPosition.Size = New System.Drawing.Size(186, 51)
        Me.txtPosition.TabIndex = 118
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(8, 44)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(211, 43)
        Me.Label9.TabIndex = 106
        Me.Label9.Text = "ค้นหาตามรหัสกลุ่ม  :  "
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label18.Location = New System.Drawing.Point(878, 125)
        Me.Label18.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(109, 43)
        Me.Label18.TabIndex = 117
        Me.Label18.Text = "ตำแหน่ง  :"
        '
        'txtGroup
        '
        Me.txtGroup.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtGroup.Location = New System.Drawing.Point(723, 119)
        Me.txtGroup.Margin = New System.Windows.Forms.Padding(2)
        Me.txtGroup.Name = "txtGroup"
        Me.txtGroup.ReadOnly = True
        Me.txtGroup.Size = New System.Drawing.Size(96, 51)
        Me.txtGroup.TabIndex = 107
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(653, 123)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 43)
        Me.Label1.TabIndex = 108
        Me.Label1.Text = "กลุ่ม :"
        '
        'txtLeader
        '
        Me.txtLeader.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtLeader.Location = New System.Drawing.Point(218, 119)
        Me.txtLeader.Margin = New System.Windows.Forms.Padding(2)
        Me.txtLeader.Name = "txtLeader"
        Me.txtLeader.ReadOnly = True
        Me.txtLeader.Size = New System.Drawing.Size(186, 51)
        Me.txtLeader.TabIndex = 109
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(436, 204)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(146, 43)
        Me.Label4.TabIndex = 113
        Me.Label4.Text = "เวลาออกงาน  :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(70, 123)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(134, 43)
        Me.Label3.TabIndex = 110
        Me.Label3.Text = "หัวหน้ากลุ่ม :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(70, 199)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(134, 43)
        Me.Label5.TabIndex = 111
        Me.Label5.Text = "เวลาเข้างาน  :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dtpTimeout)
        Me.GroupBox2.Controls.Add(Me.dtpTimein)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.txtComment)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.txtNumber)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.txtOffsetnow)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.dtpDatein)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(24, 340)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(1273, 415)
        Me.GroupBox2.TabIndex = 130
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "เลือกการบันทึก"
        '
        'dtpTimeout
        '
        Me.dtpTimeout.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpTimeout.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpTimeout.Location = New System.Drawing.Point(707, 209)
        Me.dtpTimeout.Margin = New System.Windows.Forms.Padding(6)
        Me.dtpTimeout.Name = "dtpTimeout"
        Me.dtpTimeout.Size = New System.Drawing.Size(176, 51)
        Me.dtpTimeout.TabIndex = 129
        '
        'dtpTimein
        '
        Me.dtpTimein.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpTimein.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpTimein.Location = New System.Drawing.Point(707, 141)
        Me.dtpTimein.Margin = New System.Windows.Forms.Padding(6)
        Me.dtpTimein.Name = "dtpTimein"
        Me.dtpTimein.Size = New System.Drawing.Size(176, 51)
        Me.dtpTimein.TabIndex = 128
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label16.Location = New System.Drawing.Point(564, 290)
        Me.Label16.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(111, 43)
        Me.Label16.TabIndex = 91
        Me.Label16.Text = "หมายเหตุ :"
        '
        'txtComment
        '
        Me.txtComment.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtComment.Location = New System.Drawing.Point(707, 287)
        Me.txtComment.Margin = New System.Windows.Forms.Padding(2)
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(532, 51)
        Me.txtComment.TabIndex = 90
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.Location = New System.Drawing.Point(38, 290)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(209, 43)
        Me.Label15.TabIndex = 89
        Me.Label15.Text = "เลขที่หนังสืออนุญาติ :"
        '
        'txtNumber
        '
        Me.txtNumber.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtNumber.Location = New System.Drawing.Point(270, 285)
        Me.txtNumber.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.Size = New System.Drawing.Size(214, 51)
        Me.txtNumber.TabIndex = 88
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.Location = New System.Drawing.Point(1160, 140)
        Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(50, 43)
        Me.Label14.TabIndex = 87
        Me.Label14.Text = "ชม."
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.Location = New System.Drawing.Point(901, 143)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(134, 43)
        Me.Label13.TabIndex = 86
        Me.Label13.Text = "ชดเชยครั้งนี้ :"
        '
        'txtOffsetnow
        '
        Me.txtOffsetnow.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtOffsetnow.Location = New System.Drawing.Point(1036, 140)
        Me.txtOffsetnow.Margin = New System.Windows.Forms.Padding(2)
        Me.txtOffsetnow.Name = "txtOffsetnow"
        Me.txtOffsetnow.Size = New System.Drawing.Size(118, 51)
        Me.txtOffsetnow.TabIndex = 85
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.Location = New System.Drawing.Point(554, 213)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(107, 43)
        Me.Label8.TabIndex = 83
        Me.Label8.Text = "เวลาออก :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label12.Location = New System.Drawing.Point(568, 140)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(95, 43)
        Me.Label12.TabIndex = 81
        Me.Label12.Text = "เวลาเข้า :"
        '
        'dtpDatein
        '
        Me.dtpDatein.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpDatein.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDatein.Location = New System.Drawing.Point(262, 135)
        Me.dtpDatein.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpDatein.Name = "dtpDatein"
        Me.dtpDatein.Size = New System.Drawing.Size(223, 51)
        Me.dtpDatein.TabIndex = 79
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.Location = New System.Drawing.Point(35, 146)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(171, 43)
        Me.Label7.TabIndex = 80
        Me.Label7.Text = "วันเดือนปีชดเชย :"
        '
        'dgvGroupoffset
        '
        Me.dgvGroupoffset.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGroupoffset.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column14, Me.Column15, Me.Column16, Me.Column7, Me.Column3, Me.Column2, Me.Column5, Me.Column6, Me.Column8, Me.Column9, Me.Column4})
        Me.dgvGroupoffset.Location = New System.Drawing.Point(8, 850)
        Me.dgvGroupoffset.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvGroupoffset.Name = "dgvGroupoffset"
        Me.dgvGroupoffset.RowTemplate.Height = 28
        Me.dgvGroupoffset.Size = New System.Drawing.Size(1819, 356)
        Me.dgvGroupoffset.TabIndex = 94
        Me.dgvGroupoffset.UseWaitCursor = True
        '
        'Column14
        '
        Me.Column14.HeaderText = "รหัส"
        Me.Column14.Name = "Column14"
        '
        'Column15
        '
        Me.Column15.HeaderText = "ชื่อ"
        Me.Column15.Name = "Column15"
        '
        'Column16
        '
        Me.Column16.HeaderText = "สกุล"
        Me.Column16.Name = "Column16"
        '
        'Column7
        '
        Me.Column7.HeaderText = "ครั้งนี้"
        Me.Column7.Name = "Column7"
        '
        'Column3
        '
        Me.Column3.HeaderText = "สะสมคงเหลือ"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 200
        '
        'Column2
        '
        Me.Column2.HeaderText = "วันเดือนปีชดเชย"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 200
        '
        'Column5
        '
        Me.Column5.HeaderText = "เวลาเข้า"
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 200
        '
        'Column6
        '
        Me.Column6.HeaderText = "เวลาออก"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 200
        '
        'Column8
        '
        Me.Column8.HeaderText = "เลขที่หนังสือ"
        Me.Column8.Name = "Column8"
        Me.Column8.Width = 220
        '
        'Column9
        '
        Me.Column9.HeaderText = "หมายเหตุ"
        Me.Column9.Name = "Column9"
        Me.Column9.Width = 200
        '
        'Column4
        '
        Me.Column4.FillWeight = 120.0!
        Me.Column4.HeaderText = "วันเดือนปีทีบันทึก"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 300
        '
        'butEdit
        '
        Me.butEdit.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butEdit.Location = New System.Drawing.Point(1041, 770)
        Me.butEdit.Margin = New System.Windows.Forms.Padding(2)
        Me.butEdit.Name = "butEdit"
        Me.butEdit.Size = New System.Drawing.Size(132, 60)
        Me.butEdit.TabIndex = 93
        Me.butEdit.Text = "แก้ไข"
        Me.butEdit.UseVisualStyleBackColor = True
        '
        'butDel
        '
        Me.butDel.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butDel.Location = New System.Drawing.Point(861, 770)
        Me.butDel.Margin = New System.Windows.Forms.Padding(2)
        Me.butDel.Name = "butDel"
        Me.butDel.Size = New System.Drawing.Size(114, 60)
        Me.butDel.TabIndex = 92
        Me.butDel.Text = "ลบ"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSave.Location = New System.Drawing.Point(549, 770)
        Me.butSave.Margin = New System.Windows.Forms.Padding(2)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(246, 60)
        Me.butSave.TabIndex = 76
        Me.butSave.Text = "บันทึกทำชดเชย"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'frmGroupoffset
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(2546, 1485)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmGroupoffset"
        Me.Text = "frmGroupoffset"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvGroupoffset, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Column10 As DataGridViewTextBoxColumn
    Friend WithEvents Column13 As DataGridViewTextBoxColumn
    Friend WithEvents Column11 As DataGridViewTextBoxColumn
    Friend WithEvents Column12 As DataGridViewTextBoxColumn
    Public WithEvents Label20 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtTimeout As TextBox
    Friend WithEvents txtTimein As TextBox
    Friend WithEvents txtGroupid As TextBox
    Friend WithEvents butSearch As Button
    Friend WithEvents txtDep As TextBox
    Public WithEvents Label19 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtPosition As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents txtGroup As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtLeader As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents dtpTimeout As DateTimePicker
    Friend WithEvents dtpTimein As DateTimePicker
    Friend WithEvents Label16 As Label
    Friend WithEvents txtComment As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txtNumber As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents txtOffsetnow As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents dtpDatein As DateTimePicker
    Friend WithEvents Label7 As Label
    Friend WithEvents dgvGroupoffset As DataGridView
    Friend WithEvents butEdit As Button
    Friend WithEvents butDel As Button
    Friend WithEvents butSave As Button
    Friend WithEvents Column14 As DataGridViewTextBoxColumn
    Friend WithEvents Column15 As DataGridViewTextBoxColumn
    Friend WithEvents Column16 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents Column9 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
End Class
