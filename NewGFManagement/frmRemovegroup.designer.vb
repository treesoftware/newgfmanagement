﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRemoveGroup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.butSave = New System.Windows.Forms.Button()
        Me.butEdit = New System.Windows.Forms.Button()
        Me.butDel = New System.Windows.Forms.Button()
        Me.dgvGroupnew = New System.Windows.Forms.DataGridView()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.butBack = New System.Windows.Forms.Button()
        Me.butAdd = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgvGroupin = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboGroupin = New System.Windows.Forms.ComboBox()
        Me.cboDepin = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dgvGroupout = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboGroupout = New System.Windows.Forms.ComboBox()
        Me.cboDepout = New System.Windows.Forms.ComboBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvGroupnew, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvGroupin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvGroupout, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.butSave)
        Me.GroupBox1.Controls.Add(Me.butEdit)
        Me.GroupBox1.Controls.Add(Me.butDel)
        Me.GroupBox1.Controls.Add(Me.dgvGroupnew)
        Me.GroupBox1.Controls.Add(Me.butBack)
        Me.GroupBox1.Controls.Add(Me.butAdd)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1704, 1477)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "ย้ายกลุ่ม"
        '
        'butSave
        '
        Me.butSave.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSave.Location = New System.Drawing.Point(600, 993)
        Me.butSave.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(265, 56)
        Me.butSave.TabIndex = 60
        Me.butSave.Text = "บันทึกการย้ายกลุ่ม"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'butEdit
        '
        Me.butEdit.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butEdit.Location = New System.Drawing.Point(1031, 993)
        Me.butEdit.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butEdit.Name = "butEdit"
        Me.butEdit.Size = New System.Drawing.Size(117, 56)
        Me.butEdit.TabIndex = 61
        Me.butEdit.Text = "แก้ไข"
        Me.butEdit.UseVisualStyleBackColor = True
        '
        'butDel
        '
        Me.butDel.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butDel.Location = New System.Drawing.Point(890, 993)
        Me.butDel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butDel.Name = "butDel"
        Me.butDel.Size = New System.Drawing.Size(117, 56)
        Me.butDel.TabIndex = 62
        Me.butDel.Text = "ลบ"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'dgvGroupnew
        '
        Me.dgvGroupnew.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGroupnew.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column11, Me.Column6})
        Me.dgvGroupnew.Location = New System.Drawing.Point(8, 1124)
        Me.dgvGroupnew.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvGroupnew.Name = "dgvGroupnew"
        Me.dgvGroupnew.RowTemplate.Height = 33
        Me.dgvGroupnew.Size = New System.Drawing.Size(1763, 212)
        Me.dgvGroupnew.TabIndex = 30
        '
        'Column2
        '
        Me.Column2.HeaderText = "แผนก"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "ชื่อกลุ่ม"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 150
        '
        'Column4
        '
        Me.Column4.HeaderText = "หัวหน้ากลุ่ม"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 200
        '
        'Column5
        '
        Me.Column5.HeaderText = "ชื่อพนักงาน"
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 150
        '
        'Column7
        '
        Me.Column7.HeaderText = "นามสกุล"
        Me.Column7.Name = "Column7"
        Me.Column7.Width = 200
        '
        'Column8
        '
        Me.Column8.HeaderText = "เวลาเข้างาน"
        Me.Column8.Name = "Column8"
        Me.Column8.Width = 200
        '
        'Column9
        '
        Me.Column9.HeaderText = "เวลาออกงาน"
        Me.Column9.Name = "Column9"
        Me.Column9.Width = 200
        '
        'Column10
        '
        Me.Column10.HeaderText = "วันเริ่ม"
        Me.Column10.Name = "Column10"
        Me.Column10.Width = 150
        '
        'Column11
        '
        Me.Column11.HeaderText = "วันสิ้นสุด"
        Me.Column11.Name = "Column11"
        Me.Column11.Width = 200
        '
        'Column6
        '
        Me.Column6.HeaderText = "วันเดือนปีที่บันทึก"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 300
        '
        'butBack
        '
        Me.butBack.Location = New System.Drawing.Point(748, 741)
        Me.butBack.Margin = New System.Windows.Forms.Padding(4)
        Me.butBack.Name = "butBack"
        Me.butBack.Size = New System.Drawing.Size(214, 88)
        Me.butBack.TabIndex = 17
        Me.butBack.Text = "<<"
        Me.butBack.UseVisualStyleBackColor = True
        '
        'butAdd
        '
        Me.butAdd.Location = New System.Drawing.Point(748, 535)
        Me.butAdd.Margin = New System.Windows.Forms.Padding(4)
        Me.butAdd.Name = "butAdd"
        Me.butAdd.Size = New System.Drawing.Size(214, 90)
        Me.butAdd.TabIndex = 16
        Me.butAdd.Text = ">>"
        Me.butAdd.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvGroupin)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.cboGroupin)
        Me.GroupBox3.Controls.Add(Me.cboDepin)
        Me.GroupBox3.Location = New System.Drawing.Point(1048, 41)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Size = New System.Drawing.Size(509, 946)
        Me.GroupBox3.TabIndex = 7
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "กลุ่มที่จะย้ายเข้า"
        '
        'dgvGroupin
        '
        Me.dgvGroupin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGroupin.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.dgvGroupin.Location = New System.Drawing.Point(24, 275)
        Me.dgvGroupin.Name = "dgvGroupin"
        Me.dgvGroupin.RowTemplate.Height = 33
        Me.dgvGroupin.Size = New System.Drawing.Size(455, 638)
        Me.dgvGroupin.TabIndex = 8
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "ชื่อ"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "สกุล"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(146, 217)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(310, 53)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "รายชื่อกลุมที่ต้องการย้ายเข้า"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(124, 63)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(146, 50)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "เลือกแผนก :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.Location = New System.Drawing.Point(26, 138)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(244, 50)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "เลือกกลุ่มที่จะย้ายเข้า :"
        '
        'cboGroupin
        '
        Me.cboGroupin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGroupin.FormattingEnabled = True
        Me.cboGroupin.Location = New System.Drawing.Point(288, 133)
        Me.cboGroupin.Margin = New System.Windows.Forms.Padding(4)
        Me.cboGroupin.Name = "cboGroupin"
        Me.cboGroupin.Size = New System.Drawing.Size(164, 61)
        Me.cboGroupin.TabIndex = 3
        '
        'cboDepin
        '
        Me.cboDepin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepin.FormattingEnabled = True
        Me.cboDepin.Location = New System.Drawing.Point(288, 52)
        Me.cboDepin.Margin = New System.Windows.Forms.Padding(4)
        Me.cboDepin.Name = "cboDepin"
        Me.cboDepin.Size = New System.Drawing.Size(164, 61)
        Me.cboDepin.TabIndex = 2
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvGroupout)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cboGroupout)
        Me.GroupBox2.Controls.Add(Me.cboDepout)
        Me.GroupBox2.Location = New System.Drawing.Point(158, 41)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(508, 946)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "กลุ่มที่จะย้ายออก"
        '
        'dgvGroupout
        '
        Me.dgvGroupout.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGroupout.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.dgvGroupout.Location = New System.Drawing.Point(26, 283)
        Me.dgvGroupout.Name = "dgvGroupout"
        Me.dgvGroupout.RowTemplate.Height = 33
        Me.dgvGroupout.Size = New System.Drawing.Size(455, 638)
        Me.dgvGroupout.TabIndex = 7
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "ชื่อ"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "สกุล"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(148, 227)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(321, 53)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "รายชื่อกลุมที่ต้องการย้ายออก"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(136, 63)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(146, 50)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "เลือกแผนก :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(25, 138)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(257, 50)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "เลือกกลุ่มที่จะย้ายออก :"
        '
        'cboGroupout
        '
        Me.cboGroupout.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGroupout.FormattingEnabled = True
        Me.cboGroupout.Location = New System.Drawing.Point(290, 133)
        Me.cboGroupout.Margin = New System.Windows.Forms.Padding(4)
        Me.cboGroupout.Name = "cboGroupout"
        Me.cboGroupout.Size = New System.Drawing.Size(164, 61)
        Me.cboGroupout.TabIndex = 3
        '
        'cboDepout
        '
        Me.cboDepout.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepout.FormattingEnabled = True
        Me.cboDepout.Location = New System.Drawing.Point(290, 52)
        Me.cboDepout.Margin = New System.Windows.Forms.Padding(4)
        Me.cboDepout.Name = "cboDepout"
        Me.cboDepout.Size = New System.Drawing.Size(164, 61)
        Me.cboDepout.TabIndex = 2
        '
        'frmRemoveGroup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1772, 1383)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmRemoveGroup"
        Me.Text = "frmRemoveGroup"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvGroupnew, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.dgvGroupin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvGroupout, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents cboGroupin As ComboBox
    Friend WithEvents cboDepin As ComboBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cboGroupout As ComboBox
    Friend WithEvents cboDepout As ComboBox
    Friend WithEvents butBack As Button
    Friend WithEvents butAdd As Button
    Friend WithEvents dgvGroupnew As DataGridView
    Friend WithEvents dgvGroupout As DataGridView
    Friend WithEvents butSave As Button
    Friend WithEvents butEdit As Button
    Friend WithEvents butDel As Button
    Friend WithEvents dgvGroupin As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents Column9 As DataGridViewTextBoxColumn
    Friend WithEvents Column10 As DataGridViewTextBoxColumn
    Friend WithEvents Column11 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
End Class
