﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportmonth
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dtpEnd = New System.Windows.Forms.DateTimePicker()
        Me.dtpStart = New System.Windows.Forms.DateTimePicker()
        Me.butPrint = New System.Windows.Forms.Button()
        Me.butSearch = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboDep = New System.Windows.Forms.ComboBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ReportViewer1)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.dtpEnd)
        Me.GroupBox1.Controls.Add(Me.dtpStart)
        Me.GroupBox1.Controls.Add(Me.butPrint)
        Me.GroupBox1.Controls.Add(Me.butSearch)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cboDep)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 16.125!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(21, 24)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(2004, 973)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "รายงานค่าจ้างรายเดือนตามแผนก"
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Location = New System.Drawing.Point(82, 314)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(1787, 632)
        Me.ReportViewer1.TabIndex = 33
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(352, 204)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(308, 50)
        Me.Label9.TabIndex = 32
        Me.Label9.Text = "เลือกวันสิ้นสุดของกลุ่มงาน :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.Location = New System.Drawing.Point(381, 109)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(279, 50)
        Me.Label8.TabIndex = 31
        Me.Label8.Text = "เลือกวันเริ่มของกลุ่มงาน :"
        '
        'dtpEnd
        '
        Me.dtpEnd.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEnd.Location = New System.Drawing.Point(684, 204)
        Me.dtpEnd.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpEnd.Name = "dtpEnd"
        Me.dtpEnd.Size = New System.Drawing.Size(226, 57)
        Me.dtpEnd.TabIndex = 30
        '
        'dtpStart
        '
        Me.dtpStart.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStart.Location = New System.Drawing.Point(684, 111)
        Me.dtpStart.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpStart.Name = "dtpStart"
        Me.dtpStart.Size = New System.Drawing.Size(226, 57)
        Me.dtpStart.TabIndex = 29
        '
        'butPrint
        '
        Me.butPrint.Location = New System.Drawing.Point(1351, 100)
        Me.butPrint.Name = "butPrint"
        Me.butPrint.Size = New System.Drawing.Size(397, 88)
        Me.butPrint.TabIndex = 2
        Me.butPrint.Text = "พิมพ์รายงานค่าจ้าง"
        Me.butPrint.UseVisualStyleBackColor = True
        '
        'butSearch
        '
        Me.butSearch.Location = New System.Drawing.Point(1060, 111)
        Me.butSearch.Name = "butSearch"
        Me.butSearch.Size = New System.Drawing.Size(133, 67)
        Me.butSearch.TabIndex = 2
        Me.butSearch.Text = "ค้นหา"
        Me.butSearch.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(50, 111)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(125, 50)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "ชื่อแผนก :"
        '
        'cboDep
        '
        Me.cboDep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDep.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboDep.FormattingEnabled = True
        Me.cboDep.Location = New System.Drawing.Point(177, 108)
        Me.cboDep.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cboDep.Name = "cboDep"
        Me.cboDep.Size = New System.Drawing.Size(147, 51)
        Me.cboDep.TabIndex = 22
        '
        'frmReportmonth
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(2340, 1743)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmReportmonth"
        Me.Text = "frmReportmonth"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents dtpEnd As DateTimePicker
    Friend WithEvents dtpStart As DateTimePicker
    Friend WithEvents butPrint As Button
    Friend WithEvents butSearch As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents cboDep As ComboBox
End Class
