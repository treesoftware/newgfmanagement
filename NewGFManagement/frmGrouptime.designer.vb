﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGrouptime
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.butClear = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgvWorktime = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvNamegroup = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvNamedep = New System.Windows.Forms.DataGridView()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dtpEnd = New System.Windows.Forms.DateTimePicker()
        Me.dtpStart = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.butBack = New System.Windows.Forms.Button()
        Me.butAdd = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cboLeader = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboDep = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboGroup = New System.Windows.Forms.ComboBox()
        Me.butEdit = New System.Windows.Forms.Button()
        Me.butDel = New System.Windows.Forms.Button()
        Me.butSave = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvWorktime, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvNamegroup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvNamedep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.butClear)
        Me.GroupBox1.Controls.Add(Me.DataGridView1)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.butEdit)
        Me.GroupBox1.Controls.Add(Me.butDel)
        Me.GroupBox1.Controls.Add(Me.butSave)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(22, 21)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1746, 1523)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "จัดกลุ่ม"
        '
        'butClear
        '
        Me.butClear.Location = New System.Drawing.Point(920, 1018)
        Me.butClear.Margin = New System.Windows.Forms.Padding(4)
        Me.butClear.Name = "butClear"
        Me.butClear.Size = New System.Drawing.Size(142, 56)
        Me.butClear.TabIndex = 29
        Me.butClear.Text = "เคลียร์"
        Me.butClear.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column11, Me.Column15})
        Me.DataGridView1.Location = New System.Drawing.Point(24, 1099)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 33
        Me.DataGridView1.Size = New System.Drawing.Size(1698, 401)
        Me.DataGridView1.TabIndex = 29
        '
        'Column2
        '
        Me.Column2.HeaderText = "แผนก"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "ชื่อกลุ่ม"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 150
        '
        'Column4
        '
        Me.Column4.HeaderText = "หัวหน้ากลุ่ม"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 200
        '
        'Column5
        '
        Me.Column5.HeaderText = "ชื่อพนักงาน"
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 150
        '
        'Column7
        '
        Me.Column7.HeaderText = "นามสกุล"
        Me.Column7.Name = "Column7"
        Me.Column7.Width = 200
        '
        'Column8
        '
        Me.Column8.HeaderText = "เวลาเข้างาน"
        Me.Column8.Name = "Column8"
        Me.Column8.Width = 200
        '
        'Column9
        '
        Me.Column9.HeaderText = "เวลาออกงาน"
        Me.Column9.Name = "Column9"
        Me.Column9.Width = 200
        '
        'Column10
        '
        Me.Column10.HeaderText = "วันเริ่ม"
        Me.Column10.Name = "Column10"
        Me.Column10.Width = 150
        '
        'Column11
        '
        Me.Column11.HeaderText = "วันสิ้นสุด"
        Me.Column11.Name = "Column11"
        Me.Column11.Width = 200
        '
        'Column15
        '
        Me.Column15.HeaderText = "วันเดือนปีบันทึก"
        Me.Column15.Name = "Column15"
        Me.Column15.Width = 250
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvWorktime)
        Me.GroupBox3.Controls.Add(Me.dgvNamegroup)
        Me.GroupBox3.Controls.Add(Me.dgvNamedep)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.dtpEnd)
        Me.GroupBox3.Controls.Add(Me.dtpStart)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.butBack)
        Me.GroupBox3.Controls.Add(Me.butAdd)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Location = New System.Drawing.Point(24, 165)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Size = New System.Drawing.Size(1698, 824)
        Me.GroupBox3.TabIndex = 33
        Me.GroupBox3.TabStop = False
        '
        'dgvWorktime
        '
        Me.dgvWorktime.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvWorktime.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7})
        Me.dgvWorktime.Location = New System.Drawing.Point(1138, 116)
        Me.dgvWorktime.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvWorktime.Name = "dgvWorktime"
        Me.dgvWorktime.RowTemplate.Height = 33
        Me.dgvWorktime.Size = New System.Drawing.Size(541, 337)
        Me.dgvWorktime.TabIndex = 31
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.FillWeight = 75.0!
        Me.DataGridViewTextBoxColumn5.HeaderText = "ลำดับ"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "เวลาเข้างาน"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 200
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "เวลาออกงาน"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 200
        '
        'dgvNamegroup
        '
        Me.dgvNamegroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvNamegroup.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.dgvNamegroup.Location = New System.Drawing.Point(625, 116)
        Me.dgvNamegroup.Name = "dgvNamegroup"
        Me.dgvNamegroup.RowTemplate.Height = 33
        Me.dgvNamegroup.Size = New System.Drawing.Size(478, 678)
        Me.dgvNamegroup.TabIndex = 30
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "รหัส"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "ชื่อ"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "สกุล"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'dgvNamedep
        '
        Me.dgvNamedep.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvNamedep.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column12, Me.Column13, Me.Column14})
        Me.dgvNamedep.Location = New System.Drawing.Point(23, 116)
        Me.dgvNamedep.Name = "dgvNamedep"
        Me.dgvNamedep.RowTemplate.Height = 33
        Me.dgvNamedep.Size = New System.Drawing.Size(478, 664)
        Me.dgvNamedep.TabIndex = 29
        '
        'Column12
        '
        Me.Column12.HeaderText = "รหัส"
        Me.Column12.Name = "Column12"
        '
        'Column13
        '
        Me.Column13.HeaderText = "ชื่อ"
        Me.Column13.Name = "Column13"
        '
        'Column14
        '
        Me.Column14.HeaderText = "สกุล"
        Me.Column14.Name = "Column14"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(1249, 654)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(295, 50)
        Me.Label9.TabIndex = 28
        Me.Label9.Text = "เลือกวันสิ้นสุดของกลุ่มงาน"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.Location = New System.Drawing.Point(1263, 491)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(266, 50)
        Me.Label8.TabIndex = 27
        Me.Label8.Text = "เลือกวันเริ่มของกลุ่มงาน"
        '
        'dtpEnd
        '
        Me.dtpEnd.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEnd.Location = New System.Drawing.Point(1316, 716)
        Me.dtpEnd.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpEnd.Name = "dtpEnd"
        Me.dtpEnd.Size = New System.Drawing.Size(226, 57)
        Me.dtpEnd.TabIndex = 26
        '
        'dtpStart
        '
        Me.dtpStart.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStart.Location = New System.Drawing.Point(1316, 566)
        Me.dtpStart.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpStart.Name = "dtpStart"
        Me.dtpStart.Size = New System.Drawing.Size(226, 57)
        Me.dtpStart.TabIndex = 25
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(1306, 54)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(235, 50)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "เลือกเวลาเข้าออกงาน"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(703, 54)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(252, 50)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "รายชื่อพนักงานในกลุ่ม"
        '
        'butBack
        '
        Me.butBack.Location = New System.Drawing.Point(508, 338)
        Me.butBack.Margin = New System.Windows.Forms.Padding(4)
        Me.butBack.Name = "butBack"
        Me.butBack.Size = New System.Drawing.Size(110, 44)
        Me.butBack.TabIndex = 15
        Me.butBack.Text = "<<"
        Me.butBack.UseVisualStyleBackColor = True
        '
        'butAdd
        '
        Me.butAdd.Location = New System.Drawing.Point(508, 253)
        Me.butAdd.Margin = New System.Windows.Forms.Padding(4)
        Me.butAdd.Name = "butAdd"
        Me.butAdd.Size = New System.Drawing.Size(110, 44)
        Me.butAdd.TabIndex = 14
        Me.butAdd.Text = ">>"
        Me.butAdd.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(104, 58)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(272, 50)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "รายชื่อพนักงานในแผนก "
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboLeader)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cboDep)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.cboGroup)
        Me.GroupBox2.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(24, 51)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(1698, 116)
        Me.GroupBox2.TabIndex = 32
        Me.GroupBox2.TabStop = False
        '
        'cboLeader
        '
        Me.cboLeader.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeader.FormattingEnabled = True
        Me.cboLeader.Location = New System.Drawing.Point(1273, 33)
        Me.cboLeader.Margin = New System.Windows.Forms.Padding(4)
        Me.cboLeader.Name = "cboLeader"
        Me.cboLeader.Size = New System.Drawing.Size(300, 58)
        Me.cboLeader.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(68, 36)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(146, 50)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "เลือกแผนก :"
        '
        'cboDep
        '
        Me.cboDep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDep.FormattingEnabled = True
        Me.cboDep.Location = New System.Drawing.Point(222, 33)
        Me.cboDep.Margin = New System.Windows.Forms.Padding(4)
        Me.cboDep.Name = "cboDep"
        Me.cboDep.Size = New System.Drawing.Size(279, 58)
        Me.cboDep.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1048, 42)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(207, 50)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "เลือกหัวหน้ากลุ่ม :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(572, 42)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(162, 50)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "เลือกชื่อกลุ่ม :"
        '
        'cboGroup
        '
        Me.cboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGroup.FormattingEnabled = True
        Me.cboGroup.Location = New System.Drawing.Point(742, 33)
        Me.cboGroup.Margin = New System.Windows.Forms.Padding(4)
        Me.cboGroup.Name = "cboGroup"
        Me.cboGroup.Size = New System.Drawing.Size(294, 58)
        Me.cboGroup.TabIndex = 11
        '
        'butEdit
        '
        Me.butEdit.Location = New System.Drawing.Point(552, 1018)
        Me.butEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.butEdit.Name = "butEdit"
        Me.butEdit.Size = New System.Drawing.Size(142, 56)
        Me.butEdit.TabIndex = 20
        Me.butEdit.Text = "แก้ไข"
        Me.butEdit.UseVisualStyleBackColor = True
        '
        'butDel
        '
        Me.butDel.Location = New System.Drawing.Point(727, 1018)
        Me.butDel.Margin = New System.Windows.Forms.Padding(4)
        Me.butDel.Name = "butDel"
        Me.butDel.Size = New System.Drawing.Size(142, 56)
        Me.butDel.TabIndex = 19
        Me.butDel.Text = "ลบ"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.Location = New System.Drawing.Point(311, 1018)
        Me.butSave.Margin = New System.Windows.Forms.Padding(4)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(192, 56)
        Me.butSave.TabIndex = 18
        Me.butSave.Text = "บันทึกกลุ่ม"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'frmGrouptime
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1886, 1557)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmGrouptime"
        Me.Text = "frmGrouptime"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.dgvWorktime, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvNamegroup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvNamedep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents dtpEnd As DateTimePicker
    Friend WithEvents dtpStart As DateTimePicker
    Friend WithEvents Label6 As Label
    Friend WithEvents butEdit As Button
    Friend WithEvents butDel As Button
    Friend WithEvents butSave As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents butBack As Button
    Friend WithEvents butAdd As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents cboGroup As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents cboLeader As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents cboDep As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents butClear As Button
    Friend WithEvents dgvNamegroup As DataGridView
    Friend WithEvents dgvNamedep As DataGridView
    Friend WithEvents dgvWorktime As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents Column9 As DataGridViewTextBoxColumn
    Friend WithEvents Column10 As DataGridViewTextBoxColumn
    Friend WithEvents Column11 As DataGridViewTextBoxColumn
    Friend WithEvents Column15 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents Column12 As DataGridViewTextBoxColumn
    Friend WithEvents Column13 As DataGridViewTextBoxColumn
    Friend WithEvents Column14 As DataGridViewTextBoxColumn
End Class
