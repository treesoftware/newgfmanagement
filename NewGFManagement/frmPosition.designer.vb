﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPosition
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtPos_comment = New System.Windows.Forms.TextBox()
        Me.txtPos_id = New System.Windows.Forms.TextBox()
        Me.cboDep = New System.Windows.Forms.ComboBox()
        Me.butSave = New System.Windows.Forms.Button()
        Me.txtPos_name = New System.Windows.Forms.TextBox()
        Me.butEdit = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.butDel = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvPosition = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvPosition, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox1)
        Me.GroupBox2.Controls.Add(Me.dgvPosition)
        Me.GroupBox2.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(13, 14)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox2.Size = New System.Drawing.Size(1695, 1008)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "สร้างตำแหน่งงาน"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtPos_comment)
        Me.GroupBox1.Controls.Add(Me.txtPos_id)
        Me.GroupBox1.Controls.Add(Me.cboDep)
        Me.GroupBox1.Controls.Add(Me.butSave)
        Me.GroupBox1.Controls.Add(Me.txtPos_name)
        Me.GroupBox1.Controls.Add(Me.butEdit)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.butDel)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(40, 78)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(5, 6, 5, 6)
        Me.GroupBox1.Size = New System.Drawing.Size(1545, 320)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(53, 59)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(125, 50)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "ชื่อแผนก :"
        '
        'txtPos_comment
        '
        Me.txtPos_comment.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPos_comment.Location = New System.Drawing.Point(1000, 121)
        Me.txtPos_comment.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPos_comment.Name = "txtPos_comment"
        Me.txtPos_comment.Size = New System.Drawing.Size(420, 51)
        Me.txtPos_comment.TabIndex = 20
        '
        'txtPos_id
        '
        Me.txtPos_id.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPos_id.Location = New System.Drawing.Point(180, 121)
        Me.txtPos_id.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPos_id.Name = "txtPos_id"
        Me.txtPos_id.Size = New System.Drawing.Size(147, 51)
        Me.txtPos_id.TabIndex = 19
        '
        'cboDep
        '
        Me.cboDep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDep.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cboDep.FormattingEnabled = True
        Me.cboDep.Location = New System.Drawing.Point(180, 56)
        Me.cboDep.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cboDep.Name = "cboDep"
        Me.cboDep.Size = New System.Drawing.Size(147, 51)
        Me.cboDep.TabIndex = 5
        '
        'butSave
        '
        Me.butSave.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butSave.Location = New System.Drawing.Point(340, 226)
        Me.butSave.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(244, 56)
        Me.butSave.TabIndex = 8
        Me.butSave.Text = "บันทึกตำแหน่งงาน"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'txtPos_name
        '
        Me.txtPos_name.Font = New System.Drawing.Font("Angsana New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPos_name.Location = New System.Drawing.Point(550, 125)
        Me.txtPos_name.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPos_name.Name = "txtPos_name"
        Me.txtPos_name.Size = New System.Drawing.Size(246, 51)
        Me.txtPos_name.TabIndex = 17
        '
        'butEdit
        '
        Me.butEdit.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butEdit.Location = New System.Drawing.Point(809, 226)
        Me.butEdit.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butEdit.Name = "butEdit"
        Me.butEdit.Size = New System.Drawing.Size(117, 56)
        Me.butEdit.TabIndex = 9
        Me.butEdit.Text = "แก้ไข"
        Me.butEdit.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(833, 121)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(130, 50)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "หมายเหตุ :"
        '
        'butDel
        '
        Me.butDel.Font = New System.Drawing.Font("Angsana New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.butDel.Location = New System.Drawing.Point(643, 226)
        Me.butDel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.butDel.Name = "butDel"
        Me.butDel.Size = New System.Drawing.Size(117, 56)
        Me.butDel.TabIndex = 10
        Me.butDel.Text = "ลบ"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(36, 129)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(167, 50)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "รหัสตำแหน่ง :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Angsana New", 13.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(372, 129)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(151, 50)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "ชื่อตำแหน่ง :"
        '
        'dgvPosition
        '
        Me.dgvPosition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPosition.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.Column1, Me.Column4, Me.Column2, Me.Column3})
        Me.dgvPosition.Location = New System.Drawing.Point(19, 437)
        Me.dgvPosition.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dgvPosition.Name = "dgvPosition"
        Me.dgvPosition.RowTemplate.Height = 33
        Me.dgvPosition.Size = New System.Drawing.Size(1582, 442)
        Me.dgvPosition.TabIndex = 4
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "รหัสแผนก"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "ชื่อแผนก"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 200
        '
        'Column1
        '
        Me.Column1.HeaderText = "รหัสตำแหน่ง"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 250
        '
        'Column4
        '
        Me.Column4.HeaderText = "ชื่อตำแหน่ง"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 150
        '
        'Column2
        '
        Me.Column2.HeaderText = "หมายเหตุ"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 200
        '
        'Column3
        '
        Me.Column3.HeaderText = "วันเดือนปีทีบันทึก"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 300
        '
        'frmPosition
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(2016, 1081)
        Me.Controls.Add(Me.GroupBox2)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "frmPosition"
        Me.Text = "frmPosition"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvPosition, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents butDel As Button
    Friend WithEvents butEdit As Button
    Friend WithEvents butSave As Button
    Friend WithEvents cboDep As ComboBox
    Friend WithEvents dgvPosition As DataGridView
    Friend WithEvents Label2 As Label
    Friend WithEvents txtPos_comment As TextBox
    Friend WithEvents txtPos_id As TextBox
    Friend WithEvents txtPos_name As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
End Class
